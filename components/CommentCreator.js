import { useState } from 'react';
import { gql, useMutation } from '@apollo/client';
import Button from '@mui/material/Button';
import { useSnackbar } from 'notistack';
import MarkdownEditorLoaded from './MarkdownEditorLoaded';

function CommentCreator({ entity, refetch }) {
  const [newComment, setNewComment] = useState('');
  const { enqueueSnackbar } = useSnackbar();

  const CREATE_COMMENT_MUTATION = gql`
    mutation CreateComment($createCommentComment: CommentInput!) {
      createComment(comment: $createCommentComment) {
        id
        description
        author {
          id
          name
        }
      }
    }
  `;
  const [createComment] = useMutation(CREATE_COMMENT_MUTATION);

  const handleCommentChange = (event) => {
    setNewComment(event);
  };

  const handleCommentSubmit = async () => {
    try {
      await createComment({
        variables: {
          createCommentComment: {
            ...entity,
            description: newComment,
          },
        },
      });
      await refetch();
      setNewComment('');
      enqueueSnackbar('Комментарий успешно добавлен', { variant: 'success' });
    } catch (error) {
      console.error('Ошибка при добавлении комментария:', error);
      enqueueSnackbar('Ошибка при добавлении комментария', { variant: 'error' });
    }
  };

  return (
    <div>
      <div style={{
        border: '1px solid #ddd',
        borderRadius: '5px',
        marginTop: '15px',
        marginBottom: '5px',
      }}
      >
        <MarkdownEditorLoaded
          value={newComment}
          onChange={handleCommentChange}
        />
      </div>
      <Button
        variant="contained"
        disabled={!newComment}
        onClick={handleCommentSubmit}
      >
        Добавить комментарий
      </Button>
    </div>
  );
}

export default CommentCreator;
