import { useState } from 'react';
import { gql, useMutation } from '@apollo/client';
import Button from '@mui/material/Button';
import { useSnackbar } from 'notistack';
import MarkdownEditorLoaded from './MarkdownEditorLoaded';

function CommentEditor({ refetch, comment }) {
  const [newComment, setNewComment] = useState(comment.description);
  const { enqueueSnackbar } = useSnackbar();

  const EDIT_COMMENT_MUTATION = gql`
    mutation editComment($editCommentId: ID!, $editCommentComment: CommentUpdateInput!) {
      editComment(id: $editCommentId, comment: $editCommentComment) {
        id
        description
        author {
          id
          name
        }
      }
    }
  `;

  const DELETE_COMMENT_MUTATION = gql`
    mutation deleteComment($deleteCommentId: ID!) {
      deleteComment(id: $deleteCommentId)
    }
  `;

  const [editComment] = useMutation(EDIT_COMMENT_MUTATION);
  const [deleteComment] = useMutation(DELETE_COMMENT_MUTATION);

  const handleCommentChange = (event) => {
    setNewComment(event);
  };

  const handleCommentSubmit = async () => {
    try {
      await editComment({
        variables: {
          editCommentComment: {
            description: newComment,
          },
          editCommentId: comment.id,
        },
      });
      await refetch();
      setNewComment(comment.description);
      enqueueSnackbar('Комментарий успешно отредактирован', { variant: 'success' });
    } catch (error) {
      console.error('Ошибка при редактировании комментария:', error);
      enqueueSnackbar('Ошибка при редактировании комментария', { variant: 'error' });
    }
  };

  const handleCommentDelete = async () => {
    try {
      await deleteComment({
        variables: {
          deleteCommentId: comment.id,
        },
      });
      await refetch();
      enqueueSnackbar('Комментарий успешно удален', { variant: 'success' });
    } catch (error) {
      console.error('Ошибка при удалении комментария:', error);
      enqueueSnackbar('Ошибка при удалении комментария', { variant: 'error' });
    }
  };

  return (
    <div>
      <div>
        <MarkdownEditorLoaded
          value={newComment}
          onChange={handleCommentChange}
        />
      </div>
      <div style={{ display: 'flex', gap: 8 }}>
        <Button
          variant="contained"
          disabled={!newComment}
          onClick={handleCommentSubmit}
        >
          Отредактировать
        </Button>
        <Button
          variant="contained"
          onClick={handleCommentDelete}
        >
          Удалить
        </Button>
      </div>
    </div>
  );
}

export default CommentEditor;
