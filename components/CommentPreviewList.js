import { IconButton, useTheme } from '@mui/material';
import moment from 'moment';
import { useState } from 'react';
import Edit from '../assets/edit.svg';
import CommentEditor from './CommentEditor';
import MarkdownPreviewLoaded from './MarkdownPreviewLoaded';

const styles = {
  fieldContentComment: {
    border: '1px solid #ddd',
    borderRadius: '5px',
    padding: '5px',
    marginBottom: '5px',
  },
};

function CommentPreviewList(props) {
  const { comment, refetch, user } = props;
  const [editMode, setEditMode] = useState(false);
  const theme = useTheme();

  return (
    <div key={comment.id} style={styles.fieldContentComment}>
      <span>
        <span
          style={{
            fontWeight: 'bold',
            fontSize: 14,
            paddingRight: 8,
          }}
        >
          {comment.author?.name || 'Без имени'}
        </span>
        {moment(comment.createdAt).format('DD.MM.YYYY HH:mm:ss')}
        {comment.author.id === user.id ? (
          <IconButton
            onClick={() => {
              setEditMode(!editMode);
            }}
          >
            <Edit style={{ stroke: theme.palette.primary.main }} />
          </IconButton>
        ) : null}
      </span>
      <span>
        {editMode ? (
          <CommentEditor
            comment={comment}
            refetch={async () => {
              await refetch();
              setEditMode(false);
            }}
          />
        ) : (
          <MarkdownPreviewLoaded value={comment.description} />
        )}
      </span>
    </div>
  );
}

export default CommentPreviewList;
