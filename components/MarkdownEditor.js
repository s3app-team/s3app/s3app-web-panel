// import type { CodeBlockEditorDescriptor } from '@mdxeditor/editor';
// eslint-disable-next-line import/no-unresolved

'use client';

import { imagePlugin } from '@mdxeditor/editor';
// eslint-disable-next-line import/no-unresolved
import '@mdxeditor/editor/style.css';

const {
  MDXEditor,
  headingsPlugin,
  toolbarPlugin,
  BoldItalicUnderlineToggles,
  linkDialogPlugin,
  listsPlugin,
  linkPlugin,
  quotePlugin,
  markdownShortcutPlugin,
  CreateLink,
  ListsToggle,
  InsertImage,
} = await import('@mdxeditor/editor');
// import { MDXEditor } from '@mdxeditor/editor';
function MarkdownEditor({ onChange, value, editorRef }) {
  console.log(editorRef);
  return (
    <>
      <style>
        {`.markdown-editor {
          padding-top: 0px;
          padding-bottom: 0px;
        }
        .mdxeditor-popup-container input[type='file'] {
          display: none;
        }
        .mdxeditor-popup-container label[for='file'] {
          display: none;
        }
      }`}
      </style>
      <MDXEditor
        plugins={
        [
          linkDialogPlugin(),
          headingsPlugin(),
          listsPlugin(),
          linkPlugin(),
          quotePlugin(),
          markdownShortcutPlugin(),
          imagePlugin(),
          toolbarPlugin({
            // eslint-disable-next-line react/no-unstable-nested-components
            toolbarContents: () => (
              <>
                {' '}
                <BoldItalicUnderlineToggles />
                <CreateLink />
                <ListsToggle />
                <InsertImage />
              </>
            ),
          }),
        ]
      }
        contentEditableClassName="markdown-editor"
        ref={editorRef}
        onChange={onChange}
        markdown={value}
      />
    </>
  );
}

export default MarkdownEditor;
