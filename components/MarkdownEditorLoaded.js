import dynamic from 'next/dynamic';
import { forwardRef, useEffect, useRef } from 'react';

const MarkdownEditor = dynamic(() => import('./MarkdownEditor'), { ssr: false });
// eslint-disable-next-line react/display-name
const ForwardRefEditor = forwardRef((props, ref) => <MarkdownEditor {...props} editorRef={ref} />);

function MarkdownEditorLoaded(props) {
  const ref = useRef(null);
  useEffect(() => {
    if (ref.current && (ref.current.getMarkdown() !== props.value)) {
      ref.current.setMarkdown(props.value);
    }
  }, [props.value]);
  return (
    <ForwardRefEditor
      ref={ref}
      {...props}
    />
  );
}

export default MarkdownEditorLoaded;
