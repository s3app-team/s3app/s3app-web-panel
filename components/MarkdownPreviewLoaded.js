import dynamic from 'next/dynamic';

const MarkdownPreview = dynamic(
  () => import('./MarkdownPreview'),
  { ssr: false },
);

function MarkdownPreviewLoaded(props) {
  return <MarkdownPreview {...props} />;
}

export default MarkdownPreviewLoaded;
