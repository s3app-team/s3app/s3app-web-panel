export default function MyStepper(props) {
  return (
    <div style={{ display: 'flex', flexDirection: 'column' }}>
      {props.items?.map((item, index) => (
        <div key={index} style={{ display: 'flex' }}>
          <div style={{ display: 'flex', flexDirection: 'column' }}>
            <div>{item.icon}</div>
            {(index !== props.items.length - 1) && (
            <div style={{ flex: 1, marginLeft: '50%', minHeight: '20px' }}>
              <div style={{ borderLeft: '2px solid', height: '100%', ...props.connectorStyle }} />
            </div>
            )}
          </div>
          <div>
            {item.content}
          </div>
        </div>
      ))}
    </div>
  );
}
