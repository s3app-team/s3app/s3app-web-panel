import { PickersDay } from '@mui/x-date-pickers';
import { AdapterDateFns } from '@mui/x-date-pickers/AdapterDateFns';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import { StaticDatePicker } from '@mui/x-date-pickers/StaticDatePicker';
import { useState } from 'react';

function CustomDay({
  day, startDate, endDate, ...other
}) {
  const inRange = !!(
    new Date(startDate).setHours(0, 0, 0, 0) <= day
    && day <= new Date(endDate).setHours(0, 0, 0, 0)
  );
  const isFirst = new Date(day).setHours(0, 0, 0, 0) === new Date(startDate).setHours(0, 0, 0, 0);
  const isLast = new Date(day).setHours(0, 0, 0, 0) === new Date(endDate).setHours(0, 0, 0, 0);

  let style = null;
  let containerStyle = null;
  if (inRange) {
    style = {
      background: '#fde9da',
    };
    containerStyle = {
      background: '#fde9da', borderRadius: '0px', padding: 0, margin: 0,
    };
    if (isFirst || isLast) {
      delete style.borderRadius;
      style.backgroundColor = '#fbd7bc';
      if (isFirst !== isLast) {
        if (isFirst) {
          containerStyle.borderTopLeftRadius = '50%';
          containerStyle.borderBottomLeftRadius = '50%';
          containerStyle.background = 'linear-gradient(to right, transparent 50%, #fde9da 50%)';
        } else {
          containerStyle.borderTopRightRadius = '50%';
          containerStyle.borderBottomRightRadius = '50%';
          containerStyle.background = 'linear-gradient(to right, #fde9da 50%, transparent 50%)';
        }
      } else {
        delete containerStyle.background;
      }
    }
  }

  return (
    <span style={containerStyle}>
      <PickersDay
        {...other}
        day={day}
        style={style}
      />
    </span>
  );
}

function RangeDatePicker(props) {
  const [firstSelected, setFirstSelected] = useState(false);

  return (
    <div>
      <LocalizationProvider dateAdapter={AdapterDateFns}>
        <StaticDatePicker
          orientation="landscape"
          displayStaticWrapperAs="desktop"
          showDaysOutsideCurrentMonth
          openToYearSelection
          value={new Date(props.startDate)}
          onChange={(date) => {
            if (firstSelected) {
              let startDate = new Date(props.startDate);
              let endDate = date;
              if (startDate > endDate) {
                [startDate, endDate] = [endDate, startDate];
              }
              props.onChange(startDate, endDate);
              setFirstSelected(false);
            } else {
              props.onChange(date, date);
              setFirstSelected(true);
            }
          }}
          slots={{ day: CustomDay }}
          slotProps={{
            day: { startDate: props.startDate, endDate: props.endDate },
          }}
        />
      </LocalizationProvider>
    </div>
  );
}

export default RangeDatePicker;
