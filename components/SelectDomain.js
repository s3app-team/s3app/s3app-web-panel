import { useQuery, gql } from '@apollo/client';

import Select from '@mui/material/Select';
import MenuItem from '@mui/material/MenuItem';
import { Home, RadioButtonUnchecked, SupervisedUserCircle } from '@mui/icons-material';
import { domainsToHierarchy } from './common';

const GET_DOMAINS = gql(`
query GetDomains {
    getDomains {
      id
      name
      parentDomain {
        id
      }
      domainType
    }
  }
`);

function createDomainsList(list, domains, level) {
  return domains.forEach((domain) => {
    list.push({ domain, level });
    if (domain.subDomains) {
      createDomainsList(list, domain.subDomains, level + 1);
    }
  });
}

function SelectDomain(props) {
  const domains = useQuery(GET_DOMAINS);
  if (domains.loading) return null;

  const domainsList = domainsToHierarchy(domains.data.getDomains);

  let list = [];
  createDomainsList(list, domainsList, 0);
  if (props.filter) {
    list = list.filter(props.filter);
  }

  return (
    <Select
      sx={{ minWidth: 250 }}
      {...props}
    >
      {list.map((item) => {
        let Icon = Home;
        if (item.domain.domainType === 'circle') {
          Icon = RadioButtonUnchecked;
        }
        if (item.domain.domainType === 'role') {
          Icon = SupervisedUserCircle;
        }
        return (
          <MenuItem
            key={item.domain.id}
            value={item.domain.id}
            sx={{
              paddingLeft: `${20 + 30 * item.level}px`,
            }}
          >
            <div style={{
              display: 'flex',
              alignItems: 'center',
              gap: 4,
            }}
            >
              <Icon />
              {item.domain.name}
            </div>
          </MenuItem>
        );
      })}
    </Select>
  );
}

export default SelectDomain;
