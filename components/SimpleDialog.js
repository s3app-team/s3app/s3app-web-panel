import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  DialogContentText,
} from '@mui/material';

function SimpleDialog(props) {
  return (
    <Dialog open={props.open} onClose={props.onCancel}>
      <DialogTitle>{props.title}</DialogTitle>
      <DialogContent>
        <DialogContentText>
          {props.text}
        </DialogContentText>
        {props.children}
      </DialogContent>
      <DialogActions>
        {props.externalActions}
        {props.actions ? (
          <>
            {props.actions.map((action, index) => (
              <Button
                key={index}
                onClick={action.onClick}
                variant="text"
                color={action.color || 'primary'}
              >
                {action.text}
              </Button>
            ))}
          </>
        ) : (
          <>
            <Button
              onClick={props.onCancel}
              variant="text"
              color="tertiary"
            >
              {props.cancelText || 'Нет'}
            </Button>
            <Button
              disabled={props.disabled}
              onClick={props.onAccept}
              variant="text"
            >
              {props.acceptText || 'Да'}
            </Button>
          </>
        )}
      </DialogActions>
    </Dialog>
  );
}

export default SimpleDialog;
