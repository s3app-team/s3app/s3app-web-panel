import { useState, useRef } from 'react';
import {
  TextField, InputAdornment, Tooltip, IconButton, Popover,
} from '@mui/material';
import moment from 'moment';
import { AdapterMoment } from '@mui/x-date-pickers/AdapterMoment';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import { Close } from '@mui/icons-material';

import RangeDatePicker from './RangeDatePicker';

function TableDateRangeFilter({ column, placeholder }) {
  const dateFilterRef = useRef(null);
  const [dateFilterOpen, setDateFilterOpen] = useState(false);

  if (!column) return null;

  const filterValue = column.getFilterValue() || [];
  const isEmpty = !filterValue[0] && !filterValue[1];

  return (
    <>
      <TextField
        ref={dateFilterRef}
        value={isEmpty
          ? ''
          : `${moment(filterValue[0]).format('DD.MM')} - ${moment(filterValue[1]).format('DD.MM')}`}
        placeholder={placeholder || 'Дата создания'}
        onClick={() => setDateFilterOpen(true)}
        variant="standard"
        size="small"
        InputProps={{
          className: 'mt-[3px] cursor-pointer',
          endAdornment: (
            <InputAdornment position="end">
              <Tooltip placement="right" title="Очистить фильтр">
                <span>
                  <IconButton
                    disabled={isEmpty}
                    size="small"
                    className="scale-90 size-8 text-black/[0.26]"
                    onClick={() => {
                      column.setFilterValue([]);
                    }}
                  >
                    <Close />
                  </IconButton>
                </span>
              </Tooltip>
            </InputAdornment>
          ),
          form: {
            autocomplete: 'off',
          },
        }}
        autoComplete="off"
      />
      <Popover
        open={dateFilterOpen}
        anchorEl={dateFilterRef.current}
        onClose={() => setDateFilterOpen(false)}
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'left',
        }}
      >
        <LocalizationProvider dateAdapter={AdapterMoment} adapterLocale="ru">
          <RangeDatePicker
            startDate={filterValue[0] || new Date()}
            endDate={filterValue[1] || new Date()}
            onChange={(startDate, endDate) => {
              startDate.setHours(0, 0, 0, 0);
              endDate.setHours(23, 59, 59, 999);
              column.setFilterValue([startDate, endDate]);
            }}
            onAccept={() => setDateFilterOpen(false)}
          />
        </LocalizationProvider>
      </Popover>
    </>
  );
}

export default TableDateRangeFilter;
