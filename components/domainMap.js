import { useRef } from 'react';
import { TransformWrapper, TransformComponent } from 'react-zoom-pan-pinch';
import { useRouter } from 'next/router';
import { Box, useTheme } from '@mui/material';

import S3Avatar from './S3Avatar';

function polarToCartesian(centerX, centerY, radius, angleInDegrees) {
  const returnValue = {};
  const angleInRadians = (angleInDegrees * Math.PI) / 180.0;
  returnValue.x = Math.round((centerX + radius * Math.cos(angleInRadians)) * 100) / 100;
  returnValue.y = Math.round((centerY + radius * Math.sin(angleInRadians)) * 100) / 100;
  return returnValue;
}

const constants = {
  circleBg: 115,
  gap: 120,
  avatarSize: 38,
};

function calcCircleSize(domain) {
  const length = domain?.participants?.length;
  if (length <= 5) return 150;
  return (length * 2 * constants.avatarSize + constants.gap) / Math.PI;
}

function calcOrganizationSize(domain) {
  const length = domain?.subDomains.length;
  let size = 0;

  if (!length) return calcCircleSize(domain) + constants.gap * 2;

  const ar = [];
  domain?.subDomains.forEach((d) => {
    ar.push(calcCircleSize(d));
    size += calcCircleSize(d);
  });
  size += calcCircleSize(domain);

  ar.sort((a, b) => (a < b ? 1 : -1));
  let minSize = 0;
  minSize += calcCircleSize(domain);
  minSize += ar[0] * 2;
  minSize += constants.gap * 6;
  if (size < minSize) size = minSize;

  return size;
}

function Circle(props) {
  const circle = useRef();
  const router = useRouter();

  if (!props.domain) {
    return null;
  }

  let x;
  let y;
  if (props.domain.parentDomain) {
    const coords = polarToCartesian(
      calcOrganizationSize(props.domain.parentDomain) / 2,
      calcOrganizationSize(props.domain.parentDomain) / 2,
      calcOrganizationSize(props.domain.parentDomain) / 2
          - calcCircleSize(props.domain) / 2
          - constants.circleBg,
      props.index
          * (360 / (props.domain.parentDomain.subDomains.length)),
    );
    x = coords.x;
    y = coords.y;
  }

  return (
    <Box
      id={props.domain.id}
      sx={{
        width: calcCircleSize(props.domain) + constants.circleBg,
        height: calcCircleSize(props.domain) + constants.circleBg,
        borderRadius: '50%',
        background: props.domain.domainType === 'organization' ? 'none' : '#dbe6f5',
        position: props.domain.domainType === 'organization' ? 'relative' : 'absolute',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        border: `${props.domain.domainType === 'organization' ? 0 : 2}px solid #B5CAE9`,
        cursor: props.rootProps.clickDomain ? 'pointer' : undefined,
      }}
      style={{
        left: props.domain.domainType === 'organization' ? undefined : `${
          x - (calcCircleSize(props.domain) / 2 + constants.circleBg / 2)
        }px`,
        top: props.domain.domainType === 'organization' ? undefined : `${
          y - (calcCircleSize(props.domain) / 2 + constants.circleBg / 2)
        }px`,
      }}
      onClick={() => { if (props.rootProps.clickDomain) { router.push(`/domains/${props.domain.id}`); } }}
    >
      <Box
        ref={circle}
        id={props.domain.id}
        sx={{
          width: calcCircleSize(props.domain),
          height: calcCircleSize(props.domain),
          borderRadius: '50%',
          outline: '3px solid #B5CAE9',
          position: props.domain.domainType === 'organization' ? 'relative' : 'absolute',
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'center',
          flexDirection: 'column',
        }}
        // style={{
        //   left: props.domain.domainType === 'organization' ? undefined : `${
        //     x - (calcCircleSize(props.domain) / 2 + constants.circleBg / 2)
        //   }px`,
        //   top: props.domain.domainType === 'organization' ? undefined : `${
        //     y - (calcCircleSize(props.domain) / 2 + constants.circleBg / 2)
        //   }px`,
        // }}
      >

        <S3Avatar
          name={props.domain.name}
          color="#687B98"
          noBorder
        />
        {props.domain.participants?.map((participant) => {
          const { x: participantX, y: participantY } = polarToCartesian(
            calcCircleSize(props.domain) / 2,
            calcCircleSize(props.domain) / 2,
            calcCircleSize(props.domain) / 2,
            props.domain.participants.indexOf(participant)
      * (360 / props.domain.participants.length),
          );

          return (
            <Box
              key={participant.index}
              sx={{
                display: 'flex',
                alignItems: 'center',
                justifyContent: 'center',
                flexDirection: 'column',
              }}
              style={{
                position: 'absolute',
                left: `${participantX - constants.avatarSize / 2}px`,
                top: `${participantY - constants.avatarSize / 2}px`,
              }}
            >
              <span
                style={{
                  position: 'fixed',
                  fontSize: 9,
                  marginTop: -53,
                  textWrap: 'nowrap',
                }}
              >
                {participant.role}
              </span>
              <S3Avatar name={participant.name || participant.role} />
              <span
                style={{
                  position: 'fixed',
                  fontSize: 9,
                  marginTop: 53,
                  textWrap: 'nowrap',
                }}
              >
                {participant.name}
              </span>
            </Box>
          );
        })}
      </Box>

      {props.domain.domainType !== 'organization'
        && (
        <Box
          sx={{
            color: 'white',
            background: '#859CBE',
            borderRadius: '7px',
            padding: '3px 10px',
            fontSize: '10px',
            width: 'fit-content',
            position: 'absolute',
            bottom: '-5px',
            left: '50%',
            transform: 'translateX(-50%)',
            textAlign: 'center',
          }}
        >
          {props.domain.name}
        </Box>
        )}
    </Box>
  );
}

function DomainMap(props) {
  const theme = useTheme();
  const domains = JSON.parse(JSON.stringify(props.domains));
  domains.forEach((domain) => {
    if (domain.subDomains) {
      // eslint-disable-next-line no-param-reassign
      domain.subDomains = domain.subDomains.filter((subDomain) => {
        if (subDomain.domainType === 'role') {
          subDomain.participants.forEach((participant) => {
            domain.participants.push({ ...participant, role: subDomain.name });
          });
          if (subDomain.participants.length === 0) {
            domain.participants.push({ id: subDomain.id, name: '', role: subDomain.name });
          }
          return false;
        }
        return true;
      });
      domain.subDomains.forEach((subDomain) => {
        if (subDomain.subDomains) {
          // eslint-disable-next-line no-param-reassign
          subDomain.subDomains = subDomain.subDomains.filter((subSubDomain) => {
            if (subSubDomain.domainType === 'role') {
              subSubDomain.participants.forEach((participant) => {
                subDomain.participants.push({ ...participant, role: subSubDomain.name });
              });
              if (subSubDomain.participants.length === 0) {
                subDomain.participants.push({ id: subSubDomain.id, name: '', role: subSubDomain.name });
              }
              return false;
            }
            return true;
          });
        }
      });
    }
  });
  return (
    <div>
      <TransformWrapper minScale={0.2} maxScale={4} limitToBounds={false}>
        <TransformComponent wrapperStyle={{ height: `calc(100vh - ${theme.variables.header.height}px`, width: '100%' }}>
          {domains.map((domain, index) => (
            <Box
              key={index}
              id={`${domain.id}-p`}
              sx={{
                width: `${calcOrganizationSize(domain)}px`,
                height: `${calcOrganizationSize(domain)}px`,
                m: 2,
                borderRadius: '50%',
                outline: '3px dashed #8993A3',
                outlineOffset: '-3px',
                background: '#e7eef8',
                position: 'relative',
                display: 'flex',
                alignItems: 'center',
                justifyContent: 'center',
                flexDirection: 'column',
              }}
            >
              {domain.subDomains.map((d, index2) => (
                <Circle
                  key={index2}
                  index={index2}
                  domain={{ ...d, parentDomain: domain }}
                  rootProps={props}
                />
              ))}
              <Circle domain={domain} rootProps={props} />
              <Box
                sx={{
                  color: 'white',
                  background: '#F79244',
                  borderRadius: '7px',
                  padding: '3px 10px',
                  fontSize: '10px',
                  width: 'fit-content',
                  position: 'absolute',
                  bottom: '-5px',
                  left: '50%',
                  transform: 'translateX(-50%)',
                }}
              >
                {domain.name}
              </Box>
            </Box>
          ))}
        </TransformComponent>
      </TransformWrapper>
    </div>
  );
}

export default DomainMap;
