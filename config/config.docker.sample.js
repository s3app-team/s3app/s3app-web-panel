import getConfig from 'next/config';

const { publicRuntimeConfig } = getConfig();

const config = {
  server: publicRuntimeConfig.server || '',
  demo: publicRuntimeConfig.demo,
  license: publicRuntimeConfig.license,
  noConfirmation: publicRuntimeConfig.noConfirmation,
};
console.log(config);

export default config;
