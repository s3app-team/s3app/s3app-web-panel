import {
  IconButton,
  MenuItem,
  Select,
  Tooltip,
  Button,
  TextField,
  FormControl,
  InputLabel,
} from '@mui/material';

import NavigateBeforeIcon from '@mui/icons-material/NavigateBefore';
import NavigateNextIcon from '@mui/icons-material/NavigateNext';
import CalendarMonth from '@mui/icons-material/CalendarMonth';
import ArchitectureIcon from '@mui/icons-material/Architecture';
import VisibilityIcon from '@mui/icons-material/Visibility';
import GroupsIcon from '@mui/icons-material/Groups';
import ManageAccountsIcon from '@mui/icons-material/ManageAccounts';
import BallotIcon from '@mui/icons-material/Ballot';

import moment from 'moment';
import 'moment/locale/ru';
import Head from 'next/head';
import { useState } from 'react';
import { gql, useQuery, useMutation } from '@apollo/client';

import { LocalizationProvider, StaticDatePicker } from '@mui/x-date-pickers';
import { AdapterMoment } from '@mui/x-date-pickers/AdapterMoment';

import { useRouter } from 'next/router';
import LeftSubPanel from '../../../components/LeftSubPanel';
import DomainLeftPanel from '../../../components/DomainLeftPanel';
import SimpleDialog from '../../../components/SimpleDialog';
import { checkEmpty } from '../../../components/common';

moment.locale('ru');

const GET_DOMAIN = gql`
    query getDomain(
        $id: ID!
        $idOrganizationUsers: String!
        $withSubDomains: Boolean
    ) {
        getDomain(id: $id) {
            id
            name
            sprints {
                id
                name
                endDate
                startDate
            }
            events(withSubDomains: $withSubDomains) {
                id
                name
                endDate
                startDate
                type
                participants {
                    id
                    name
                }
                domain {
                  id
                  name
                }
            }
            participants {
                id
                name
            }
        }
        getOrganizationUsers(id: $idOrganizationUsers) {
            accessRole
            createdAt
            user {
                id
                name
            }
        }
        
        getDomainsOfOrganization(domainId: $id) {
          id
          name
        }
    }
`;

const CREATE_EVENT = gql`
    mutation CreateEvent($event: EventInput!) {
        createEvent(event: $event) {
            id
        }
    }
`;

const EDIT_EVENT = gql`
    mutation EditEvent($editEventId: ID!, $event: EventUpdateInput!) {
        editEvent(id: $editEventId, event: $event) {
            id
        }
    }
`;

const DELETE_EVENT = gql`
    mutation DeleteEvent($id: ID!) {
        deleteEvent(id: $id)
    }
`;

const viewTypes = [
  { value: 'day', title: 'День' },
  { value: 'week', title: 'Неделя' },
  { value: 'month', title: 'Месяц' },
];

function compareDates(date1, date2, hours) {
  const d1 = new Date(date1);
  const d2 = new Date(date2);

  if (!hours) {
    d1.setHours(0, 0, 0, 0);
    d2.setHours(0, 0, 0, 0);
  }

  return d1.getTime() === d2.getTime();
}

function isDateInRange(start, end, target, strict, hours) {
  const startDate = new Date(start);
  const targetDate = new Date(target);
  const endDate = new Date(end);

  if (!hours) {
    startDate.setHours(0, 0, 0, 0);
    targetDate.setHours(0, 0, 0, 0);
    endDate.setHours(0, 0, 0, 0);
  }

  if (strict) return targetDate > startDate && targetDate < endDate;
  return targetDate >= startDate && targetDate <= endDate;
}

function colorEvent(type) {
  const colors = { backgroundColor: '#DAE5F4', color: '#000' };
  switch (type) {
    case 'review':
      colors.backgroundColor = '#61E294';
      break;
    case 'retrospective':
      colors.backgroundColor = '#cb52a4';
      colors.color = '#fff';
      break;
    case 'management':
      colors.backgroundColor = '#6D9DC5';
      colors.color = '#fff';
      break;
    case 'choiceForRole':
      colors.backgroundColor = '#BCE7FD';
      break;
    default:
      break;
  }

  return colors;
}

function EventIcon({ type }) {
  const types = {
    review: <VisibilityIcon style={{ zIndex: 1 }} />,
    retrospective: <GroupsIcon style={{ zIndex: 1 }} />,
    management: <ManageAccountsIcon style={{ zIndex: 1 }} />,
    choiceForRole: <BallotIcon style={{ zIndex: 1 }} />,
    planning: <ArchitectureIcon style={{ zIndex: 1 }} />,
  };
  return types[type] || null;
}

function getEventName(type) {
  const types = {
    review: 'Ревью',
    retrospective: 'Ретроспектива',
    management: 'Управление',
    choiceForRole: 'Выбор ролей',
    planning: 'Планирование',
  };

  return types[type] || null;
}

function Sprint(props) {
  const router = useRouter();
  const { sprint, time } = props;
  const startDate = moment(sprint.startDate).format('DD.MM.YYYY');
  const endDate = moment(sprint.endDate).format('DD.MM.YYYY');
  const tooltipTitle = `${sprint.name} | ${startDate} - ${endDate}`;

  const sprintStyle = {
    backgroundColor: '#f79244',
    cursor: 'pointer',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    gap: '5px',
    height: '25px',
  };

  const iconStyle = {
    fill: 'white',
    scale: '0.8',
  };

  const openSprint = () => {
    router.push({ pathname: `sprints/${sprint.id}`, query: router.query });
  };

  if (
    compareDates(sprint.startDate, sprint.endDate)
    && compareDates(sprint.startDate, time)
  ) {
    return (
      <Tooltip title={tooltipTitle}>
        <div
          style={{
            ...sprintStyle,
            borderRadius: '4px',
            alignSelf: 'center',
            padding: '0px 10px',
            margin: '0 10px',
          }}
          onClick={openSprint}
        >
          <span style={{ color: 'white', textWrap: 'nowrap' }}>
            {sprint.name}
          </span>
          <CalendarMonth style={iconStyle} />
        </div>
      </Tooltip>
    );
  }

  if (isDateInRange(sprint.startDate, sprint.endDate, time, true)) {
    return (
      <Tooltip title={tooltipTitle}>
        <div
          style={{
            ...sprintStyle,
            width: '101%',
          }}
          onClick={openSprint}
        >
          {props.index === 0 && (
            <>
              <span
                style={{ color: 'white', textWrap: 'nowrap' }}
              >
                {sprint.name}
              </span>
              <CalendarMonth style={iconStyle} />
            </>
          )}
        </div>
      </Tooltip>
    );
  }

  if (compareDates(sprint.startDate, time)) {
    return (
      <Tooltip title={tooltipTitle}>
        <div
          style={{
            ...sprintStyle,
            width: '90%',
            borderRadius: '4px 0 0 4px',
            alignSelf: 'end',
            marginRight: '-1px',
            paddingLeft: '10px',
          }}
          onClick={openSprint}
        >
          <span style={{ color: 'white', textWrap: 'nowrap' }}>
            {sprint.name}
          </span>
          <CalendarMonth style={iconStyle} />
        </div>
      </Tooltip>
    );
  }

  if (compareDates(sprint.endDate, time)) {
    return (
      <Tooltip title={tooltipTitle}>
        <div
          style={{
            ...sprintStyle,
            width: '90%',
            borderRadius: '0 4px 4px 0',
          }}
          onClick={openSprint}
        />
      </Tooltip>
    );
  }

  if (!isDateInRange(sprint.startDate, sprint.endDate, time, false)) {
    return (
      <div
        style={{
          backgroundColor: 'transparent',
          width: '100%',
          height: sprintStyle.height,
        }}
      />
    );
  }

  return null;
}

function Event(props) {
  const { event, time, openEvent } = props;
  const startDate = moment(event.startDate).format('DD.MM.YYYY HH:mm');
  const endDate = moment(event.endDate).format('DD.MM.YYYY HH:mm');
  const tooltipTitle = `${event.name} | ${startDate} - ${endDate}`;

  const eventStyle = {
    cursor: 'pointer',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    gap: '5px',
    height: '25px',
    ...colorEvent(event.type),
  };

  if (
    compareDates(event.startDate, event.endDate)
    && compareDates(event.startDate, time)
  ) {
    return (
      <Tooltip title={tooltipTitle}>
        <div
          style={{
            ...eventStyle,
            borderRadius: '4px',
            alignSelf: 'center',
            padding: '0px 10px',
            margin: '0 10px',
          }}
          onClick={() => openEvent(event)}
        >
          <span style={{ textWrap: 'nowrap' }}>{event.name}</span>
          <EventIcon type={event.type} />
        </div>
      </Tooltip>
    );
  }

  if (isDateInRange(event.startDate, event.endDate, time, true)) {
    return (
      <Tooltip title={tooltipTitle}>
        <div
          style={{
            ...eventStyle,
            width: '101%',
          }}
          onClick={() => openEvent(event)}
        >
          {props.index === 0 && (
            <>
              <span style={{ textWrap: 'nowrap' }}>
                {event.name}
              </span>
              <EventIcon type={event.type} />
            </>
          )}
        </div>
      </Tooltip>
    );
  }

  if (compareDates(event.startDate, time)) {
    return (
      <Tooltip title={tooltipTitle}>
        <div
          style={{
            ...eventStyle,
            width: '90%',
            borderRadius: '4px 0 0 4px',
            alignSelf: 'end',
            marginRight: '-1px',
            paddingLeft: '10px',
          }}
          onClick={() => openEvent(event)}
        >
          <span style={{ textWrap: 'nowrap' }}>{event.name}</span>
          <EventIcon type={event.type} />
        </div>
      </Tooltip>
    );
  }

  if (compareDates(event.endDate, time)) {
    return (
      <Tooltip title={tooltipTitle}>
        <div
          style={{
            ...eventStyle,
            width: '97%',
            borderRadius: '0 4px 4px 0',
          }}
          onClick={() => openEvent(event)}
        />
      </Tooltip>
    );
  }

  if (!isDateInRange(event.startDate, event.endDate, time, false)) {
    return (
      <div
        style={{
          backgroundColor: 'transparent',
          width: '100%',
          height: eventStyle.height,
        }}
      />
    );
  }

  return null;
}

function VerticalEvent(props) {
  const { event, time, openEvent } = props;
  const startDate = moment(event.startDate).format('DD.MM.YYYY HH:mm');
  const endDate = moment(event.endDate).format('DD.MM.YYYY HH:mm');
  const tooltipTitle = `${event.name} | ${startDate} - ${endDate}`;

  const eventStyle = {
    cursor: 'pointer',
    // display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    gap: '5px',
    width: '30%',
    ...colorEvent(event.type),
  };

  if (
    compareDates(event.startDate, event.endDate, true)
    && compareDates(event.startDate, time, true)
  ) {
    return (
      <Tooltip title={tooltipTitle}>
        <div
          style={{
            ...eventStyle,
            borderRadius: '4px',
            alignSelf: 'center',
            top: '300%',
            position: 'absolute',
            height: 'calc(100% + 10px)',
          }}
          onClick={() => openEvent(event)}
        >
          {event.name}
          <EventIcon type={event.type} />
        </div>
      </Tooltip>
    );
  }

  if (
    isDateInRange(
      time,
      new Date(time.getTime() + 3599999),
      event.startDate,
      false,
      true,
    )
  ) {
    return (
      <Tooltip title={tooltipTitle}>
        <div
          style={{
            ...eventStyle,
            borderRadius: '4px 4px 0 0',
            marginTop: '10px',
            height: '100%',
            position: 'relative',
          }}
          onClick={() => openEvent(event)}
        >
          <span
            style={{
              display: 'inline-flex',
              alignItems: 'center',
              gap: '10px',
              textWrap: 'nowrap',
              fontSize: '1rem',
              position: 'absolute',
              textAlign: 'left',
              top: 0,
              transform: 'rotate(90deg)',
              transformOrigin: 'left bottom',
              zIndex: 1,
              left: 0,
            }}
          >
            {event.name}
            <EventIcon type={event.type} />
          </span>
        </div>
      </Tooltip>
    );
  }

  if (
    isDateInRange(
      time,
      new Date(time.getTime() + 3599999),
      event.endDate,
      false,
      true,
    )
  ) {
    return (
      <Tooltip title={tooltipTitle}>
        <div
          style={{
            ...eventStyle,
            borderRadius: '0 0 4px 4px',
            marginBottom: '10px',
          }}
          onClick={() => openEvent(event)}
        />
      </Tooltip>
    );
  }

  if (isDateInRange(event.startDate, event.endDate, time, true, true)) {
    return (
      <Tooltip title={tooltipTitle}>
        <div
          style={{
            ...eventStyle,
            height: '110%',
            position: 'relative',
          }}
          onClick={() => openEvent(event)}
        >
          {props.index === 0 && (
            <span
              style={{
                display: 'inline-flex',
                alignItems: 'center',
                gap: '10px',
                textWrap: 'nowrap',
                position: 'absolute',
                textAlign: 'left',
                top: '5em',
                transform: 'rotate(90deg)',
                zIndex: 1,
              }}
            >
              {event.name}
              <EventIcon type={event.type} />
            </span>
          )}
        </div>
      </Tooltip>
    );
  }

  if (!isDateInRange(event.startDate, event.endDate, time, false, true)) {
    return (
      <div
        style={{
          backgroundColor: 'transparent',
          width: eventStyle.width,
          height: eventStyle.height,
        }}
      />
    );
  }

  return null;
}

function isInWeek(object, week) {
  return !(
    new Date(object.startDate).getTime()
    > week[6].getTime() + 24 * 60 * 60 * 1000
    || new Date(object.endDate).getTime() < week[0].getTime()
  );
}

function isInDay(object, day) {
  return !(
    new Date(object.startDate).getTime()
    > day.getTime() + 24 * 60 * 60 * 1000
    || new Date(object.endDate).getTime() < day.getTime()
  );
}

const getDayOfWeek = (day, date) => {
  if (!date) {
    // eslint-disable-next-line no-param-reassign
    date = new Date();
  }
  const currentDay = date.getDay();
  const distance = day - currentDay;
  date.setDate(date.getDate() + distance);
  return date;
};

function Calendar(props) {
  const router = useRouter();
  const [type, setType] = useState('day');
  const [eventDialogOpen, setEventDialogOpen] = useState(false);
  const [eventDialog, setEventDialog] = useState({});
  const [startDate, setStartDate] = useState(new Date());

  const getDayStartDate = () => {
    const date = new Date(startDate);
    date.setHours(0, 0, 0, 0);
    return date;
  };

  const getWeekStartDate = () => {
    const date = getDayOfWeek(0, new Date(startDate));
    date.setHours(0, 0, 0, 0);
    return date;
  };

  const getMonthStartDate = () => {
    const date = new Date(startDate);
    date.setDate(1);
    date.setHours(0, 0, 0, 0);
    return date;
  };

  const [createEvent] = useMutation(CREATE_EVENT);
  const [editEvent] = useMutation(EDIT_EVENT);
  const [deleteEvent] = useMutation(DELETE_EVENT);

  const domainQuery = useQuery(GET_DOMAIN, {
    variables: {
      id: router.query.id,
      idOrganizationUsers: router.query.id,
      withSubDomains: true,
    },
    // onCompleted: eventDialog?.id
    //   ? (data) => setEventDialog({
    //     ...eventDialog,
    //     participants: data?.getOrganizationUsers.map(
    //       (item) => item.user.id,
    //     ),
    //   })
    //   : undefined,
  });

  if (domainQuery.loading) {
    return null;
  }

  console.log(domainQuery);
  const { sprints, events } = domainQuery.data.getDomain;

  async function openEvent(event) {
    const participants = event.participants
      ? event.participants.map((item) => item.id)
      : [];
    const dialogEvent = { ...event };
    dialogEvent.domainId = dialogEvent.domain.id;
    delete dialogEvent.domain;
    setEventDialog({
      ...dialogEvent,
      participants,
    });
    setEventDialogOpen(true);
  }

  let calendarTable = null;
  if (type === 'day') {
    const times = [];
    for (let i = 0; i < 24; i++) {
      times.push(
        new Date(getDayStartDate().getTime() + i * 60 * 60 * 1000),
      );
    }
    calendarTable = (
      <div style={{ display: 'flex' }}>
        <div style={{ flex: 1 }}>
          <table
            style={{ borderCollapse: 'collapse', width: '100%' }}
          >
            <tr>
              <td
                style={{
                  height: 36,
                  fontSize: '14px',
                  border: '1px solid #DAE5F4',
                  textAlign: 'center',
                  verticalAlign: 'middle',
                }}
              >
                Спринты
              </td>
              <td
                style={{
                  height: '100%',
                  fontSize: '14px',
                  border: '1px solid #DAE5F4',
                  textAlign: 'center',
                  gap: '10px',
                }}
              >
                <div
                  style={{
                    display: 'flex',
                    flexDirection: 'column',
                    margin: '-1px',
                  }}
                >
                  {sprints.map((sprint) => {
                    if (
                      !isDateInRange(
                        sprint.startDate,
                        sprint.endDate,
                        getDayStartDate(),
                        false,
                      )
                    ) {
                      return null;
                    }
                    const tooltipTitle = `${sprint.name
                    } | ${moment(sprint.startDate).format(
                      'DD.MM.YYYY',
                    )} - ${moment(sprint.endDate).format(
                      'DD.MM.YYYY',
                    )}`;
                    return (
                      <Tooltip
                        key={sprint.id}
                        title={tooltipTitle}
                      >
                        <div
                          style={{
                            backgroundColor:
                              '#f79244',
                            height: '25px',
                            display: 'flex',
                            justifyContent:
                              'center',
                            flexDirection: 'column',
                            margin: '5px',
                            borderRadius: '4px',
                            cursor: 'pointer',
                          }}
                          onClick={() => router.push({
                            pathname: `sprints/${sprint.id}`,
                            query: router.query,
                          })}
                        >
                          <span
                            style={{
                              color: 'white',
                            }}
                          >
                            {sprint.name}
                          </span>
                        </div>
                      </Tooltip>
                    );
                  })}
                </div>
              </td>
            </tr>
            {times.map((time, index) => {
              const isCurrentTime = new Date() >= time
                && new Date()
                < new Date(time.getTime() + 60 * 60 * 1000);

              const date = new Date(getDayStartDate());
              date.setHours(time.getHours());
              date.setMinutes(time.getMinutes());
              date.setSeconds(time.getSeconds());
              return (
                <tr key={index}>
                  <td
                    style={{
                      width: 90,
                      height: 36,
                      fontSize: '14px',
                      border: '1px solid #DAE5F4',
                      textAlign: 'center',
                      verticalAlign: 'middle',
                      color: isCurrentTime
                        ? '#687B98'
                        : '#A2BDE4',
                      backgroundColor: isCurrentTime
                        ? '#f0f5fb'
                        : undefined,
                    }}
                  >
                    {moment(time).format('HH:mm')}
                  </td>
                  <td
                    style={{
                      height: 36,
                      border: '1px solid #DAE5F4',
                      fontSize: '14px',
                      textAlign: 'right',
                      verticalAlign: 'top',
                      color: isCurrentTime
                        ? '#687B98'
                        : '#A2BDE4',
                      backgroundColor: isCurrentTime
                        ? '#f0f5fb'
                        : undefined,
                      display: 'flex',
                      gap: '10px',
                    }}
                  >
                    {events
                      .filter(
                        (event) => new Date(event.startDate)
                          < new Date(time).setHours(
                            23,
                            59,
                            59,
                            999,
                          )
                          && new Date(event.endDate)
                          > new Date(time).setHours(
                            0,
                            0,
                            0,
                            0,
                          ),
                      )
                      .sort(
                        (a, b) => new Date(a.endDate)
                          - new Date(b.endDate),
                      )
                      .map((event) => (
                        <VerticalEvent
                          key={event.id}
                          index={index}
                          event={event}
                          time={date}
                          openEvent={(e) => openEvent(e)}
                        />
                      ))}
                  </td>
                </tr>
              );
            })}
          </table>
        </div>
        <div>
          <LocalizationProvider
            dateAdapter={AdapterMoment}
            adapterLocale="ru"
          >
            <StaticDatePicker
              displayStaticWrapperAs="desktop"
              value={moment(getDayStartDate())}
              onChange={(newValue) => {
                setStartDate(new Date(newValue));
              }}
            />
          </LocalizationProvider>
        </div>
      </div>
    );
  } else if (type === 'week') {
    const dayOfWeeks = [];
    for (let i = 0; i < 7; i++) {
      dayOfWeeks.push(
        new Date(
          getWeekStartDate().getTime() + (i + 1) * 24 * 60 * 60 * 1000,
        ),
      );
    }
    const times = [];
    for (let i = 0; i < 24; i++) {
      times.push(
        new Date(getWeekStartDate().getTime() + i * 60 * 60 * 1000),
      );
    }
    calendarTable = (
      <table style={{ borderCollapse: 'collapse', overflow: 'hidden' }}>
        <tr>
          <td
            style={{
              width: 90,
              height: 36,
              fontSize: '14px',
              border: '1px solid #DAE5F4',
              textAlign: 'right',
              verticalAlign: 'top',
            }}
          />
          {dayOfWeeks.map((time, index) => {
            const isCurrentDay = moment(time).format('DD.MM.YYYY')
              === moment(new Date()).format('DD.MM.YYYY');
            return (
              <td
                key={index}
                style={{
                  width: 148,
                  height: 36,
                  fontSize: '14px',
                  border: '1px solid #DAE5F4',
                  textAlign: 'center',
                  verticalAlign: 'middle',
                  backgroundColor: isCurrentDay
                    ? '#f0f5fb'
                    : undefined,
                }}
              >
                <span
                  style={{
                    color: isCurrentDay
                      ? '#4B5A73'
                      : '#A2BDE4',
                  }}
                >
                  {moment(time).format('dd')}
                </span>
                &nbsp;
                <span style={{ color: '#4B5A73' }}>
                  {moment(time).format('D')}
                </span>
              </td>
            );
          })}
        </tr>
        <tr>
          <td
            style={{
              width: 90,
              height: 36,
              fontSize: '14px',
              border: '1px solid #DAE5F4',
              textAlign: 'center',
              verticalAlign: 'middle',
            }}
          >
            Спринты
          </td>
          {dayOfWeeks.map((date, index) => {
            const isCurrentDay = moment(date).format('DD.MM.YYYY')
              === moment(new Date()).format('DD.MM.YYYY');
            return (
              <td
                key={index}
                style={{
                  width: 148,
                  height: 36,
                  fontSize: '14px',
                  border: '1px solid #DAE5F4',
                  textAlign: 'center',
                  verticalAlign: 'middle',
                  backgroundColor: isCurrentDay
                    ? '#f0f5fb'
                    : undefined,
                }}
              >
                <div
                  style={{
                    display: 'flex',
                    flexDirection: 'column',
                    gap: '5px',
                    margin: '-1px',
                    padding: '10px 0',
                  }}
                >
                  {sprints
                    .filter((sp) => isInWeek(sp, dayOfWeeks))
                    .map((sprint) => (
                      <Sprint
                        key={sprint.id}
                        index={index}
                        sprint={sprint}
                        time={date}
                      />
                    ))}
                </div>
              </td>
            );
          })}
        </tr>
        {times.map((time, index) => (
          <tr key={index}>
            <td
              style={{
                width: 90,
                height: 36,
                fontSize: '14px',
                border: '1px solid #DAE5F4',
                textAlign: 'center',
                verticalAlign: 'middle',
                color: '#A2BDE4',
              }}
            >
              {moment(time).format('HH:mm')}
            </td>
            {dayOfWeeks.map((day, dayIndex) => {
              const isCurrentDay = moment(day).format('DD.MM.YYYY')
                === moment(new Date()).format('DD.MM.YYYY');
              const isCurrentTime = moment(time).format('HH')
                === moment(new Date()).format('HH');

              const date = new Date(day);
              date.setHours(time.getHours());
              date.setMinutes(time.getMinutes());
              date.setSeconds(time.getSeconds());
              return (
                <td
                  key={dayIndex}
                  style={{
                    width: 148,
                    height: 36,
                    fontSize: '14px',
                    border: '1px solid #DAE5F4',
                    backgroundColor:
                      isCurrentDay && isCurrentTime
                        ? '#f0f5fb'
                        : undefined,
                  }}
                >
                  <div
                    style={{
                      display: 'flex',
                      flexWrap: 'nowrap',
                      justifyContent: 'center',
                      gap: '5px',
                      width: '100%',
                      height: '100%',
                    }}
                  >
                    {events
                      .filter((event) => isDateInRange(
                        event.startDate,
                        event.endDate,
                        day,
                      ))
                      .sort(
                        (a, b) => new Date(a.endDate)
                          - new Date(b.endDate),
                      )
                      .map((event) => (
                        <VerticalEvent
                          key={event.id}
                          index={index}
                          event={event}
                          time={date}
                          openEvent={(e) => openEvent(e)}
                        />
                      ))}
                  </div>
                </td>
              );
            })}
          </tr>
        ))}
      </table>
    );
  } else if (type === 'month') {
    const weeks = [];
    for (let i = 0; i < 6; i++) {
      const firstDayOfWeek = new Date(
        getMonthStartDate().getTime() + i * 7 * 24 * 60 * 60 * 1000,
      );
      if (
        i > 0
        && firstDayOfWeek.getMonth() !== getMonthStartDate().getMonth()
      ) {
        break;
      }
      const days = [];
      for (let j = 0; j < 7; j++) {
        days.push(
          new Date(firstDayOfWeek.getTime() + j * 24 * 60 * 60 * 1000),
        );
      }
      weeks.push(days);
    }

    calendarTable = (
      <table style={{ borderCollapse: 'collapse', overflow: 'hidden' }}>
        {weeks.map((week, weekIndex) => (
          <tr key={weekIndex}>
            {week.map((day, dayIndex) => {
              const isCurrentDay = moment(day).format('DD.MM.YYYY')
                === moment(new Date()).format('DD.MM.YYYY');
              return (
                <td
                  key={dayIndex}
                  style={{
                    width: 160,
                    height: 160,
                    boxSizing: 'border-box',
                    fontSize: '14px',
                    border: '1px solid #DAE5F4',
                    textAlign: 'right',
                    verticalAlign: 'top',
                    padding: 0,
                    color:
                      day.getMonth()
                        === getMonthStartDate().getMonth()
                        ? '#4B5A73'
                        : '#B5CAE9',
                    backgroundColor: isCurrentDay
                      ? '#f0f5fb'
                      : undefined,
                  }}
                >
                  <span style={{ padding: 8 }}>
                    {moment(day).format('D')}
                  </span>
                  <div
                    style={{
                      display: 'flex',
                      flexDirection: 'column',
                      gap: '5px',
                      padding: '10px 0',
                    }}
                  >
                    <div
                      style={{
                        display: 'flex',
                        flexDirection: 'column',
                        gap: '5px',
                      }}
                    >
                      {sprints
                        ?.filter((sp) => isInWeek(sp, week))
                        .map((sprint) => (
                          <Sprint
                            key={sprint.id}
                            index={dayIndex}
                            sprint={sprint}
                            time={day}
                          />
                        ))}
                    </div>
                    <div
                      style={{
                        display: 'flex',
                        flexDirection: 'column',
                        gap: '5px',
                        height: '100%',
                      }}
                    >
                      {events
                        ?.filter((ev) => isInDay(ev, day))
                        .map((event) => (
                          <Event
                            key={event.id}
                            index={dayIndex}
                            event={event}
                            time={day}
                            openEvent={(e) => openEvent(e)}
                          />
                        ))}
                    </div>
                  </div>
                </td>
              );
            })}
          </tr>
        ))}
      </table>
    );
  }

  const formatDate = () => {
    if (type === 'day') {
      return (
        <>
          <span style={{ color: '#2E394D' }}>
            {moment(getDayStartDate()).format('D MMMM,')}
          </span>
          &nbsp;
          <span style={{ color: '#C7D7EF' }}>
            {moment(getDayStartDate()).format('YYYY')}
          </span>
        </>
      );
    }
    if (type === 'week') {
      return (
        <>
          <span style={{ color: '#2E394D' }}>
            {moment(getWeekStartDate()).format('MMMM,')}
          </span>
          &nbsp;
          <span style={{ color: '#C7D7EF' }}>
            {moment(getWeekStartDate()).format('YYYY')}
          </span>
        </>
      );
    }
    if (type === 'month') {
      return (
        <>
          <span style={{ color: '#2E394D' }}>
            {moment(getMonthStartDate()).format('MMMM,')}
          </span>
          &nbsp;
          <span style={{ color: '#C7D7EF' }}>
            {moment(getMonthStartDate()).format('YYYY')}
          </span>
        </>
      );
    }
    return null;
  };

  return (
    <>
      <Head>
        <title>Календарь</title>
      </Head>
      <div style={{ display: 'flex' }}>
        <LeftSubPanel title="Домены">
          <DomainLeftPanel selected="calendar" />
        </LeftSubPanel>
        <SimpleDialog
          open={eventDialogOpen}
          title={
            eventDialog.id
              ? 'Редактирование мероприятия'
              : 'Создание мероприятия'
          }
          acceptText={eventDialog.id ? 'Сохранить' : 'Создать'}
          cancelText="Отмена"
          onCancel={() => setEventDialogOpen(false)}
          onAccept={async () => {
            try {
              const { id } = eventDialog;
              if (eventDialog.id) {
                await editEvent({
                  variables: {
                    editEventId: id,
                    event: {
                      domainId: eventDialog.domainId,
                      name: eventDialog.name,
                      startDate: new Date(
                        eventDialog.startDate,
                      ).toISOString(),
                      endDate: new Date(
                        eventDialog.endDate,
                      ).toISOString(),
                      facilitatorUserId:
                        eventDialog.facilitatorUserId,
                      participants:
                        eventDialog.participants,
                      type: eventDialog.type,
                    },
                  },
                });
                setEventDialogOpen(false);
                domainQuery.refetch();
              } else {
                await createEvent({
                  variables: {
                    event: {
                      domainId: eventDialog.domainId,
                      name: eventDialog.name,
                      startDate: eventDialog.startDate,
                      endDate: eventDialog.endDate,
                      facilitatorUserId: props.user.id,
                      participants:
                        eventDialog.participants,
                      type: eventDialog.type,
                    },
                  },
                });
                setEventDialogOpen(false);
                domainQuery.refetch();
              }
            } catch (e) {
              console.error(e);
            }
          }}
          externalActions={
            eventDialog.id ? (
              <Button
                color="tertiary"
                onClick={async () => {
                  await deleteEvent({
                    variables: {
                      id: eventDialog.id,
                    },
                  });
                  setEventDialogOpen(false);
                  domainQuery.refetch();
                }}
              >
                Удалить
              </Button>
            ) : null
          }
        >
          <TextField
            label="Название"
            variant="outlined"
            fullWidth
            margin="normal"
            value={eventDialog.name}
            onChange={(event) => setEventDialog({
              ...eventDialog,
              name: event.target.value,
            })}
          />
          <TextField
            label="Дата начала"
            type="datetime-local"
            variant="outlined"
            fullWidth
            margin="normal"
            InputLabelProps={{
              shrink: true,
            }}
            value={moment(eventDialog.startDate).format(
              'YYYY-MM-DDTHH:mm',
            )}
            onChange={(event) => setEventDialog({
              ...eventDialog,
              startDate: new Date(event.target.value),
            })}
          />
          <TextField
            label="Дата окончания"
            type="datetime-local"
            variant="outlined"
            fullWidth
            inputProps={{
              min: eventDialog.startDate,
            }}
            disabled={!eventDialog.startDate}
            margin="normal"
            InputLabelProps={{
              shrink: true,
            }}
            value={moment(eventDialog.endDate).format(
              'YYYY-MM-DDTHH:mm',
            )}
            onChange={(event) => setEventDialog({
              ...eventDialog,
              endDate: new Date(event.target.value),
            })}
          />

          <FormControl variant="outlined" fullWidth margin="normal">
            <InputLabel>Домен</InputLabel>
            <Select
              error={checkEmpty(eventDialog.domainId)}
              color="secondary"
              label="Домен"
              onChange={async (event) => {
                setEventDialog({
                  ...eventDialog,
                  domainId: event.target.value,
                });
              }}
              value={eventDialog.domainId}
            >
              {domainQuery.data?.getDomainsOfOrganization?.map((domain) => (
                <MenuItem key={domain.id} value={domain.id}>
                  {domain.name}
                </MenuItem>
              ))}
            </Select>
          </FormControl>
          <FormControl variant="outlined" fullWidth margin="normal">
            <InputLabel>Тип</InputLabel>
            <Select
              value={eventDialog.type}
              onChange={(event) => setEventDialog({
                ...eventDialog,
                type: event.target.value,
              })}
              label="Тип"
            >
              {[
                'planning',
                'review',
                'retrospective',
                'management',
                'choiceForRole',
              ].map((_type) => (
                <MenuItem
                  key={_type}
                  value={_type}
                  style={{ display: 'flex', gap: 8 }}
                >
                  <EventIcon type={_type} />
                  {getEventName(_type)}
                </MenuItem>
              ))}
            </Select>
          </FormControl>
          <FormControl variant="outlined" fullWidth margin="normal">
            <InputLabel shrink>Участники</InputLabel>
            <Select
              multiple
              displayEmpty
              fullWidth
              variant="outlined"
              label="Участники"
              onChange={(e) => setEventDialog({
                ...eventDialog,
                participants: e.target.value,
              })}
              value={eventDialog.participants || []}
              renderValue={(selected) => {
                if (selected.length === 0) {
                  return <span>Участников нет</span>;
                }
                return selected
                  .map(
                    (p) => domainQuery.data?.getOrganizationUsers.find(
                      (user) => user.user.id === p,
                    )?.user.name,
                  )
                  .join(', ');
              }}
            >
              {domainQuery.data?.getOrganizationUsers.map(
                (participant) => (
                  <MenuItem
                    key={participant.user.id}
                    value={participant.user.id}
                  >
                    {participant.user.name}
                  </MenuItem>
                ),
              )}
            </Select>
          </FormControl>
        </SimpleDialog>
        <div style={{ flex: 1, padding: 36 }}>
          <div
            style={{
              display: 'flex',
              justifyContent: 'space-between',
              alignItems: 'center',
              marginBottom: 24,
            }}
          >
            <div style={{ display: 'flex', alignItems: 'center' }}>
              <IconButton
                onClick={() => {
                  if (type === 'day') {
                    const newDate = new Date(
                      getDayStartDate().getTime(),
                    );
                    newDate.setDate(newDate.getDate() - 1);
                    setStartDate(newDate);
                  } else if (type === 'week') {
                    const newDate = new Date(
                      getWeekStartDate().getTime(),
                    );
                    newDate.setDate(newDate.getDate() - 7);
                    setStartDate(newDate);
                  } else if (type === 'month') {
                    const newDate = new Date(
                      getMonthStartDate().getTime(),
                    );
                    newDate.setMonth(
                      newDate.getMonth() - 1,
                    );
                    setStartDate(newDate);
                  }
                }}
              >
                <NavigateBeforeIcon />
              </IconButton>
              <span
                style={{
                  display: 'flex',
                  alignItems: 'center',
                  fontSize: '16px',
                }}
              >
                {formatDate()}
              </span>
              <IconButton
                onClick={() => {
                  if (type === 'day') {
                    const newDate = new Date(
                      getDayStartDate().getTime(),
                    );
                    newDate.setDate(newDate.getDate() + 1);
                    setStartDate(newDate);
                  } else if (type === 'week') {
                    const newDate = new Date(
                      getWeekStartDate().getTime(),
                    );
                    newDate.setDate(newDate.getDate() + 7);
                    setStartDate(newDate);
                  } else if (type === 'month') {
                    const newDate = new Date(
                      getMonthStartDate().getTime(),
                    );
                    newDate.setMonth(
                      newDate.getMonth() + 1,
                    );
                    setStartDate(newDate);
                  }
                }}
              >
                <NavigateNextIcon />
              </IconButton>
            </div>
            <div style={{ display: 'flex', gap: '20px' }}>
              <Button
                variant="contained"
                onClick={async () => {
                  setEventDialog({
                    name: '',
                    startDate: new Date(),
                    endDate: new Date(
                      new Date().getTime()
                      + 2 * 60 * 60 * 1000,
                    ),
                    type: 'planning',
                    participants: [],
                    domainId: '',
                  });
                  setEventDialogOpen(true);
                }}
              >
                СОЗДАТЬ МЕРОПРИЯТИЕ
              </Button>
              <Select
                value={type}
                onChange={(e) => {
                  setType(e.target.value);
                }}
                style={{ marginRight: 54 }}
                variant="standard"
                color="primary"
              >
                {viewTypes.map((currentType) => (
                  <MenuItem
                    key={currentType.value}
                    value={currentType.value}
                  >
                    {currentType.title}
                  </MenuItem>
                ))}
              </Select>
            </div>
          </div>
          {calendarTable}
        </div>
      </div>
    </>
  );
}

export default Calendar;
