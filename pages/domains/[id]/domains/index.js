import { useState } from 'react';
import { gql, useQuery, useMutation } from '@apollo/client';
import Head from 'next/head';

import {
  Accordion as MuiAccordion,
  AccordionSummary as MuiAccordionSummary,
  AccordionDetails as MuiAccordionDetails,
  Typography,
  Box,
  Select,
  MenuItem,
  InputLabel,
  TextField,
} from '@mui/material';

import { useRouter } from 'next/router';
import { RadioButtonUnchecked, SupervisedUserCircle } from '@mui/icons-material';
import { useSnackbar } from 'notistack';
import LeftSubPanel from '../../../../components/LeftSubPanel';
import DomainLeftPanel from '../../../../components/DomainLeftPanel';
import DomainCreator from '../../../../components/domain-creator';
import DomainMap from '../../../../components/domainMap';
import S3Avatar from '../../../../components/S3Avatar';
import SimpleDialog from '../../../../components/SimpleDialog';

import Triangle from '../../../../assets/triangle.svg';
import Home from '../../../../assets/home.svg';
import Cross from '../../../../assets/cross.svg';
import Edit from '../../../../assets/edit.svg';
import Menu from '../../../../assets/menu.svg';
import PlusSquare from '../../../../assets/plus-squared.svg';
import UserPlus from '../../../../assets/user-plus.svg';
import { domainsToHierarchy } from '../../../../components/common';

const GET_DOMAINS = gql`
query getDomains($domainId: ID!) {
  getDomainsOfOrganization(domainId: $domainId) {
    id
    name
    domainType
    parentDomain {
      id
    }
    participants {
      name
      id
    }
  }
  getUsersOfOrganization(domainId: $domainId) {
    name
    id
  }
}`;

const ADD_USERS = gql`
mutation AddUsersToDomain($domainId: ID!, $userIds: [ID]!) {
  addUsersToDomain(domainId: $domainId, userIds: $userIds) {
    id
  }
}`;

const REMOVE_USERS = gql`mutation RemoveUsersFromDomain($domainId: ID!, $userIds: [ID]!) {
  removeUsersFromDomain(domainId: $domainId, userIds: $userIds) {
    id
  }
}`;

const DELETE_DOMAIN = gql`
mutation DeleteDomain($deleteDomainId: ID!) {
  deleteDomain(id: $deleteDomainId)
}`;

const EDIT_DOMAIN = gql`
  mutation($editDomainId: ID!, $domain: DomainUpdateInput!) {
    editDomain(id: $editDomainId, domain: $domain) {
      id
      name
    }
  }
`;

const AccStyle = {
  width: '100%',
  background: 'none',
  boxShadow: 'none',
  margin: '0!important',
  border: 'none',
  '&:before': {
    display: 'none',
  },
};

const AccSumStyle = {
  width: '100%',
  background: 'none',
  boxShadow: 'none',
  flexDirection: 'row-reverse',
  // height: '48px!important',
  minHeight: '48px!important',
  '& .MuiAccordionSummary-content': {
    alignItems: 'center',
    marginBottom: '8px',
  },
  '& .MuiAccordionSummary-expandIconWrapper': {
    transform: 'rotate(-90deg)',
  },
  '& .MuiAccordionSummary-expandIconWrapper.Mui-expanded': {
    transform: 'rotate(0deg)',
  },
};

function User({
  user, domain, setUserDialog, userDialog,
}) {
  return (
    <Box
      sx={{
        display: 'flex', alignItems: 'center', mb: 1, mt: '4px', ml: '35px',
      }}
    >
      <Menu style={{ marginRight: '12px' }} />
      <PlusSquare style={{ marginRight: '10px' }} />
      <S3Avatar name={user.name} />
      <Box sx={{ marginLeft: '8px' }}>
        <Typography
          variant="body3"
          sx={{ alignSelf: 'flex-start' }}
        >
          {user.name}
        </Typography>
        <br />
        <Typography
          variant="body2"
          sx={{ alignSelf: 'flex-end' }}
        />
      </Box>
      <Cross
        style={{ marginLeft: 'auto', cursor: 'pointer', zIndex: 999 }}
        onClick={(e) => {
          setUserDialog({
            ...userDialog,
            open: true,
            deletion: true,
            domain,
            user,
          });
          e.stopPropagation();
        }}
      />
    </Box>
  );
}

function DomainEdit(props) {
  return (
    <Edit
      onClick={(e) => {
        props.setEditDomainDialog({
          open: true, domain: props.domain, name: props.domain.name,
        });
        e.stopPropagation();
      }}
      style={{
        stroke: 'rgba(0, 0, 0, 0.87)',
        zoom: '74%',
        paddingLeft: 8,
      }}
    />
  );
}

function DomainList({
  domain, setEditDomainDialog, setUserDialog, userDialog, setDomainDialog, level,
}) {
  let Icon = Home;
  if (domain.domainType === 'circle') {
    Icon = RadioButtonUnchecked;
  }
  if (domain.domainType === 'role') {
    Icon = SupervisedUserCircle;
  }
  return (
    <MuiAccordion sx={{ ...AccStyle, paddingLeft: '10px' }} key={domain.id}>
      <MuiAccordionSummary sx={AccSumStyle} expandIcon={<Triangle />}>
        <Icon style={{ margin: '0px 10px' }} />
        <Typography style={{ flex: 1 }}>
          {domain.name}
          <DomainEdit domain={domain} setEditDomainDialog={setEditDomainDialog} />
        </Typography>
        <div style={{ width: 48, display: 'flex', alignItems: 'center' }}>
          <UserPlus
            style={{ marginLeft: 'auto', marginRight: '10px' }}
            onClick={(e) => {
              setUserDialog({
                ...userDialog, open: true, deletion: false, domain,
              });
              e.stopPropagation();
            }}
          />
          <Cross onClick={(e) => {
            setDomainDialog({ open: true, deletion: true, domain });
            e.stopPropagation();
          }}
          />
        </div>
      </MuiAccordionSummary>

      <MuiAccordionDetails sx={{ padding: '0px 16px 0px 0px' }}>
        {domain.participants.map((user) => (
          <User
            key={user.id}
            user={user}
            domain={domain}
            setUserDialog={setUserDialog}
            userDialog={userDialog}
          />
        ))}
        {domain.subDomains.map((subDomain) => (
          <DomainList
            key={subDomain.id}
            domain={subDomain}
            setEditDomainDialog={setEditDomainDialog}
            setUserDialog={setUserDialog}
            userDialog={userDialog}
            setDomainDialog={setDomainDialog}
            level={level + 1}
          />
        ))}
      </MuiAccordionDetails>
    </MuiAccordion>
  );
}

function Domains(props) {
  const [userDialog, setUserDialog] = useState({
    open: false, user: {}, domain: {}, deletion: false,
  });

  const [domainDialog, setDomainDialog] = useState({
    open: false, domain: {},
  });

  const [editDomainDialog, setEditDomainDialog] = useState({
    open: false, domain: {}, name: '',
  });

  const [domainsRefetch, setDomainsRefetch] = useState(0);

  const router = useRouter();

  const allDomains = useQuery(GET_DOMAINS, {
    variables: {
      domainId: router.query.id,
    },
  });
  const [addUsers] = useMutation(ADD_USERS);
  const [removeUsers] = useMutation(REMOVE_USERS);
  const [deleteDomain] = useMutation(DELETE_DOMAIN);
  const [editDomain] = useMutation(EDIT_DOMAIN);

  const { enqueueSnackbar } = useSnackbar();

  async function updateUsers({ domain, user, deletion }) {
    const mutation = deletion ? removeUsers : addUsers;

    await mutation({
      variables: {
        domainId: domain.id,
        userIds: [user.id],
      },
    });

    allDomains.refetch();
    props.refetchUser();
    setUserDialog({
      ...userDialog,
      open: false,
      deletion: false,
      user: {},
    });
  }

  if (allDomains.loading) return null;

  const domains = domainsToHierarchy(allDomains.data.getDomainsOfOrganization);
  console.log(domains);

  return (
    <>
      <Head>
        <title>Карта доменов</title>
      </Head>
      <Box style={{ display: 'flex' }}>
        <LeftSubPanel title="Домены">
          <DomainLeftPanel selected="domainMap" />
        </LeftSubPanel>

        <SimpleDialog
          open={userDialog.open}
          title={`${userDialog.deletion ? 'Удаление' : 'Добавление'} пользователя`}
          text={`Домен: ${userDialog.domain.name}`}
          acceptText={userDialog.deletion ? 'Удалить' : 'Добавить'}
          cancelText="Отмена"
          onCancel={() => setUserDialog({ ...userDialog, open: false })}
          onAccept={async () => updateUsers(userDialog)}
        >
          {!userDialog.deletion
              && (
              <>
                <InputLabel>
                  Пользователь:
                </InputLabel>
                <Select
                  value={userDialog.user.id || ''}
                  variant="outlined"
                  size="small"
                  fullWidth
                  onChange={
                    (e) => setUserDialog(
                      { ...userDialog, user: { ...userDialog.user, id: e.target.value } },
                    )
                  }
                >
                  {allDomains.data?.getUsersOfOrganization.map((user) => (
                    (!userDialog.domain.participants?.some((elem) => elem.id === user.id)
                      && (
                      <MenuItem key={user.id} value={user.id}>
                        {user.name}
                      </MenuItem>
                      )
                    )
                  ))}
                </Select>
              </>
              )}
        </SimpleDialog>
        <SimpleDialog
          open={domainDialog.open}
          title="Удаление домена"
          text="Внимание! При удалении данного домена, все дочерние домены будут также удалены"
          acceptText="Удалить"
          cancelText="Отмена"
          onCancel={() => setDomainDialog({ ...domainDialog, open: false })}
          onAccept={async () => {
            try {
              const deleteIds = domainDialog.domain.subDomains?.map((domain) => domain.id) || [];
              deleteIds.push(domainDialog.domain.id);

              try {
                const deletePromises = deleteIds.map((id) => deleteDomain({
                  variables: {
                    deleteDomainId: id,
                  },
                }));

                await Promise.all(deletePromises);
              } catch {
                enqueueSnackbar('Доступ запрещен', { variant: 'error' });
              }

              enqueueSnackbar('Домен успешно удален', { variant: 'success' });
              if (domainDialog.domain.id === router.query.id) {
                router.push('/');
                return;
              }

              allDomains.refetch();
              setDomainsRefetch((refetch) => refetch + 1);
              setDomainDialog({
                ...domainDialog,
                open: false,
              });
            } catch (e) {
              console.error(e);
            }
          }}
        />
        <SimpleDialog
          open={editDomainDialog.open}
          title="Переименование домена"
          disabled={editDomainDialog.name === ''}
          acceptText="Переименовать"
          cancelText="Отмена"
          onCancel={() => setEditDomainDialog({ ...editDomainDialog, open: false })}
          onAccept={async () => {
            try {
              await editDomain({
                variables: {
                  editDomainId: editDomainDialog.domain.id,
                  domain: { name: editDomainDialog.name },
                },
              });
              allDomains.refetch();
              setDomainsRefetch((refetch) => refetch + 1);
              setEditDomainDialog({
                ...editDomainDialog,
                open: false,
              });
            } catch (e) {
              console.error(e);
            }
          }}
        >
          <TextField
            variant="standard"
            fullWidth
            label="Название"
            value={editDomainDialog.name}
            onChange={(e) => setEditDomainDialog({ ...editDomainDialog, name: e.target.value })}
          />
        </SimpleDialog>
        <Box style={{ flex: 1, overflow: 'hidden' }}>
          <DomainMap domains={domains} />
        </Box>
        <Box
          style={{
            width: 315,
            backgroundColor: '#DAE5F4',
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
            overflow: 'auto',
          }}
        >
          {domains.map((domain) => (
            <DomainList
              key={domain.id}
              domain={domain}
              setEditDomainDialog={setEditDomainDialog}
              setUserDialog={setUserDialog}
              userDialog={userDialog}
              setDomainDialog={setDomainDialog}
              level={0}
            />
          ))}

          <Box
            sx={{
              width: '100%',
              padding: '10px 0px',
              borderTop: '0.5px solid #859CBE',
              borderBottom: '0.5px solid #859CBE',
              display: 'inline-flex',
              alignItems: 'center',
              justifyContent: 'space-between',
            }}
          >
            <span style={{ marginLeft: '20px' }}>Новый круг</span>

            <DomainCreator
              refetch={domainsRefetch}
              user={props.user}
              refetchUser={props.refetchUser}
              organization={false}
              plus={{ marginRight: 12 }}
            />
          </Box>
          <Box
            sx={{
              width: '100%',
              padding: '10px 0px',
              borderBottom: '0.5px solid #859CBE',
              display: 'inline-flex',
              alignItems: 'center',
              justifyContent: 'space-between',
            }}
          >
            <span style={{ marginLeft: '20px' }}>Новая роль</span>

            <DomainCreator
              refetch={domainsRefetch}
              user={props.user}
              refetchUser={props.refetchUser}
              // eslint-disable-next-line jsx-a11y/aria-role
              role
              plus={{ marginRight: 12 }}
            />
          </Box>
        </Box>
      </Box>
    </>
  );
}

export default Domains;
