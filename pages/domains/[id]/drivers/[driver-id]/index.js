import Head from 'next/head';
import Link from 'next/link';
import moment from 'moment';
import { useRouter } from 'next/router';
import { useQuery, gql } from '@apollo/client';
import {
  Box,
  IconButton,

  useTheme,
} from '@mui/material';
import Edit from '../../../../../assets/edit.svg';

import Close from '../../../../../assets/close.svg';
import LeftSubPanel from '../../../../../components/LeftSubPanel';
import DomainLeftPanel from '../../../../../components/DomainLeftPanel';

import S3Avatar from '../../../../../components/S3Avatar';

import CommentPreviewList from '../../../../../components/CommentPreviewList';
import CommentCreator from '../../../../../components/CommentCreator';
import MarkdownPreviewLoaded from '../../../../../components/MarkdownPreviewLoaded';
import { driverStates } from '../../../../../components/common';

const GET_DRIVER = gql`
  query getDriver($id: ID!) {
    getDriver(id: $id) {
      id
      name
      description
      tensions {
        id
        name
        domain {
          id
        }
      }
      createdAt
      authorUser {
        id
        name
      }
      domain {
        name
        id
      }
      driverEffect
      driverState
      comments {
        id
        author {
          id
          name
        }
        description
        createdAt
      }
    }
  }
`;

const styles = {
  fields: {
    '&': { width: '50%', paddingTop: '10px' },
    '& > div': {
      padding: '10px 0px',
      display: 'flex',
      alignItems: 'center',
      height: '20px',
    },
  },
  fieldContent: {
    minWidth: '348px',
    minHeight: '30px',
    border: '2px solid #DAE5F4',
    borderRadius: '5px',
    fontSize: '14px',
    display: 'flex',
    alignItems: 'center',
    padding: '10px',
    boxSizing: 'border-box',
  },
  fieldTitle: {
    color: '#A2BDE4',
    paddingRight: 4,
    fontSize: '14px',
  },
  link: {
    cursor: 'pointer',
    textDecoration: 'underline',
    color: '#4B5A73',
  },
  description: {
    border: '2px solid #A2BDE4',
    borderRadius: '5px',
    padding: '20px',
  },
  edit: {
    textAlign: 'right',
  },
  rightBlock: {
    padding: '12px 24px',
    background: '#4B5A73',
    color: '#A2BDE4',
    borderRadius: '5px',
    flex: 1,
    fontSize: '14px',
  },
  rightBlockText: {
    color: '#A2BDE4',
  },
  sprint: {
    padding: '0px 10px',
  },
  header: { fontSize: '14px', color: '#4B5A73' },
};

export default function Driver(props) {
  const router = useRouter();
  const { 'driver-id': id } = router.query;
  const { data, loading, refetch } = useQuery(GET_DRIVER, {
    variables: { id },
  });
  const theme = useTheme();
  if (loading) {
    return null;
  }

  const driver = data.getDriver;
  const comments = [...driver.comments];
  comments.sort((a, b) => moment(a.createdAt) - moment(b.createdAt));
  return (
    <>
      <Head>
        <title>Потребность</title>
      </Head>
      <Box sx={{ display: 'flex' }}>
        <LeftSubPanel title="Домены">
          <DomainLeftPanel selected="drivers" />
        </LeftSubPanel>

        <Box sx={{
          flex: 1, ml: 4, display: 'flex', flexDirection: 'column',
        }}
        >
          <Box sx={{ display: 'flex', justifyContent: 'space-between' }}>
            <Box
              style={{
                display: 'flex',
                alignItems: 'center',
                width: '100%',
                borderBottom: `${theme.palette.primary.main} solid 2px`,
              }}
            >
              <span style={{ width: '100%', fontSize: '20px', fontWeight: 700 }}>{driver.name}</span>
              <IconButton onClick={() => router.push(`${router.asPath}/edit`)}>
                <Edit
                  style={{ stroke: theme.palette.primary.main }}
                />
              </IconButton>

              <IconButton onClick={() => router.back()}>
                <Close
                  style={{ stroke: theme.palette.primary.main }}
                />
              </IconButton>
            </Box>
          </Box>

          <Box sx={{ display: 'flex', flex: 1 }}>
            <Box sx={{ flex: 1 }}>
              <Box sx={{ mr: 4 }}>
                <Box sx={{
                  display: 'grid',
                  mt: 2.75,
                  gridTemplateColumns: 'repeat(3, 1fr)',
                  alignItems: 'center',
                  gap: 1,
                  fontSize: 14,
                }}
                >
                  <div>
                    <span style={styles.fieldTitle}>
                      ID
                    </span>
                    <span style={styles.fieldValue}>
                      {driver.id}
                    </span>
                  </div>
                  <div style={{ display: 'flex', alignItems: 'center' }}>
                    <span style={styles.fieldTitle}>
                      Домен
                    </span>
                    <span
                      style={styles.link}
                      onClick={() => router.push(`/domains/${router.query.id}`)}
                    >
                      {driver.domain?.name}
                    </span>
                    <span>
                      <S3Avatar name={driver.domain?.name} size={30} sx={{ ml: 1 }} noBorder color="#687B98" />
                    </span>
                  </div>
                  <div>
                    <span style={styles.fieldTitle}>
                      Факты
                    </span>
                    <span>
                      {driver.tensions?.map((linkedTension, tensionKey) => (
                        <Link key={tensionKey} href={`/domains/${linkedTension.domain.id}/tensions/${linkedTension.id}`}>
                          <span>
                            <span style={styles.link}>{linkedTension.name}</span>
                            {tensionKey !== driver.tensions.length - 1 && ', '}
                          </span>
                        </Link>
                      ))}
                    </span>
                  </div>
                  <div style={{ display: 'flex', alignItems: 'center' }}>
                    <span style={styles.fieldTitle}>
                      Создатель
                    </span>
                    {driver.authorUser?.name || 'Создателя нет'}
                    <span>
                      <S3Avatar name={driver.authorUser?.name} size={30} sx={{ ml: 1 }} tooltip />
                    </span>
                  </div>
                  <div style={{ display: 'flex', alignItems: 'center' }}>
                    <span style={styles.fieldTitle}>
                      Эффект
                    </span>
                    {driver.driverEffect}
                  </div>
                  <div style={{ display: 'flex', alignItems: 'center' }}>
                    <span style={styles.fieldTitle}>
                      Состояние
                    </span>
                    {driverStates[driver.driverState]?.label}
                  </div>
                </Box>
                <span style={{ ...styles.fieldContent, margin: 0, padding: 0 }}>
                  <MarkdownPreviewLoaded value={driver.description || 'Описания нет'} />
                </span>
                <div style={{ marginTop: 10 }}>
                  <span style={styles.fieldTitle}>
                    Комментарии
                  </span>
                  <div style={styles.commentsList}>
                    {comments && comments.length > 0 ? (
                      comments.map((comment) => (
                        <CommentPreviewList
                          comment={comment}
                          key={comment.id}
                          user={props.user}
                          refetch={refetch}
                        />
                      ))
                    ) : (
                      <div>
                        Пока нет комментариев
                      </div>
                    )}
                    <CommentCreator entity={{ driverId: driver.id }} refetch={refetch} />
                  </div>
                </div>
              </Box>
            </Box>

          </Box>
        </Box>

      </Box>

    </>
  );
}
