import Head from 'next/head';
import {
  Box,
  Button,
  MenuItem,
  Select,
  TextField,
  useTheme,
  IconButton,
} from '@mui/material';
import { useQuery, gql, useMutation } from '@apollo/client';
import { useState } from 'react';
import { useRouter } from 'next/router';
import { checkEmpty, driverStates } from '../../../../components/common';
import LeftSubPanel from '../../../../components/LeftSubPanel';
import DomainLeftPanel from '../../../../components/DomainLeftPanel';

import Delete from '../../../../assets/delete.svg';
import Close from '../../../../assets/close.svg';
import SimpleDialog from '../../../../components/SimpleDialog';
import MarkdownEditorLoaded from '../../../../components/MarkdownEditorLoaded';

const GET_TENSIONS = gql`
  query getTensionsOfOrganization($domainId: ID!) {
    getTensionsOfOrganization(domainId: $domainId) {
      id 
      name
    }
  }
`;

const CREATE_DRIVER = gql`
  mutation createDriver($driver: DriverInput!) {
    createDriver(driver: $driver) {
      id
    }
  }
`;

const EDIT_DRIVER = gql`
  mutation editDriver($id: ID!, $driver: DriverInput!) {
    editDriver(id: $id, driver: $driver) {
      id
    }
  }
`;

const DELETE_DRIVER = gql`
  mutation deleteDriver($id: ID!) {
    deleteDriver(id: $id)
  }
`;

const GET_DRIVER = gql`
  query getDriver($id: ID!) {
    getDriver(id: $id) {
      id
      name
      description
      tensions {
        id
        name
      }
      createdAt
      authorUser {
        id
        name
      }
      domain {
        name
        id
      }
      driverEffect
      driverState
    }
  }
`;

const GET_DOMAINS = gql`
  query getDomainsOfOrganization($domainId: ID!) {
    getDomainsOfOrganization(domainId: $domainId) {
      id
      name
    }
  }
`;

const styles = {
  fields: {
    '&': { paddingTop: '10px' },
    '& > div': {
      padding: '10px 0px',
      display: 'flex',
      alignItems: 'center',
      height: '20px',
    },
  },
  fieldTitle: {
    color: '#A2BDE4',
    paddingRight: 4,
    fontSize: '14px',
  },
  description: {
    border: '2px solid #A2BDE4',
    borderRadius: '5px',
    padding: '20px',
  },
  image: {
    marginLeft: '4px',
    borderRadius: '50%',
    borderWidth: '2px',
    borderStyle: 'solid',
    borderColor: (theme) => theme.palette.primary.main,
  },
};

export default function Driver() {
  const theme = useTheme();

  const router = useRouter();

  const isNew = () => !router.query['driver-id'];

  const domainId = router.query.id;
  const tensions = useQuery(GET_TENSIONS, { variables: { domainId } });
  const domains = useQuery(GET_DOMAINS, { variables: { domainId } });

  const [save] = useMutation(CREATE_DRIVER);
  const [edit] = useMutation(EDIT_DRIVER);
  const [remove] = useMutation(DELETE_DRIVER);

  const [driver, setDriver] = useState({
    name: '',
    description: '',
    domainId: router.query.id,
    driverEffect: '0',
    driverState: 'need',
  });

  const { data } = useQuery(GET_DRIVER, {
    variables: { id: router.query['driver-id'] },
    skip: !router.query['driver-id'],
    onCompleted: (_data) => {
      setDriver({
        name: _data.getDriver.name,
        description: _data.getDriver.description,
        tensionIds: _data.getDriver.tensions?.map((tension) => tension.id) || [],
        domainId: _data.getDriver.domain?.id,
        authorUserId: _data.getDriver.author?.id,
        driverEffect: _data.getDriver.driverEffect,
        driverState: _data.getDriver.driverState,
      });
    },
  });

  const [deleteDialog, setDeleteDialog] = useState(false);
  const [closeDialog, setCloseDialog] = useState(false);

  if (tensions.loading || domains.loading) {
    return null;
  }

  return (
    <div style={{ display: 'flex' }}>
      <Head>
        <title>
          {router.query['driver-id'] ? `Потребность ${driver.name}` : 'Новая потребность'}
        </title>
      </Head>
      <LeftSubPanel title="Домены">
        <DomainLeftPanel selected="drivers" />
      </LeftSubPanel>
      <Box sx={{
        flex: 1, mx: 4, display: 'flex', flexDirection: 'column',
      }}
      >
        <Box sx={{ display: 'flex', justifyContent: 'space-between' }}>
          <Box
            style={{
              display: 'flex',
              alignItems: 'center',
              width: '100%',
              borderBottom: `${theme.palette.primary.main} solid 2px`,
            }}
          >
            <span style={styles.fieldTitle}>Название*</span>
            <TextField
              value={driver.name}
              error={checkEmpty(driver.name)}
              size="normal"
              variant="standard"
              color="secondary"
              fullWidth
              sx={checkEmpty(driver.name) ? undefined : {
                '& .MuiInput-underline::before': {
                  display: 'none',
                },
                '& .MuiInput-underline::after': {
                  display: 'none',
                },
              }}
              onChange={(e) => {
                setDriver({
                  ...driver,
                  name: e.target.value,
                });
              }}
            />

            {!isNew()
                && (
                <IconButton onClick={() => setDeleteDialog(true)}>
                  <Delete
                    style={{ stroke: theme.palette.primary.main }}
                  />
                </IconButton>
                )}

            <IconButton onClick={() => {
              if (isNew()) router.back();
              else setCloseDialog(true);
            }}
            >
              <Close
                style={{ stroke: theme.palette.primary.main }}
              />
            </IconButton>
          </Box>
        </Box>
        <div style={{
          display: 'flex', marginBottom: '20px', flexWrap: 'wrap', gap: 32,
        }}
        >
          <Box sx={styles.fields}>
            <div>
              <span style={styles.fieldTitle}>
                Создатель
              </span>
              <span style={styles.fieldValue}>
                {data?.getDriver?.authorUser?.name || ''}
              </span>
            </div>
          </Box>
          <Box sx={styles.fields}>
            <div>
              <span style={styles.fieldTitle}>
                Эффект
              </span>
              <Select
                value={driver.driverEffect}
                onChange={(e) => {
                  setDriver({
                    ...driver,
                    driverEffect: e.target.value,
                  });
                }}
                variant="standard"
                color="secondary"
              >
                {['-', '+', '0'].map((driverEffect) => (
                  <MenuItem key={driverEffect} value={driverEffect}>
                    {driverEffect}
                  </MenuItem>
                ))}
              </Select>
            </div>
          </Box>
          <Box sx={styles.fields}>
            <div>
              <span style={styles.fieldTitle}>
                Состояние
              </span>
              <Select
                value={driver.driverState}
                onChange={(e) => {
                  setDriver({
                    ...driver,
                    driverState: e.target.value,
                  });
                }}
                variant="standard"
                color="secondary"
              >
                {Object.keys(driverStates).map((driverState) => (
                  <MenuItem key={driverState} value={driverState}>
                    {driverStates[driverState]?.label}
                  </MenuItem>
                ))}
              </Select>
            </div>
          </Box>
          <Box sx={styles.fields}>
            <div>
              <span style={styles.fieldTitle}>
                Факты
              </span>
              <Select
                value={driver.tensionIds || []}
                onChange={(e) => {
                  setDriver({
                    ...driver,
                    tensionIds: e.target.value
                      .filter((value) => tensions.data
                        .getTensionsOfOrganization.map((_tension) => _tension.id)
                        .includes(value)),
                  });
                }}
                variant="standard"
                color="secondary"
                multiple
              >
                {tensions.data.getTensionsOfOrganization.map((currentTension) => (
                  <MenuItem key={currentTension.id} value={currentTension.id}>
                    {currentTension.name}
                  </MenuItem>
                ))}
              </Select>
            </div>
          </Box>
          <Box sx={styles.fields}>
            <div>
              <span style={styles.fieldTitle}>
                Домен
              </span>
              <Select
                value={driver.domainId}
                onChange={(e) => {
                  setDriver({
                    ...driver,
                    domainId: e.target.value,
                  });
                }}
                variant="standard"
                color="secondary"
              >
                {domains.data?.getDomainsOfOrganization?.map((domain) => (
                  <MenuItem key={domain.id} value={domain.id}>
                    {domain.name}
                  </MenuItem>
                ))}
              </Select>
            </div>
          </Box>
        </div>
        <Box style={{
          border: '1px solid #ddd',
          borderRadius: '5px',
          padding: '5px',
          marginBottom: '5px',
        }}
        >
          <MarkdownEditorLoaded
            value={driver.description}
            onChange={(e) => setDriver({ ...driver, description: e })}
          />
        </Box>
        <div style={{ marginTop: '40px', textAlign: 'right' }}>
          <Button
            variant="outlined"
            color="tertiary"
            style={{ marginRight: '30px' }}
            onClick={() => setCloseDialog(true)}
          >
            Закрыть

          </Button>
          <Button
            variant="contained"
            disabled={
              checkEmpty(driver.name) || checkEmpty(driver.domainId)
            }
            onClick={async () => {
              if (router.query['driver-id']) {
                await edit({ variables: { id: router.query['driver-id'], driver: { ...driver, authorUserId: undefined } } });
              } else {
                await save({ variables: { driver: { ...driver, authorUserId: undefined } } });
              }
              router.push(`${router.asPath}/../`);
            }}
          >
            {isNew() ? 'Создать' : 'Сохранить'}
          </Button>
        </div>
      </Box>
      <SimpleDialog
        open={closeDialog}
        title="Сохранить изменения перед закрытием?"
        onCancel={() => { setCloseDialog(false); router.push(`${router.asPath}/../`); }}
        onAccept={async () => {
          if (router.query['driver-id']) {
            await edit({ variables: { id: router.query['driver-id'], driver: { ...driver, authorUserId: undefined } } });
          } else {
            await save({ variables: { driver: { ...driver, authorUserId: undefined } } });
          }
          router.push(`${router.asPath}/../`);
        }}
      />
      <SimpleDialog
        open={deleteDialog}
        title="Удаление потребности"
        text="Вы уверены, что хотите удалить эту потребность?"
        onCancel={() => setDeleteDialog(false)}
        onAccept={async () => {
          if (router.query['driver-id']) {
            await remove({ variables: { id: router.query['driver-id'] } });
          } else {
            await save({ variables: { driver } });
          }
          router.push(`${router.asPath}/../../`);
        }}
      />
    </div>
  );
}
