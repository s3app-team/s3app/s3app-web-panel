/* eslint-disable react/no-unstable-nested-components */

import { useState, useMemo } from 'react';
import Head from 'next/head';
import { useRouter } from 'next/router';
import { gql, useMutation, useQuery } from '@apollo/client';
import moment from 'moment';
import { useSnackbar } from 'notistack';

import {
  Button, Tooltip, Box, MenuItem, ListItemIcon,
  Typography, TableContainer, Paper,
  TableRow,
  TableCell,
  Table,
} from '@mui/material';
import {
  Delete, Edit, OpenInBrowser,
} from '@mui/icons-material';

import {
  MaterialReactTable,
  useMaterialReactTable,
  MRT_GlobalFilterTextField as GlobalFilterTextField,
  MRT_TablePagination as TablePagination,
  MRT_ToggleFiltersButton as ToggleFiltersButton,
} from 'material-react-table';
import { MRT_Localization_RU as LocalizationRU } from 'material-react-table/locales/ru';

import Link from 'next/link';
import MarkdownPreviewLoaded from '../../../../components/MarkdownPreviewLoaded';
import LeftSubPanel from '../../../../components/LeftSubPanel';
import DomainLeftPanel from '../../../../components/DomainLeftPanel';
import SimpleDialog from '../../../../components/SimpleDialog';
import TableDateRangeFilter from '../../../../components/TableDateRangeFilter';
import { driverStates, effects } from '../../../../components/common';

const DELETE_DRIVER = gql`
mutation Mutation($id: ID!) {
  deleteDriver(id: $id)
}
`;

const GET_DOMAIN = gql`
    query getDomain($id: ID!, $withSubDomains: Boolean) {
        getDomain(id: $id) {
            drivers(withSubDomains: $withSubDomains){
                id
                name
                description
                createdAt
                driverEffect
                driverState
                domain {
                  id
                  name
                }
                authorUser {
                  id
                  name
                }
                tensions {
                  id
                  name
                  createdAt
                  updatedAt
                  tensionMark
                  domain {
                    id
                    name
                  }
                }
            }
        }
    }
`;

function Drivers() {
  const router = useRouter();
  const { enqueueSnackbar } = useSnackbar();

  const { id } = router.query;

  const { data, refetch, loading } = useQuery(GET_DOMAIN, {
    variables: {
      id,
      withSubDomains: true,
    },
  });

  const [deleteDriver] = useMutation(DELETE_DRIVER);

  const [deleteDialog, setDeleteDialog] = useState({ open: false, driver: {} });

  // eslint-disable-next-line react-hooks/exhaustive-deps
  const columns = useMemo(() => [

    {
      accessorFn: (row) => new Date(row.createdAt),
      header: 'Дата создания',
      Header: ({ column }) => (
        <Tooltip title={column.columnDef.header}>
          {column.columnDef.header}
        </Tooltip>
      ),
      sortingFn: 'datetime',
      muiFilterTextFieldProps: {
        placeholder: 'Дата создания',
        autoComplete: 'off',
        inputProps: {
          form: {
            autocomplete: 'off',
          },
        },
      },
      Filter: ({ column }) => <TableDateRangeFilter column={column} />,
      filterFn: (row, _id, filterValue) => {
        const date = new Date(row.original.createdAt);
        if (!filterValue.length) return true;
        return filterValue[0] <= date && date <= filterValue[1];
      },
      Cell: ({ cell }) => moment(cell.getValue()).format('DD.MM.YYYY'),
      maxSize: 100,
      grow: false,
    },
    {
      accessorKey: 'driverEffect',
      header: 'Влияние',
      Header: ({ column }) => (
        <Tooltip title={column.columnDef.header}>
          {column.columnDef.header}
        </Tooltip>
      ),
      sortingFn: (rowA, rowB, columnId) => {
        const order = { '+': 1, 0: 2, '-': 3 };
        return order[rowA.original[columnId]] - order[rowB.original[columnId]];
      },
      filterVariant: 'multi-select',
      filterSelectOptions: [
        { label: 'Положительное', value: '+' },
        { label: 'Нейтральное', value: '0' },
        { label: 'Негативное', value: '-' },
      ],
      muiFilterTextFieldProps: {
        placeholder: 'Влияние',
      },
      muiTableBodyCellProps: {
        align: 'center',
      },
      Cell: ({ cell }) => (
        <Tooltip title={effects[cell.getValue()]?.labelDriver}>
          {effects[cell.getValue()]?.icon}
        </Tooltip>
      ),
      maxSize: 60,
      grow: false,
    },
    {
      accessorKey: 'name',
      header: 'Название',
      filterFn: 'contains',
      filterVariant: 'text',
      muiFilterTextFieldProps: {
        placeholder: 'Название',
      },
      muiTableBodyCellProps: (cell) => ({
        className: 'cursor-pointer text-wrap',
        onClick: () => router.push(`/domains/${cell.row.original.id}/drivers/${cell.row.original.id}`),
      }),
    },
    {
      accessorKey: 'driverState',
      header: 'Состояние',
      Header: ({ column }) => (
        <Tooltip title={column.columnDef.header}>
          {column.columnDef.header}
        </Tooltip>
      ),
      filterVariant: 'multi-select',
      filterSelectOptions: Object.keys(driverStates).map((key) => ({
        value: key,
        label: driverStates[key].label,
      })),
      muiFilterTextFieldProps: {
        placeholder: 'Состояние',
      },
      muiTableBodyCellProps: {
        align: 'center',
      },
      Cell: ({ cell }) => (
        driverStates[cell.getValue()]?.label || ''
      ),
      maxSize: 100,
      grow: false,
    },
    {
      accessorKey: 'domain.name',
      header: 'Домен',
      filterVariant: 'autocomplete',
      muiFilterTextFieldProps: {
        placeholder: 'Домен',
      },
      Cell: ({ row }) => <Link className="text-wrap" href={`/domains/${row.original.domain.id}`}>{row.original.domain.name}</Link>,
      minSize: 2,
      grow: false,
    },
    {
      accessorKey: 'authorUser.name',
      header: 'Автор',
      filterVariant: 'autocomplete',
      muiFilterTextFieldProps: {
        placeholder: 'Автор',
      },
      maxSize: 100,
      grow: false,
    },
  ], [router]);

  const mtable = useMaterialReactTable({
    columns,
    data: data?.getDomain.drivers || [],
    state: { isLoading: loading },
    muiCircularProgressProps: {
      color: 'primary',
      thickness: 5,
      size: 55,
    },
    localization: LocalizationRU,
    globalFilterFn: 'contains',
    columnFilterModeOptions: ['contains'],
    initialState: {
      pagination: { pageSize: 20, pageIndex: 0 },
      showColumnFilters: true,
      showGlobalFilter: true,
      density: 'compact',
      columnPinning: {
        left: ['mrt-row-expand'],
        right: ['mrt-row-actions'],
      },
    },
    displayColumnDefOptions: { 'mrt-row-actions': { minSize: 80, grow: false }, 'mrt-row-expand': { maxSize: 40, grow: false } },
    enableDensityToggle: false,
    enableColumnDragging: false,
    enableFacetedValues: true,
    enableRowActions: true,
    layoutMode: 'grid',
    paginationDisplayMode: 'pages',
    muiTablePaperProps: {
      className: 'shadow-none grow flex flex-col',
    },
    muiTableContainerProps: {
      className: 'grow border-solid border-0 border-t border-gray-200 flex max-h-min ',
    },
    muiSearchTextFieldProps: {
      size: 'small',
      variant: 'outlined',
      autoComplete: 'off',
      inputProps: {
        form: {
          autocomplete: 'off',
        },
      },
    },
    muiFilterTextFieldProps: {
      autoComplete: 'off',
      inputProps: {
        form: {
          autocomplete: 'off',
        },
      },
    },
    muiPaginationProps: {
      color: 'secondary',
      shape: 'rounded',
      variant: 'outlined',
    },
    muiExpandButtonProps: ({ row, table }) => ({
      sx: {
        width: '20px',
      },
      onClick: () => table.setExpanded({ [row.id]: !row.getIsExpanded() }),
    }),
    muiExpandAllButtonProps: { sx: { width: '20px' } },
    renderTopToolbar: ({ table }) => (
      <Box className="flex justify-between items-center px-4 py-2">
        <Box className="flex items-center gap-2">
          <GlobalFilterTextField table={table} />
          <ToggleFiltersButton table={table} />
          <TablePagination table={table} />
        </Box>
        <Button
          color="primary"
          variant="contained"
          onClick={() => router.push(`${router.asPath}/create`)}
        >
          Добавить потребность
        </Button>
      </Box>

    ),
    muiDetailPanelProps: {
      style: { display: 'block' },
    },
    renderDetailPanel: ({ row }) => {
      const sortedTensions = row.original.tensions.sort(
        (a, b) => (a.createdAt < b.createdAt ? 1 : -1),
      );
      return (
        <Box>
          {row.original.description ? (
            <div>
              <MarkdownPreviewLoaded value={row.original.description || 'Нет описания'} />
            </div>
          ) : null}
          {row.original.tensions.length !== 0
        && (
        <Box>
          <Typography variant="h5" className="font-normal text-center mb-2">Факты</Typography>
          <TableContainer component={Paper} elevation={0} variant="outlined">
            <Table>
              {sortedTensions.map((tension) => (
                <TableRow key={tension.id}>
                  <TableCell>{moment(tension.createdAt).format('DD.MM.YYYY')}</TableCell>
                  <TableCell>{effects[tension.tensionMark]?.icon}</TableCell>
                  <TableCell style={{ width: '100%' }}>{tension.name}</TableCell>
                </TableRow>
              ))}
            </Table>
          </TableContainer>
        </Box>
        )}
        </Box>
      );
    },
    renderRowActionMenuItems: ({ row }) => [
      <MenuItem
        key={0}
        onClick={() => router.push(
          `/domains/${row.original.domain.id}/drivers/${row.original.id}`,
        )}
      >
        <ListItemIcon>
          <OpenInBrowser />
        </ListItemIcon>
        Открыть
      </MenuItem>,
      <MenuItem
        key={0}
        onClick={() => router.push(
          `/domains/${row.original.domain.id}/drivers/${row.original.id}/edit`,
        )}
      >
        <ListItemIcon>
          <Edit />
        </ListItemIcon>
        Редактировать
      </MenuItem>,
      <MenuItem
        key={1}
        onClick={() => setDeleteDialog({ open: true, driver: row.original })}
      >
        <ListItemIcon>
          <Delete />
        </ListItemIcon>
        Удалить
      </MenuItem>,
    ],
  }, [data]);

  return (
    <>
      <Head>
        <title>Потребности</title>
      </Head>
      <div className="flex">
        <LeftSubPanel title="Домены">
          <DomainLeftPanel selected="drivers" />
        </LeftSubPanel>

        <div className="flex-1 flex flex-col">
          <SimpleDialog
            open={deleteDialog.open}
            title="Удаление потребности"
            text={`Вы уверены, что хотите удалить потребность "${deleteDialog.driver.name}"?`}
            onCancel={() => setDeleteDialog({ ...deleteDialog, open: false })}
            onAccept={async () => {
              try {
                await deleteDriver({
                  variables: { id: deleteDialog.driver.id },
                  onCompleted: () => {
                    enqueueSnackbar('Потребность успешно удалена', { variant: 'success' });
                    setDeleteDialog({ ...deleteDialog, open: false });
                    refetch();
                  },
                });
              } catch (e) {
                enqueueSnackbar(e.message, { variant: 'error' });
              }
            }}
          />
          <MaterialReactTable table={mtable} />
        </div>
      </div>
    </>
  );
}

export default Drivers;
