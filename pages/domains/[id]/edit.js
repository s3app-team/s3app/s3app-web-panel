import { gql, useMutation, useQuery } from '@apollo/client';
import Head from 'next/head';
import { useRouter } from 'next/router';

import {
  Box, Typography, TextField, Button, useTheme, MenuItem,
} from '@mui/material';

import { useState } from 'react';
import { useSnackbar } from 'notistack';
import DomainLeftPanel from '../../../components/DomainLeftPanel';
import LeftSubPanel from '../../../components/LeftSubPanel';
import S3Avatar from '../../../components/S3Avatar';

import Process from '../../../assets/process.svg';

const GET_DOMAIN = gql`
query getDomain($id: ID!) {
    getDomain(id: $id) {
      name
      description
      performanceCriteria
      mainDriver {
        id
        name
      }
      implementationStrategy
      restrictions
      participants {
        id
        name
      }
      subDomains {
        name
        domainType
        participants {
          id
          name
        }
      }
      stages {
          id
          name
      }
    }
    getDriversOfOrganization(domainId: $id) {
      id
      name
    }
  }
  `;

const EDIT_DOMAIN = gql`
  mutation($editDomainId: ID!, $domain: DomainUpdateInput!) {
    editDomain(id: $editDomainId, domain: $domain) {
      id
      name
    }
  }
`;

function Domain() {
  const router = useRouter();
  const theme = useTheme();

  const style = {
    fieldName: {
      fontWeight: 500,
      width: '230px',
      fontSize: '14px',
      lineHeight: '17px',
      color: '#A2BDE4',
    },
    roleHeader:
      {
        width: '212px',
        height: '30px',
        background: theme.palette.secondary.main,
        color: '#4B5A73',
        border: '1px solid',
        borderColor: theme.palette.secondary.main,
        textAlign: 'center',
      },
    roleCell: {
      width: '212px',
      height: '40px',
      border: '1px solid',
      borderColor: theme.palette.secondary.main,
      textAlign: 'center',
    },
    fieldContent: {
      width: '430px',
      height: '30px',
      // border: '2px solid',
      borderColor: theme.palette.secondary.main,
      borderRadius: '5px',
      fontSize: '14px',
      display: 'flex',
      alignItems: 'center',
      padding: '10px',
      boxSizing: 'border-box',
    },
  };

  const { id } = router.query;
  const [editForm, setEditForm] = useState({
    name: '',
    // description: '',
    performanceCriteria: '',
    implementationStrategy: '',
    restrictions: '',
    mainDriverId: '',
  });
  const { loading, data, refetch } = useQuery(GET_DOMAIN, {
    variables: { id },
    onCompleted: (_data) => {
      setEditForm({
        name: _data.getDomain.name,
        // description: _data.getDomain.description,
        performanceCriteria: _data.getDomain.performanceCriteria,
        implementationStrategy: _data.getDomain.implementationStrategy,
        restrictions: _data.getDomain.restrictions,
        mainDriverId: _data.getDomain.mainDriver?.id,
      });
    },
  });

  const [editDomain] = useMutation(EDIT_DOMAIN);

  const { enqueueSnackbar } = useSnackbar();

  if (loading) {
    return null;
  }

  return (
    <>
      <Head>
        <title>Редактирование домена</title>
      </Head>
      <Box sx={{ display: 'flex', justifyContent: 'space-between' }}>
        <LeftSubPanel title="Домены">
          <DomainLeftPanel selected="domain" />
        </LeftSubPanel>

        <Box sx={{
          mt: 4.5,
          flex: 1,
          display: 'flex',
          flexDirection: 'column',
          ml: 4.5,
        }}
        >
          <Box display="inline-flex" gap={16} mb={5}>
            <Box sx={{
              bgcolor: 'tertiary.main',
              borderRadius: '5px',
              height: 100,
              width: 100,
              display: 'flex',
              alignItems: 'center',
              justifyContent: 'center',
            }}
            >
              <Typography color="white" variant="h3">
                {editForm.name?.toUpperCase()
                  .split(' ')
                  .slice(0, 2)
                  .map((word) => word.charAt(0))
                  .join('')}
              </Typography>
            </Box>
            <Box sx={{
              flex: 1,
              display: 'flex',
              flexDirection: 'column',
              gap: '8px',
            }}
            >
              <Box display="flex" gap={1}>
                <TextField
                  variant="standard"
                  fullWidth
                  label="Название домена"
                  value={editForm.name}
                  onChange={(e) => setEditForm({ ...editForm, name: e.target.value })}
                />
              </Box>
              {/* <TextField
                fullWidth
                variant="standard"
                label="Описание домена"
                multiline
                value={editForm.description}
                onChange={(e) => setEditForm({ ...editForm, description: e.target.value })}
              /> */}
            </Box>
          </Box>

          <Box display="inline-flex">
            <Box sx={style.fieldName}>Основной драйвер:</Box>
            <Box>
              <TextField
                select
                fullWidth
                variant="standard"
                value={editForm.mainDriverId}
                onChange={(e) => setEditForm({
                  ...editForm,
                  mainDriverId: e.target.value,
                })}
              >
                {data.getDriversOfOrganization.map((driver) => (
                  <MenuItem key={driver.id} value={driver.id}>{driver.name}</MenuItem>
                ))}
              </TextField>
            </Box>

          </Box>
          <Box display="inline-flex" my={8}>
            <Box sx={style.fieldName}>Участники домена:</Box>
            <Box>
              <table style={{ borderCollapse: 'collapse', width: '100%' }}>
                <thead>
                  <tr>
                    <td style={style.roleHeader}>Участник</td>
                    <td style={style.roleHeader}>Роль</td>
                  </tr>
                </thead>
                <tbody>
                  {data.getDomain.subDomains.map((subDomain) => {
                    if (subDomain.domainType !== 'role') {
                      return null;
                    }
                    return subDomain.participants.map((participant, index) => (
                      <tr key={index}>
                        <td style={style.roleCell}>
                          <Box display="flex" alignItems="center">
                            <S3Avatar name={participant.name} size={24} sx={{ mx: 1 }} />
                            <Typography variant="body2" lineHeight={1}>{participant.name}</Typography>
                          </Box>
                        </td>
                        <td style={style.roleCell}>{subDomain.name}</td>
                      </tr>
                    ));
                  })}
                  {data.getDomain.participants.length > 0 ? (
                    <>
                      {data.getDomain.participants.map((participant, index) => (
                        <tr key={index}>
                          <td style={style.roleCell}>
                            <Box display="flex" alignItems="center">
                              <S3Avatar name={participant.name} size={24} sx={{ mx: 1 }} />
                              <Typography variant="body2" lineHeight={1}>{participant.name}</Typography>
                            </Box>
                          </td>
                          <td style={style.roleCell}>Участник домена</td>
                        </tr>
                      ))}
                    </>
                  ) : (
                    <tr>
                      <td colSpan="2" style={style.roleCell}>Ролей нет</td>
                    </tr>
                  )}
                </tbody>
              </table>
            </Box>
          </Box>
          <Box display="inline-flex" sx={{ alignItems: 'center' }}>
            <Box sx={style.fieldName}>Стратегия реализации:</Box>
            <Box>
              <Box sx={style.fieldContent}>
                <TextField
                  fullWidth
                  variant="standard"
                  value={editForm.implementationStrategy}
                  onChange={(e) => setEditForm({
                    ...editForm,
                    implementationStrategy: e.target.value,
                  })}
                />
              </Box>
            </Box>
          </Box>
          <Box display="inline-flex" my={1} sx={{ alignItems: 'center' }}>
            <Box sx={style.fieldName}>Критерии эффективности:</Box>
            <Box>
              <Box sx={style.fieldContent}>
                <TextField
                  fullWidth
                  variant="standard"
                  value={editForm.performanceCriteria}
                  onChange={(e) => setEditForm({
                    ...editForm,
                    performanceCriteria: e.target.value,
                  })}
                />
              </Box>
            </Box>

          </Box>
          <Box display="inline-flex" sx={{ alignItems: 'center' }}>
            <Box sx={style.fieldName}>Ограничения домена:</Box>
            <Box>
              <Box sx={style.fieldContent}>
                <TextField
                  fullWidth
                  variant="standard"
                  value={editForm.restrictions}
                  onChange={(e) => setEditForm({
                    ...editForm,
                    restrictions: e.target.value,
                  })}
                />
              </Box>
            </Box>
          </Box>
          <Box style={{ paddingTop: 16, display: 'flex', gap: 8 }}>
            <Button
              variant="contained"
              color="primary"
              onClick={async () => {
                await editDomain({
                  variables: {
                    editDomainId: id,
                    domain: editForm,
                  },
                });
                enqueueSnackbar('Домен успешно изменен', { variant: 'success' });
                refetch();
              }}
            >
              Сохранить
            </Button>
            <Button
              variant="contained"
              color="secondary"
              onClick={() => {
                router.push(`/domains/${id}`);
              }}
            >
              Отмена
            </Button>
          </Box>
        </Box>

        <Box sx={{
          width: 256, bgcolor: 'secondary.main', display: 'flex', flexDirection: 'column', alignItems: 'center',
        }}
        >
          <Box m={2}>Цепочка создания ценности</Box>
          <Box sx={{
            width: 208,
            height: 52,
            backgroundColor: '#FFFFFF',
            borderRadius: '5px',
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
            marginBottom: 1,
          }}
          >
            {/* {data.getDomain.valueChain?.resources} */}
          </Box>
          <Box>
            {data.getDomain.stages?.map((stage, index) => (
              <Box
                key={index}
                style={{
                  display: 'flex',
                  alignItems: 'center',
                  justifyContent: 'center',
                  position: 'relative',
                  marginBottom: 8,
                }}
              >
                <Process />
                <Box style={{ position: 'absolute' }}>{stage.name}</Box>
              </Box>
            ))}
          </Box>
          <Box style={{
            width: 208,
            height: 40,
            backgroundColor: '#FFFFFF',
            borderRadius: 5,
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
          }}
          >
            {/* {data.getDomain.valueChain?.products} */}

          </Box>
        </Box>
      </Box>
    </>
  );
}

export default Domain;
