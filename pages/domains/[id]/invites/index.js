import Head from 'next/head';
import { useState } from 'react';
import { gql, useMutation } from '@apollo/client';
import { useSnackbar } from 'notistack';

import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';

import LeftSubPanel from '../../../../components/LeftSubPanel';
import DomainLeftPanel from '../../../../components/DomainLeftPanel';

const SEND_INVITE = gql`
mutation($id: ID!, $name: String!, $email: String, $key: String) {
  sendInvinte(name: $name, email: $email, key: $key)
}
`;

function Invites(props) {
  const [inviteForm, setInviteForm] = useState({ name: '', email: '', key: '' });

  const [sendInvinte] = useMutation(SEND_INVITE);

  const { enqueueSnackbar } = useSnackbar();

  return (
    <>
      <Head>
        <title>Приглашения в организацию</title>
      </Head>
      <div style={{ display: 'flex' }}>
        <LeftSubPanel title="Домены">
          <DomainLeftPanel selected="invites" />
        </LeftSubPanel>
        <div>
          <div style={{
            display: 'flex', flexDirection: 'column', gap: '15px', margin: '15px',
          }}
          >
            <Typography component="h1" variant="h5">
              Пригласить в организацию
              &quot;
              {props.organization.name}
              &quot;
            </Typography>
            <TextField
              label="Имя"
              value={inviteForm.name}
              onChange={(e) => setInviteForm({ ...inviteForm, name: e.target.value })}
            />
            <TextField
              label="E-Mail"
              type="email"
              value={inviteForm.email}
              onChange={(e) => setInviteForm({ ...inviteForm, email: e.target.value })}
            />
            <TextField
              label="Лицензионный ключ"
              value={inviteForm.key}
              onChange={(e) => setInviteForm({ ...inviteForm, key: e.target.value })}
            />
            <Button
              fullWidth
              variant="contained"
              sx={{ mt: 1 }}
              disabled={!inviteForm.name || !inviteForm.email || !inviteForm.key}
              onClick={async () => {
                try {
                  await sendInvinte({
                    variables: { id: props.organization.id, ...inviteForm },
                    onCompleted: () => {
                      enqueueSnackbar('Приглашение отправлено успешно', { variant: 'success' });
                    },
                  });
                } catch (e) {
                  enqueueSnackbar(e.message, { variant: 'error' });
                }
              }}
            >
              Отправить приглашение
            </Button>
          </div>
        </div>
      </div>
    </>
  );
}

export default Invites;
