import Head from 'next/head';
import { useState } from 'react';
import { gql, useQuery, useMutation } from '@apollo/client';
import TextField from '@mui/material/TextField';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import { useSnackbar } from 'notistack';
import { useRouter } from 'next/router';
import List from '@mui/material/List';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemText from '@mui/material/ListItemText';

import {
  Checkbox, Table, TableCell, TableRow,
} from '@mui/material';
import LeftSubPanel from '../../../../components/LeftSubPanel';
import DomainLeftPanel from '../../../../components/DomainLeftPanel';
import SimpleDialog from '../../../../components/SimpleDialog';
import { checkEmpty } from '../../../../components/common';

const GET_ISSUE_TYPES = gql`
query GetDomain($getDomainId: ID!) {
  getDomain(id: $getDomainId) {
    issueTypes {
      id
      name
      stages {
        id
        name
      }
    }
    stages {
      id
      name
      color
    }
  }
}
`;

const CREATE_ISSUE_TYPE = gql`
mutation CreateIssueType($issueType: IssueTypeInput!) {
  createIssueType(issueType: $issueType) {
    id
  }
}`;

const DELETE_ISSUE_TYPE = gql`
mutation Mutation($deleteIssueTypeId: ID!) {
  deleteIssueType(id: $deleteIssueTypeId)
}`;

const EDIT_ISSUE_TYPE = gql`
mutation Mutation($editIssueTypeId: ID!, $issueType: IssueTypeInput!) {
  editIssueType(id: $editIssueTypeId, issueType: $issueType) {
    id
  }
}`;

function IssueTypes() {
  const router = useRouter();

  const { enqueueSnackbar } = useSnackbar();

  const { data, refetch } = useQuery(
    GET_ISSUE_TYPES,
    { variables: { getDomainId: router.query.id } },
  );

  const [selectedIssueType, setSelectedIssueType] = useState('');

  const [createIssueType] = useMutation(CREATE_ISSUE_TYPE);
  const [editIssueType] = useMutation(EDIT_ISSUE_TYPE);
  const [deleteIssueType] = useMutation(DELETE_ISSUE_TYPE);

  const [issueTypeDialog, setIssueTypeDialog] = useState({ open: false, newIssueType: '' });

  const handleListItemClick = (event, issueType) => {
    setSelectedIssueType({
      id: issueType.id,
      name: issueType.name,
      stageIds: issueType.stages.map((stage) => stage.id),
    });
  };

  return (
    <>
      <Head>
        <title>Создание ценности</title>
      </Head>
      <div style={{ display: 'flex' }}>
        <LeftSubPanel title="Домены">
          <DomainLeftPanel selected="issue-types" />
        </LeftSubPanel>
        <div>
          <Box sx={{ marginTop: 3, marginLeft: 3, display: 'flex' }}>
            <Box sx={{ width: 250 }}>
              <List
                sx={{
                  bgcolor: 'background.paper',
                  position: 'relative',
                  overflow: 'auto',
                  maxHeight: 300,
                  '& ul': { padding: 0 },
                  border: '2px solid #F79244',
                  borderRadius: 1,
                  padding: 0,
                }}
              >
                {data?.getDomain?.issueTypes.length ? (
                  data.getDomain?.issueTypes.map((issueType) => (
                    <div
                      key={issueType.id}
                    >
                      <ListItemButton
                        selected={selectedIssueType.id === issueType.id}
                        key={issueType.id}
                        onClick={(event) => handleListItemClick(event, issueType)}
                      >
                        <ListItemText primary={issueType.name} />
                      </ListItemButton>
                    </div>
                  ))
                ) : (
                  <ListItemButton disabled>
                    <ListItemText primary="Типов задач нет" />
                  </ListItemButton>
                )}
              </List>
              <Button
                variant="contained"
                sx={{ marginTop: 1, width: '100%' }}
                onClick={() => {
                  setIssueTypeDialog({ open: true });
                }}
              >
                Создать тип задачи
              </Button>
            </Box>
            {selectedIssueType ? (
              <>
                <Box
                  sx={{
                    marginLeft: 1,
                    maxWidth: 370,
                  }}
                >
                  <div>
                    <TextField
                      required
                      error={!selectedIssueType.name}
                      variant="outlined"
                      size="small"
                      label="Название"
                      value={selectedIssueType.name}
                      sx={{ width: '100%' }}
                      onChange={(e) => setSelectedIssueType(
                        {
                          ...selectedIssueType,
                          name: e.target.value,
                        },
                      )}
                    />
                  </div>
                  <Button
                    disabled={checkEmpty(selectedIssueType.name)}
                    variant="contained"
                    sx={{ marginTop: 1, width: '100%' }}
                    onClick={async () => {
                      try {
                        await editIssueType({
                          variables: {
                            editIssueTypeId: selectedIssueType.id,
                            issueType: {
                              name: selectedIssueType.name,
                              stageIds: selectedIssueType.stageIds,
                            },
                          },
                        }).then(() => {
                          refetch();
                        });
                      } catch (e) {
                        console.log(e);
                        enqueueSnackbar(e.message, { variant: 'error' });
                        return;
                      }
                      enqueueSnackbar('Тип задачи успешно отредактирован', { variant: 'success' });
                    }}
                  >
                    Сохранить
                  </Button>
                  <Button
                    variant="contained"
                    sx={{
                      marginTop: 1, width: '100%', background: 'red', '&:hover': { background: 'firebrick' },
                    }}
                    onClick={async () => {
                      try {
                        await deleteIssueType({
                          variables: {
                            deleteIssueTypeId: selectedIssueType.id,
                          },
                        }).then(() => {
                          refetch();
                        });
                      } catch (e) {
                        console.log(e);
                        enqueueSnackbar(e.message, { variant: 'error' });
                        return;
                      }
                      setSelectedIssueType('');
                      enqueueSnackbar('Тип задачи успешно удален', { variant: 'success' });
                    }}
                  >
                    Удалить
                  </Button>
                </Box>
                <Box>
                  <Table size="small">
                    {
                        data.getDomain.stages.map((stage, index) => (
                          <TableRow key={index}>
                            <TableCell>
                              {stage.name}
                            </TableCell>
                            <TableCell style={{ textAlign: 'center' }}>
                              <Checkbox
                                checked={selectedIssueType.stageIds.includes(stage.id)}
                                onChange={(e) => {
                                  if (e.target.checked) {
                                    setSelectedIssueType({
                                      ...selectedIssueType,
                                      stageIds: [...selectedIssueType.stageIds, stage.id],
                                    });
                                  } else {
                                    setSelectedIssueType({
                                      ...selectedIssueType,
                                      stageIds: selectedIssueType.stageIds.filter(
                                        (id) => id !== stage.id,
                                      ),
                                    });
                                  }
                                }}
                                size="small"
                              />
                            </TableCell>
                          </TableRow>
                        ))
                      }
                  </Table>
                </Box>
              </>
            ) : null}
          </Box>
          <SimpleDialog
            disabled={!issueTypeDialog.issueType}
            open={issueTypeDialog.open}
            title="Создание типа задачи"
            text="Тип задачи - этапы, которые может проходить задача"
            acceptText="Создать"
            cancelText="Отмена"
            onCancel={() => setIssueTypeDialog({})}
            onAccept={async () => {
              try {
                await createIssueType({
                  variables: {
                    issueType: { domainId: router.query.id, name: issueTypeDialog.issueType },
                  },
                }).then(() => {
                  refetch();
                  setIssueTypeDialog({});
                });
              } catch (e) {
                console.log(e);
                enqueueSnackbar(e.message, { variant: 'error' });
                return;
              }
              enqueueSnackbar('Тип задачи успешно создан', { variant: 'success' });
              setIssueTypeDialog({});
            }}
          >
            <TextField
              error={checkEmpty(issueTypeDialog.issueType)}
              autoFocus
              margin="dense"
              id="name"
              label="Название"
              type="text"
              fullWidth
              variant="standard"
              onChange={(e) => setIssueTypeDialog(
                { ...issueTypeDialog, issueType: e.target.value },
              )}
            />
          </SimpleDialog>
        </div>
      </div>
    </>
  );
}

export default IssueTypes;
