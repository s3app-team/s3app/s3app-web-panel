import Head from 'next/head';
import Link from 'next/link';
// import Markdown from 'react-markdown';
// import '@uiw/react-md-editor/markdown-editor.css';
// import '/styles/markdown.css'
// import '@uiw/react-md-editor/markdown-editor.css';
// import '@uiw/react-markdown-preview/markdown.css';

// import MarkdownPreview from '@uiw/react-markdown-preview';
// import * as MarkdownPreview from '@uiw/react-markdown-preview/index.js';
// const MarkdownPreview = require('@uiw/react-markdown-preview/index.js');
import { useRouter } from 'next/router';
import { useQuery, gql } from '@apollo/client';
import {
  Box,
  IconButton,
  Tooltip,
  useTheme,
} from '@mui/material';
import moment from 'moment';
import {
  priorities, priorityShield,
} from '../../../../../components/common';
import Edit from '../../../../../assets/edit.svg';

import Close from '../../../../../assets/close.svg';
import HistoryAddExecutor from '../../../../../assets/historyAddExecutor.svg';
import HistoryAddSprint from '../../../../../assets/historyAddSprint.svg';
import HistoryChange from '../../../../../assets/historyChange.svg';
import HistoryCreate from '../../../../../assets/historyCreate.svg';
import HistoryStart from '../../../../../assets/historyStart.svg';
import MyStepper from '../../../../../components/MyStepper';
import LeftSubPanel from '../../../../../components/LeftSubPanel';
import DomainLeftPanel from '../../../../../components/DomainLeftPanel';
import CommentCreator from '../../../../../components/CommentCreator';
import S3Avatar from '../../../../../components/S3Avatar';
import CommentPreviewList from '../../../../../components/CommentPreviewList';
import MarkdownPreviewLoaded from '../../../../../components/MarkdownPreviewLoaded';

const historyTypes = {
  newExecutor: HistoryAddExecutor,
  newSprints: HistoryAddSprint,
  create: HistoryCreate,
  executorStartedWork: HistoryStart,
  update: HistoryChange,
};

// const MarkdownPreview = dynamic(
//   () => import('@uiw/react-markdown-preview').then((mod) => mod.default),
//   { ssr: false },
// );

const GET_ISSUE = gql`
  query getIssue($id: ID!) {
    getIssue(id: $id) {
      id
      name
      description
      issueLinks {
        id
        name
        domain {
          id
        }
      }
      priority
      createdAt
      stage {
        name
      }
      author {
        id
        name
      }
      historyLog {
        date
        name
        description
        type
      }
      domain {
        name
      }
      executor {
        name
      }
      sprint {
        id
      }
      version
      stage {
        id
        name
      }
      comments {
        id
        author {
          id
          name
        }
        description
        createdAt
      }
    }
  }
`;

const styles = {
  fields: {
    '&': { width: '50%', paddingTop: '10px' },
    '& > div': {
      padding: '10px 0px',
      display: 'flex',
      alignItems: 'center',
      height: '20px',
    },
  },
  commentsList: {
    padding: '10px',
    marginTop: '10px',
  },
  fieldContent: {
    minWidth: '348px',
    minHeight: '30px',
    border: '2px solid #DAE5F4',
    borderRadius: '5px',
    fontSize: '14px',
    display: 'flex',
    alignItems: 'center',
    padding: '10px',
    boxSizing: 'border-box',
  },
  fieldTitle: {
    color: '#A2BDE4',
    paddingRight: 4,
    fontSize: '14px',
  },
  link: {
    cursor: 'pointer',
    textDecoration: 'underline',
    color: '#4B5A73',
  },
  description: {
    border: '2px solid #A2BDE4',
    borderRadius: '5px',
    padding: '20px',
  },
  edit: {
    textAlign: 'right',
  },
  rightBlock: {
    padding: '12px 24px',
    background: '#4B5A73',
    color: '#A2BDE4',
    borderRadius: '5px',
    flex: 1,
    fontSize: '14px',
  },
  rightBlockText: {
    color: '#A2BDE4',
  },
  sprint: {
    padding: '0px 10px',
  },
  header: { fontSize: '14px', color: '#4B5A73' },
  authorName: {
    fontWeight: 'bold',
    marginRight: '5px',
  },
};

function StepIconType(props) {
  const theme = useTheme();
  const Component = historyTypes[props.type];
  return (
    <span style={{
      borderRadius: '50px',
      borderColor: theme.palette.primary.main,
      borderWidth: '2px',
      borderStyle: 'solid',
      width: 40,
      height: 40,
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
    }}
    >
      <Component style={{ stroke: theme.palette.primary.main }} />
    </span>
  );
}

export default function Issue(props) {
  const router = useRouter();
  const { 'issue-id': id } = router.query;
  const { data, loading, refetch } = useQuery(GET_ISSUE, {
    variables: { id },
  });
  const theme = useTheme();
  if (loading) {
    return null;
  }
  const issue = data.getIssue;
  const comments = [...issue.comments];
  comments.sort((a, b) => moment(a.createdAt) - moment(b.createdAt));
  return (
    <>
      <Head>
        <title>Задача</title>
      </Head>
      <Box sx={{ display: 'flex' }}>
        <LeftSubPanel title="Домены">
          <DomainLeftPanel selected="issues" />
        </LeftSubPanel>

        <Box sx={{
          flex: 1, ml: 4, display: 'flex', flexDirection: 'column',
        }}
        >
          <Box sx={{ display: 'flex', justifyContent: 'space-between' }}>
            <Box
              style={{
                display: 'flex',
                alignItems: 'center',
                width: '100%',
                borderBottom: `${theme.palette.primary.main} solid 2px`,
              }}
            >
              <span style={{ width: '100%', fontSize: '20px', fontWeight: 700 }}>{issue.name}</span>
              <span style={{ fontSize: 14, textWrap: 'nowrap' }}>{issue.stage?.name || 'Этап не задан'}</span>
              <IconButton onClick={() => router.push(`${router.asPath}/edit`)}>
                <Edit
                  style={{ stroke: theme.palette.primary.main }}
                />
              </IconButton>

              <IconButton onClick={() => router.back()}>
                <Close
                  style={{ stroke: theme.palette.primary.main }}
                />
              </IconButton>
            </Box>
          </Box>

          <Box sx={{ display: 'flex', flex: 1 }}>
            <Box sx={{ flex: 1 }}>
              <Box sx={{ mr: 4 }}>
                <Box sx={{
                  display: 'grid',
                  mt: 2.75,
                  gridTemplateColumns: 'repeat(3, 1fr)',
                  alignItems: 'center',
                  gap: 1,
                  fontSize: 14,
                }}
                >
                  <div>
                    <span style={styles.fieldTitle}>
                      ID
                    </span>
                    <span style={styles.fieldValue}>
                      {issue.id}
                    </span>
                  </div>
                  <div style={{ display: 'flex', alignItems: 'center' }}>
                    <span style={styles.fieldTitle}>
                      Домен
                    </span>
                    <span
                      style={styles.link}
                      onClick={() => router.push(`/domains/${router.query.id}`)}
                    >
                      {issue.domain?.name}
                    </span>
                    <span>
                      <S3Avatar name={issue.domain?.name} size={30} sx={{ ml: 1 }} noBorder color="#687B98" />
                    </span>
                  </div>
                  <div>
                    <span style={styles.fieldTitle}>
                      Связь
                    </span>
                    <span>
                      {issue.issueLinks?.map((linkedIssue, issueKey) => (
                        <Link key={issueKey} href={`/domains/${linkedIssue.domain.id}/issues/${linkedIssue.id}`}>
                          <span>
                            <span style={styles.link}>{linkedIssue.name}</span>
                            {issueKey !== issue.issueLinks.length - 1 && ', '}
                          </span>
                        </Link>
                      ))}
                    </span>
                  </div>
                  <div style={{ display: 'flex', alignItems: 'center' }}>
                    <span style={styles.fieldTitle}>
                      Создатель
                    </span>
                    {issue.author?.name || 'Создателя нет'}
                    <span>
                      <S3Avatar name={issue.author?.name} size={30} sx={{ ml: 1 }} tooltip />
                    </span>
                  </div>
                  <div style={{ display: 'flex', alignItems: 'center' }}>
                    <span style={styles.fieldTitle}>
                      Исполнитель
                    </span>
                    {issue.executor?.name || 'Исполнителя нет'}
                    <S3Avatar name={issue.executor?.name} size={28} sx={{ ml: 1 }} tooltip />
                  </div>
                  <div style={{ display: 'flex', alignItems: 'center' }}>
                    <span style={styles.fieldTitle}>
                      Приоритет решения
                    </span>
                    <Tooltip title={priorities[issue.priority]?.name}>
                      <span>
                        {priorityShield(issue.priority)}
                      </span>
                    </Tooltip>
                  </div>

                </Box>
                <span style={{ ...styles.fieldContent, margin: 0, padding: 0 }}>
                  <MarkdownPreviewLoaded value={issue.description || 'Описания нет'} />
                </span>

                <div style={{ marginTop: 10 }}>
                  <span style={styles.fieldTitle}>
                    Комментарии
                  </span>
                  <div style={styles.commentsList}>
                    {comments && comments.length > 0 ? (
                      comments.map((comment) => (
                        <CommentPreviewList
                          comment={comment}
                          key={comment.id}
                          user={props.user}
                          refetch={refetch}
                        />
                      ))
                    ) : (
                      <div>
                        Пока нет комментариев
                      </div>
                    )}
                    <CommentCreator entity={{ issueId: issue.id }} refetch={refetch} />
                  </div>
                </div>
              </Box>

            </Box>

            <Box
              sx={{
                width: 280,
                backgroundColor: '#DAE5F4',
                display: 'flex',
                flexDirection: 'column',
                alignItems: 'center',
              }}
            >
              <span style={{ ...styles.header, margin: '16px' }}>История изменений</span>
              <MyStepper
                items={
                  data.getIssue.historyLog?.map((history) => ({
                    icon: <StepIconType type={history.type} />,
                    content: (
                      <div style={{
                        ...styles.rightBlockText, paddingTop: '14px', paddingLeft: '10px', fontSize: '14px',
                      }}
                      >
                        <div style={{ color: theme.palette.primary.main }}>{history.name}</div>
                        <div>{moment(history.date).format('DD.MM.YYYY HH:mm:ss')}</div>
                      </div>
                    ),
                  }))
                }
                connectorStyle={{ color: theme.palette.primary.main }}
              />
            </Box>
          </Box>
        </Box>

      </Box>

    </>
  );
}
