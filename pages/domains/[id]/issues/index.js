/* eslint-disable react/no-unstable-nested-components */
/* eslint-disable no-param-reassign */
import { useMemo, useState } from 'react';
import { gql, useMutation, useQuery } from '@apollo/client';
import { useRouter } from 'next/router';
import Head from 'next/head';
import Link from 'next/link';
import 'moment/locale/ru';

import {
  Box,
  Button,
  Chip,
  LinearProgress,
  ListItemIcon,
  MenuItem,
  Select,
  Tooltip,
} from '@mui/material';
import {
  Delete, Done, Edit, OpenInBrowser, PlayArrow,
} from '@mui/icons-material';
import Color from 'color';

import {
  MaterialReactTable,
  MRT_GlobalFilterTextField as GlobalFilterTextField,
  MRT_TablePagination as TablePagination,
  MRT_ToggleFiltersButton as ToggleFiltersButton,
  useMaterialReactTable,
} from 'material-react-table';
import { MRT_Localization_RU as LocalizationRU } from 'material-react-table/locales/ru';
import moment from 'moment/moment';
import { priorities, priorityShield } from '../../../../components/common';
import LeftSubPanel from '../../../../components/LeftSubPanel';
import DomainLeftPanel from '../../../../components/DomainLeftPanel';
import Issue from './create';
import TableDateRangeFilter from '../../../../components/TableDateRangeFilter';
import MarkdownPreviewLoaded from '../../../../components/MarkdownPreviewLoaded';
import SimpleDialog from '../../../../components/SimpleDialog';

const GET_DOMAIN = gql`
    query getDomain($id: ID!, 
      $withSubDomains: Boolean) {
        getDomain(id: $id) {
            issuesCount(withSubDomains: $withSubDomains)
            issues(withSubDomains: $withSubDomains ) {
                id
                createdAt
                deadlineAt
                name
                priority
                description
                issueTypeId
                stageId
                authorUserId
                executorUserId
                domainId
                issueLinks {
                  id
                  name
                  domainId
                }
                isClosed
                closedAt
            }
            stages {
              id
              name
              color
            }
        }
        getUsersOfOrganization(domainId: $id) {
          id
          name
        }
        getDomainsOfOrganization(domainId: $id) {
          id
          name
          stages {
            id
            name
            isStart
            isEnd
            color
          }
        }
        getIssueTypesOfOrganization(domainId: $id) {
          id
          name
        }
    }
`;

const DELETE_ISSUE = gql`
  mutation deleteIssue($id: ID!) {
    deleteIssue(id: $id)
  }
`;

function IssuesTable({
  issues, setEditDialog, setCreateDialogOpen, links, refetch,
}) {
  const router = useRouter();
  const issuesPrefix = router.asPath.split('/').slice(0, 4).join('/');
  const [remove] = useMutation(DELETE_ISSUE);
  const [deleteDialog, setDeleteDialog] = useState(false);
  const columns = useMemo(() => [
    {
      accessorKey: 'priority',
      Cell: ({ cell }) => (
        <Tooltip title={priorities[cell.getValue()].name}>
          {priorityShield(cell.getValue())}
        </Tooltip>
      ),
      filterSelectOptions: priorities.map(({ name }, index) => ({ label: name, value: index })),
      filterVariant: 'select',
      header: 'Приоритет',
      Header: ({ column }) => (
        <Tooltip title={column.columnDef.header}>
          {column.columnDef.header}
        </Tooltip>
      ),
      muiFilterTextFieldProps: {
        className: 'max-w-34',
        placeholder: 'Приоритет',
      },
      muiTableBodyCellProps: {
        align: 'center',
      },
      maxSize: 100,
      grow: false,
    },
    {
      accessorKey: 'name',
      header: 'Название',
      filterFn: 'contains',
      filterVariant: 'text',
      muiFilterTextFieldProps: {
        placeholder: 'Название',
      },
      muiTableBodyCellProps: (cell) => ({
        className: 'cursor-pointer text-wrap',
        onClick: () => {
          setEditDialog(cell.row.original.id);
        },
      }),
      minSize: 400,
    },
    {
      accessorFn: (row) => {
        if (row.stage.isEnd) {
          return 'Закрыта';
        }

        return 'Открыта';
      },
      filterVariant: 'select',
      filterSelectOptions: ['Открыта', 'Закрыта'],
      header: 'Статус',
      Header: ({ column }) => (
        <Tooltip title={column.columnDef.header}>
          {column.columnDef.header}
        </Tooltip>
      ),
      muiFilterTextFieldProps: {
        placeholder: 'Статус',
      },
      muiTableBodyCellProps: {
        align: 'center',
      },
      maxSize: 100,
      grow: false,
    },
    {
      accessorKey: 'author.name',
      filterVariant: 'autocomplete',
      header: 'Автор',
      Header: ({ column }) => (
        <Tooltip title={column.columnDef.header}>
          {column.columnDef.header}
        </Tooltip>
      ),
      muiFilterTextFieldProps: {
        placeholder: 'Автор',
      },
      maxSize: 100,
      grow: false,
    },
    {
      accessorKey: 'executor.name',
      filterVariant: 'autocomplete',
      header: 'Исполнитель',
      Header: ({ column }) => (
        <Tooltip title={column.columnDef.header}>
          {column.columnDef.header}
        </Tooltip>
      ),
      muiFilterTextFieldProps: {
        placeholder: 'Исполнитель',
      },
      maxSize: 100,
      grow: false,
    },
    {
      accessorFn: (row) => new Date(row.createdAt),
      Cell: ({ cell }) => moment(cell.getValue()).format('DD.MM.YYYY'),
      Filter: ({ column }) => <TableDateRangeFilter column={column} />,
      filterFn: (row, id, filterValue) => {
        const date = new Date(row.original.createdAt);
        console.log(filterValue);
        if (!filterValue.length) return true;
        if (filterValue[0] === filterValue[1]) {
          return moment(filterValue[0]).format('YYYY-MM-DD') === moment(date).format('YYYY-MM-DD');
        }
        return filterValue[0] <= date && date <= filterValue[1];
      },
      filterVariant: 'text',
      header: 'Дата создания',
      Header: ({ column }) => (
        <Tooltip title={column.columnDef.header}>
          {column.columnDef.header}
        </Tooltip>
      ),
      id: 'createdAt',
      muiFilterTextFieldProps: {
        placeholder: 'Дата создания',
        style: {
          display: 'none',
        },
        autoComplete: 'off',
        inputProps: {
          form: {
            autocomplete: 'off',
          },
        },
      },
      sortingFn: 'datetime',
      maxSize: 100,
      grow: false,
    },
    {
      accessorFn: (row) => new Date(row.deadlineAt),
      Cell: ({ cell }) => (cell.row.original.deadlineAt ? moment(cell.getValue()).format('DD.MM.YYYY') : ''),
      Filter: ({ column }) => <TableDateRangeFilter column={column} placeholder="Дедлайн" />,
      filterFn: (row, id, filterValue) => {
        const date = new Date(row.original.deadlineAt);
        console.log(filterValue);
        if (!filterValue.length) return true;
        if (filterValue[0] === filterValue[1]) {
          return moment(filterValue[0]).format('YYYY-MM-DD') === moment(date).format('YYYY-MM-DD');
        }
        return filterValue[0] <= date && date <= filterValue[1];
      },
      filterVariant: 'text',
      header: 'Дедлайн',
      Header: ({ column }) => (
        <Tooltip title={column.columnDef.header}>
          {column.columnDef.header}
        </Tooltip>
      ),
      id: 'deadlineAt',
      muiFilterTextFieldProps: {
        placeholder: 'Дедлайн',
        style: {
          display: 'none',
        },
        autoComplete: 'off',
        inputProps: {
          form: {
            autocomplete: 'off',
          },
        },
      },
      sortingFn: 'datetime',
      maxSize: 100,
      grow: false,
    },
    {
      accessorKey: 'domain.name',
      Cell: ({ row }) => <Link className="text-wrap" href={`/domains/${row.original.domain.id}`}>{row.original.domain.name}</Link>,
      filterVariant: 'autocomplete',
      header: 'Домен',
      muiFilterTextFieldProps: {
        placeholder: 'Домен',
      },
    },
    {
      accessorKey: 'stage.name',
      Cell: ({ row }) => {
        const { stage } = row.original;
        return (
          <Tooltip title={stage?.name}>
            <Chip
              className="h-auto min-h-8"
              label={(
                <div className="flex items-center gap-1 whitespace-normal max-w-32">
                  {stage?.name}
                  {stage?.isStart ? <PlayArrow /> : null}
                  {stage?.isEnd ? <Done /> : null}
                </div>
                )}
              sx={{
                backgroundColor: stage?.color || 'primary.main',
                color: !stage?.color || Color(stage?.color).luminosity() > 0.6 ? 'black' : 'white',
              }}
            />
          </Tooltip>
        );
      },
      filterVariant: 'autocomplete',
      header: 'Этап',
      muiFilterTextFieldProps: {
        placeholder: 'Этап',
      },
      muiTableBodyCellProps: {
        align: 'center',
      },
    },
    {
      accessorKey: 'issueType.name',
      filterVariant: 'autocomplete',
      header: 'Тип задачи',
      muiFilterTextFieldProps: {
        placeholder: 'Тип задачи',
      },
    },
    {
      accessorFn: (row) => row.issueLinks.map((link) => link.id),
      Cell: ({ row }) => row.original.issueLinks.map((link) => (
        <Link
          className="text-wrap"
          href={`/domains/${link.domain.id}/issues/${link.id}`}
          key={link.id}
        >
          {link.name}
        </Link>
      )),
      filterSelectOptions: links.map(({ id, name }) => ({ label: name, value: id })),
      filterVariant: 'multi-select',
      header: 'Связи',
      muiFilterTextFieldProps: {
        placeholder: 'Связи',
      },
      muiTableBodyCellProps: {
        align: 'center',
      },
    },
  ], [setEditDialog, links]);

  const mtable = useMaterialReactTable({
    columns,
    data: issues,
    localization: LocalizationRU,
    globalFilterFn: 'contains',
    columnFilterModeOptions: ['contains'],
    initialState: {
      pagination: { pageSize: 20, pageIndex: 0 },
      showColumnFilters: true,
      showGlobalFilter: true,
      density: 'compact',
      columnPinning: {
        left: ['mrt-row-expand'],
        right: ['mrt-row-actions'],
      },
      sorting: [
        {
          id: 'createdAt',
          desc: true,
        },
      ],
    },
    displayColumnDefOptions: { 'mrt-row-actions': { minSize: 80, grow: false }, 'mrt-row-expand': { maxSize: 40, grow: false } },
    enableDensityToggle: false,
    enableColumnDragging: false,
    enableFacetedValues: true,
    enableRowActions: true,
    layoutMode: 'grid',
    paginationDisplayMode: 'pages',
    muiTablePaperProps: {
      className: 'shadow-none grow flex flex-col',
    },
    muiTableContainerProps: {
      className: 'grow border-solid border-0 border-t border-gray-200 flex max-h-min ',
    },
    muiSearchTextFieldProps: {
      size: 'small',
      variant: 'outlined',
      autoComplete: 'off',
      inputProps: {
        form: {
          autocomplete: 'off',
        },
      },
    },
    muiFilterTextFieldProps: {
      autoComplete: 'off',
      inputProps: {
        form: {
          autocomplete: 'off',
        },
      },
    },
    muiPaginationProps: {
      color: 'secondary',
      shape: 'rounded',
      variant: 'outlined',
    },
    muiExpandButtonProps: ({ row, table }) => ({
      onClick: () => table.setExpanded({ [row.id]: !row.getIsExpanded() }),
    }),
    renderTopToolbar: ({ table }) => (
      <Box className="flex items-center px-4 py-2 gap-4">
        <Box className="flex items-center gap-2">
          <GlobalFilterTextField table={table} />
          <ToggleFiltersButton table={table} />
          <TablePagination table={table} />
        </Box>
        <Button
          color="primary"
          variant="contained"
          onClick={() => setCreateDialogOpen(true)}
        >
          Добавить задачу
        </Button>
        <Select defaultValue="issues" variant="standard">
          <MenuItem
            value="issues"
            onClick={() => router.push(issuesPrefix)}
          >
            Список проблем
          </MenuItem>
          <MenuItem
            value="kanban"
            onClick={() => router.push(`${issuesPrefix}/kanban`)}
          >
            Канбан-доска
          </MenuItem>
        </Select>
      </Box>

    ),
    renderDetailPanel: ({ row }) => (
      <div className="flex flex-col">
        <div>
          <MarkdownPreviewLoaded value={row.original.description || 'Нет описания'} />
        </div>
      </div>
    ),
    renderRowActionMenuItems: ({ row, closeMenu }) => [
      <MenuItem
        key={0}
        onClick={() => router.push(
          `/domains/${row.original.domain.id}/issues/${row.original.id}`,
        )}
      >
        <ListItemIcon>
          <OpenInBrowser />
        </ListItemIcon>
        Открыть
      </MenuItem>,
      <MenuItem
        key={0}
        onClick={() => router.push(
          `/domains/${row.original.domain.id}/issues/${row.original.id}/edit`,
        )}
      >
        <ListItemIcon>
          <Edit />
        </ListItemIcon>
        Редактировать
      </MenuItem>,
      <MenuItem
        key={1}
        onClick={() => {
          setDeleteDialog(row.original.id);
          closeMenu();
        }}
      >
        <ListItemIcon>
          <Delete />
        </ListItemIcon>
        Удалить
      </MenuItem>,
    ],
  }, [issues]);

  return (
    <>
      <MaterialReactTable table={mtable} />
      <SimpleDialog
        open={deleteDialog}
        title="Удаление задачи"
        text="Вы уверены, что хотите удалить эту задачу?"
        onCancel={() => setDeleteDialog(false)}
        onAccept={async () => {
          await remove({ variables: { id: deleteDialog } });
          setDeleteDialog(false);
          refetch();
        }}
      />
    </>
  );
}

export default function Issues() {
  const router = useRouter();
  const { id } = router.query;

  const [editDialog, setEditDialog] = useState('');

  const [createDialogOpen, setCreateDialogOpen] = useState(false);

  const [statsData, setStatsData] = useState(null);

  const [loading, setLoading] = useState(false);

  const processData = ({
    getDomain, getDomainsOfOrganization, getUsersOfOrganization, getIssueTypesOfOrganization,
  }) => {
    const stages = getDomainsOfOrganization.flatMap((domain) => domain.stages);
    const usersMap = new Map(getUsersOfOrganization.map((user) => [user.id, user]));
    const issueTypesMap = new Map(getIssueTypesOfOrganization.map((type) => [type.id, type]));
    const domainsMap = new Map(getDomainsOfOrganization.map((domain) => [domain.id, domain]));

    getDomain.issues.forEach((issue) => {
      issue.stage = stages.find((stage) => stage.id === issue.stageId);
      issue.author = usersMap.get(issue.authorUserId);
      issue.executor = usersMap.get(issue.executorUserId);
      issue.issueType = issueTypesMap.get(issue.issueTypeId);
      issue.domain = domainsMap.get(issue.domainId);
      issue.issueLinks = issue.issueLinks.map((link) => ({
        ...link,
        domain: domainsMap.get(link.domainId),
      }));
    });

    return {
      getDomain, getDomainsOfOrganization, getUsersOfOrganization, getIssueTypesOfOrganization,
    };
  };

  const stats = useQuery(GET_DOMAIN, {
    variables: {
      id,
      withSubDomains: true,
    },
    onCompleted: (d) => setStatsData(processData(d)),
  });

  const refetchWork = async () => {
    setLoading(true);
    const result = await stats.refetch();
    setStatsData(processData(result.data));
    setLoading(false);
  };

  if (!statsData) {
    return <LinearProgress />;
  }

  const stages = [];
  statsData.getDomain.issues.forEach((issue) => {
    if (issue.stage && !stages.includes(issue.stage)) {
      stages.push(issue.stage);
    }
  });

  const issueTypes = [];
  statsData.getDomain.issues.forEach((issue) => {
    if (issue.issueType && !issueTypes.includes(issue.issueType.name)) {
      issueTypes.push(issue.issueType);
    }
  });

  const links = [];
  statsData.getDomain.issues.forEach((issue) => {
    if (issue.issueLinks) {
      issue.issueLinks.forEach((link) => {
        if (!links.find((l) => l.id === link.id)) {
          links.push(link);
        }
      });
    }
  });

  console.log(statsData);

  return (
    <>
      <Head>
        <title>Задачи</title>
      </Head>
      <div style={{ display: 'flex' }}>
        <LeftSubPanel title="Домены">
          <DomainLeftPanel selected="issues" />
        </LeftSubPanel>
        {createDialogOpen ? (
          <Issue
            isDialog
            domainId={router.query.id}
            refetch={refetchWork}
            open={createDialogOpen}
            onClose={() => setCreateDialogOpen(false)}
          />
        ) : null}
        {editDialog ? (
          <Issue
            isDialog
            issueId={editDialog}
            domainId={router.query.id}
            refetch={refetchWork}
            open={editDialog}
            onClose={() => setEditDialog(false)}
          />
        ) : null}
        <div className="flex-1 flex flex-col">
          {loading && <LinearProgress />}
          <IssuesTable
            issues={statsData.getDomain.issues}
            links={links}
            setEditDialog={setEditDialog}
            setCreateDialogOpen={setCreateDialogOpen}
            refetch={refetchWork}
          />
        </div>
      </div>
    </>
  );
}
