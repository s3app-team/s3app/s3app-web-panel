import { Box } from '@mui/material';
import Head from 'next/head';
import Link from 'next/link';
import { DragDropContext, Draggable, Droppable } from 'react-beautiful-dnd';
import moment from 'moment';

import { useQuery, gql, useMutation } from '@apollo/client';
import { useState } from 'react';
import { useRouter } from 'next/router';
import SearchPanel from '../../../../components/SearchPanel';
import VerticalProgressbar from '../../../../components/VerticalProgressbar';
import LeftSubPanel from '../../../../components/LeftSubPanel';
import DomainLeftPanel from '../../../../components/DomainLeftPanel';
import IssuesPanel from '../../../../components/IssuesPanel';
import S3Avatar from '../../../../components/S3Avatar';

const GET_DOMAIN = gql`
query GetDomain($id: ID!) {
  getDomain(id: $id) {
    stages {
      id
      name
      issues {
        id
        name
        createdAt
        domain {
          name
        }
        author {
          id
          name
        }
        executor {
          id
          name
        }
      }
    }
  }
}
`;

const EDIT_ISSUE = gql`
  mutation($editIssueId: ID!, $issue: IssueUpdateInput!) {
    editIssue(id: $editIssueId, issue: $issue) {
      id
    }
  }
`;

export default function Issues() {
  const router = useRouter();
  const [search, setSearch] = useState('');
  const { loading, data, refetch } = useQuery(GET_DOMAIN, { variables: { id: router.query.id } });
  const [editIssue] = useMutation(EDIT_ISSUE);

  const onDragEnd = (result) => {
    if (!result.destination) return;
    const { destination, draggableId } = result;
    editIssue({
      variables: {
        editIssueId: draggableId,
        issue: {
          stageId: destination.droppableId,
        },
      },
      onCompleted: () => refetch(),
    });
  };

  if (loading) {
    return null;
  }

  const stages = data.getDomain?.stages;
  return (
    <>
      <Head>
        <title>Канбан</title>
      </Head>
      <div style={{ display: 'flex' }}>
        <LeftSubPanel title="Домены">
          <DomainLeftPanel selected="issues" />
        </LeftSubPanel>
        <div style={{ flex: 1 }}>
          <SearchPanel search={search} setSearch={setSearch}>
            <IssuesPanel page="kanban" />
          </SearchPanel>
          <DragDropContext
            onDragEnd={(result) => onDragEnd(result)}
          >
            <table style={{ borderCollapse: 'collapse', borderSpacing: '0px', width: '100%' }}>
              <tbody>
                <tr>
                  {stages.map((stage) => (
                    <td key={stage.id} style={{ border: '1px solid #DAE5F4', padding: '0px', width: '25%' }}>
                      <div style={{
                        fontSize: '14px',
                        paddingTop: '15px',
                        borderBottom: '8px solid #F79244',
                        textAlign: 'center',
                      }}
                      >
                        <span>
                          <span style={{ color: '#4B5A73', marginRight: '20px' }}>{stage.name}</span>
                        </span>
                      </div>
                      <Droppable droppableId={stage.id} key={stage.id}>
                        {(droppableProvided, snapshot) => (
                          <div
                            {...droppableProvided.droppableProps}
                            ref={droppableProvided.innerRef}
                            style={{
                              background: snapshot.isDraggingOver
                                ? 'lightblue'
                                : null,
                              padding: 4,
                              minHeight: 500,
                            }}
                          >
                            {stage.issues.map((issue, index) => (
                              <Draggable
                                key={issue.id}
                                draggableId={issue.id}
                                index={index}
                              >
                                {(draggableProvided) => (
                                  <div
                                    ref={draggableProvided.innerRef}
                                    {...draggableProvided.draggableProps}
                                    {...draggableProvided.dragHandleProps}
                                    style={{
                                      backgroundColor: 'white',
                                      ...draggableProvided.draggableProps.style,
                                      padding: 10,
                                      margin: 10,
                                      border: '1px solid #C7D7EF',
                                    }}
                                    key={issue.id}
                                  >
                                    <Link href={`/domains/${router.query.id}/issues/${issue.id}`}>
                                      <div style={{ display: 'flex' }}>
                                        <div style={{
                                          flex: 1,
                                          display: 'flex',
                                          flexDirection: 'column',
                                          justifyContent: 'space-between',
                                        }}
                                        >
                                          <Box sx={{
                                            color: (theme) => theme.palette.primary.main,
                                            fontSize: '14px',
                                            lineHeight: '150%',
                                          }}
                                          >
                                            {issue.name}
                                          </Box>
                                          <div style={{
                                            display: 'flex',
                                            alignItems: 'end',
                                            justifyContent: 'space-between',
                                          }}
                                          >
                                            <div style={{
                                              color: '#A2BDE4',
                                              fontSize: '12px',
                                            }}
                                            >
                                              {issue.domain?.name}
                                              <S3Avatar
                                                name={issue.domain?.name}
                                                size={30}
                                                tooltip
                                              />
                                            </div>
                                            <div style={{
                                              color: '#A2BDE4',
                                              fontSize: '10px',
                                            }}
                                            >
                                              {moment(issue.createdAt).format('DD.MM.YYYY')}
                                            </div>
                                          </div>
                                        </div>
                                        <div style={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
                                          <div>
                                            <S3Avatar
                                              name={issue.author?.name}
                                              size={30}
                                              tooltip
                                            />
                                          </div>
                                          <VerticalProgressbar percent={20} />
                                        </div>
                                      </div>
                                    </Link>
                                  </div>
                                )}
                              </Draggable>
                            ))}
                            {droppableProvided.placeholder}
                          </div>
                        )}
                      </Droppable>
                    </td>
                  ))}
                </tr>
              </tbody>
            </table>
          </DragDropContext>
        </div>
      </div>
    </>
  );
}
