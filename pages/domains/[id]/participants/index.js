import React, { useEffect, useState } from 'react';
import Head from 'next/head';
import { gql, useMutation, useQuery } from '@apollo/client';

import {
  TextField,
  Button,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Tab,
  Tabs,
  Box,
  MenuItem,
  IconButton,
  Select,
} from '@mui/material';

import { useRouter } from 'next/router';
import moment from 'moment';
import { useSnackbar } from 'notistack';
import { Save } from '@mui/icons-material';
import LeftSubPanel from '../../../../components/LeftSubPanel';
import DomainLeftPanel from '../../../../components/DomainLeftPanel';
import SimpleDialog from '../../../../components/SimpleDialog';
import { checkEmpty } from '../../../../components/common';
import config from '../../../../config/config';

const GET_INVITES = gql`
  query($domainId: ID!) {
    getInvites(domainId: $domainId) {
      id
      invitedUser {
        id
        name
      }
      inviterUser {
        id
        name
      }
      isAccepted
      acceptedAt
      license
      name
      email
      createdAt
    }
  }
`;

const REVOKE_INVITE = gql`
  mutation($revokeInviteId: ID!) {
    revokeInvite(id: $revokeInviteId)
  }
`;

const CHANGE_USER_ROLE = gql`
mutation($domainId: ID!, $userId: ID!, $accessRole: String!) {
  changeUserRole(domainId: $domainId, userId: $userId, accessRole: $accessRole) {
    id
  }
}
`;

function InvitationsTable({ invitations, refetch }) {
  const [revokeInvite] = useMutation(REVOKE_INVITE);
  const [deleteDialog, setDeleteDialog] = useState(false);

  return (
    <>
      <SimpleDialog
        open={!!deleteDialog}
        title="Отмена заявки"
        text={`Отменить заявку на приглашение ${deleteDialog.name}`}
        acceptText="Отменить"
        cancelText="Отмена"
        onCancel={() => setDeleteDialog(false)}
        onAccept={async () => {
          await revokeInvite({ variables: { revokeInviteId: deleteDialog.id } });
          refetch();
          setDeleteDialog(false);
        }}
      />
      <TableContainer>
        <Table>
          <TableHead>
            <TableRow>
              <TableCell>Отправитель</TableCell>
              <TableCell align="center">Дата отправки</TableCell>
              <TableCell align="center">Имя</TableCell>
              <TableCell align="center">E-Mail</TableCell>
              <TableCell align="center">Статус</TableCell>
              <TableCell align="center">Дата принятия</TableCell>
              <TableCell align="center">Получатель</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {invitations.map((invitation) => (
              <TableRow key={invitation.id}>
                <TableCell>{invitation.inviterUser.name}</TableCell>
                <TableCell align="center">{moment(invitation.createdAt).format('HH:mm:ss DD.MM.YYYY')}</TableCell>
                <TableCell align="center">{invitation.name}</TableCell>
                <TableCell align="center">{invitation.email}</TableCell>
                <TableCell align="center">{invitation.isAccepted ? 'Принято' : 'Не принято'}</TableCell>
                <TableCell align="center">{invitation.acceptedAt ? moment(invitation.acceptedAt).format('HH:mm:ss DD.MM.YYYY') : ''}</TableCell>
                <TableCell align="center">{invitation.invitedUser?.name}</TableCell>
                <TableCell align="center">
                  {invitation.isAccepted
                    ? undefined
                    : (
                      <Button
                        variant="contained"
                        onClick={() => setDeleteDialog(invitation)}
                      >
                        Отменить
                      </Button>
                    )}
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    </>
  );
}

function AccessRoleSelect(props) {
  const [role, setRole] = useState(props.role);
  const [changeUserRole] = useMutation(CHANGE_USER_ROLE);
  useEffect(() => {
    setRole(props.role);
  }, [props.role]);
  const { enqueueSnackbar } = useSnackbar();

  return (
    <div>
      <Select value={role} onChange={(e) => setRole(e.target.value)} variant="standard">
        {['user', 'admin'].map((_role) => <MenuItem key={_role} value={_role}>{_role}</MenuItem>)}
      </Select>
      {props.role !== role ? (
        <IconButton onClick={async () => {
          await changeUserRole({
            variables: {
              domainId: props.domainId,
              userId: props.userId,
              accessRole: role,
            },
          });
          props.refetch();
          enqueueSnackbar('Роль пользователя изменена', { variant: 'success' });
        }}
        >
          <Save />
        </IconButton>
      ) : null}
    </div>
  );
}

function MembersTable({
  members, onExclude, user, refetch,
}) {
  const [deleteDialog, setDeleteDialog] = useState(false);

  const onlyOneAdmin = members.filter((member) => member.accessRole === 'admin').length === 1;

  const isAdmin = members.find((member) => member.user.id === user.id)?.accessRole === 'admin';

  const router = useRouter();
  const { id } = router.query;

  return (
    <>
      <SimpleDialog
        open={!!deleteDialog}
        title="Исключение участника"
        text={`Исключить участника ${deleteDialog?.user?.name}`}
        acceptText="Исключить"
        cancelText="Отмена"
        onCancel={() => setDeleteDialog(false)}
        onAccept={() => {
          onExclude(deleteDialog);
          setDeleteDialog(false);
        }}
      />
      <TableContainer>
        <Table>
          <TableHead>
            <TableRow>
              <TableCell>Имя</TableCell>
              <TableCell align="center">Роль</TableCell>
              <TableCell align="center">Дата присоединения</TableCell>
              <TableCell />
            </TableRow>
          </TableHead>
          <TableBody>
            {members.map((member) => (
              <TableRow key={member.id}>
                <TableCell>{member.user.name}</TableCell>
                <TableCell align="center">
                  {
                  isAdmin && user.id !== member.user.id ? (
                    <AccessRoleSelect
                      role={member.accessRole}
                      userId={member.user.id}
                      domainId={id}
                      refetch={refetch}
                    />
                  )
                    : member.accessRole
}
                </TableCell>
                <TableCell align="center">{moment(member.createdAt).format('HH:mm:ss DD.MM.YYYY')}</TableCell>
                <TableCell>
                  {!isAdmin || (member.accessRole === 'admin' && onlyOneAdmin) ? undefined
                    : <Button variant="contained" onClick={() => setDeleteDialog(member)}>Исключить</Button>}
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    </>
  );
}

const GET_ORGANIZATION_USERS = gql`
query($getOrganizationUsersId: String!) {
  getOrganizationUsers(id: $getOrganizationUsersId) {
    accessRole
    createdAt
    user {
      id
      name
    }
  }
}
`;

const REMOVE_USER_FROM_ORGANIZATION = gql`
  mutation($domainId: ID!, $userId: ID!) {
    removeUserFromOrganization(domainId: $domainId, userId: $userId) {
      id
      name
    }
  }
`;

const SEND_INVITE = gql`
  mutation($domainId: ID!, $email: String!, $license: String, $name: String) {
    sendInvite(domainId: $domainId, email: $email license: $license name: $name)
  }
`;

function Participants(props) {
  const [invitationForm, setInvitationForm] = useState({
    open: false, name: '', email: '', licenseKey: '',
  });
  const [removeUserFromOrganization] = useMutation(REMOVE_USER_FROM_ORGANIZATION);
  const [sendInvite] = useMutation(SEND_INVITE);

  const { enqueueSnackbar } = useSnackbar();

  const router = useRouter();
  const { id } = router.query;

  const {
    data, loading, refetch,
  } = useQuery(GET_ORGANIZATION_USERS, {
    variables: { getOrganizationUsersId: id },
  });
  const invites = useQuery(GET_INVITES, {
    variables: { domainId: id },
  });
  const [viewMode, setViewMode] = useState('members');

  const handleInvite = async (invitation) => {
    await sendInvite({
      variables: {
        domainId: id,
        email: invitation.email,
        license: invitation.licenseKey,
        name: invitation.name,
      },
    });
    invites.refetch();
    refetch();
    setInvitationForm({ ...invitationForm, open: false });
    enqueueSnackbar('Приглашение отправлено либо пользователь добавлен в организацию', { variant: 'success' });
  };

  const handleExclude = async (excludedMember) => {
    await removeUserFromOrganization({
      variables:
      { domainId: id, userId: excludedMember.user.id },
    });
    refetch();
  };

  if (loading || invites.loading) return null;

  return (
    <>
      <Head>
        <title>Участники организации</title>
      </Head>
      <div style={{ display: 'flex' }}>
        <LeftSubPanel title="Домены">
          <DomainLeftPanel selected="participants" />
        </LeftSubPanel>
        <div style={{ flex: 1 }}>
          <Box sx={{
            display: 'flex', alignItems: 'center', borderBottom: '1px solid', borderColor: 'secondary.main', justifyContent: 'space-between',
          }}
          >
            <Tabs value={viewMode} onChange={(e, value) => setViewMode(value)}>
              <Tab value="members" label="Участники" />
              <Tab value="invitations" label="Приглашения" />
            </Tabs>
            <Button
              color="primary"
              variant="contained"
              sx={{ mr: '20px' }}
              onClick={() => setInvitationForm({ ...invitationForm, open: true })}
            >
              Пригласить в организацию
            </Button>
          </Box>

          <div>
            {viewMode === 'invitations' && (
              <div>
                <InvitationsTable
                  invitations={invites.data.getInvites}
                  refetch={invites.refetch}
                />
              </div>
            )}
            {viewMode === 'members' && (
              <div>
                <MembersTable
                  members={data.getOrganizationUsers}
                  onExclude={handleExclude}
                  user={props.user}
                  refetch={refetch}
                />
              </div>
            )}
          </div>
          <SimpleDialog
            disabled={!invitationForm.email}
            open={invitationForm.open}
            title="Приглашение в организацию"
            acceptText="Пригласить"
            cancelText="Отмена"
            onCancel={() => setInvitationForm({ ...invitationForm, open: false })}
            onAccept={() => { handleInvite(invitationForm); }}
          >
            <TextField
              autoFocus
              margin="dense"
              label="Имя"
              type="text"
              fullWidth
              variant="standard"
              onChange={(e) => setInvitationForm({ ...invitationForm, name: e.target.value })}
            />
            <TextField
              error={checkEmpty(invitationForm.email)}
              autoFocus
              margin="dense"
              label="E-Mail"
              type="email"
              fullWidth
              variant="standard"
              onChange={(e) => setInvitationForm({ ...invitationForm, email: e.target.value })}
            />
            {config.license && (
            <TextField
              autoFocus
              margin="dense"
              id="name"
              label="Лицензионный ключ"
              type="text"
              fullWidth
              variant="standard"
              onChange={(e) => setInvitationForm({ ...invitationForm, licenseKey: e.target.value })}
            />
            )}
          </SimpleDialog>
        </div>
      </div>
    </>
  );
}

export default Participants;
