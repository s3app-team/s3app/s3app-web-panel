import { useState, useMemo } from 'react';
import Head from 'next/head';
import { useRouter } from 'next/router';
import { gql, useMutation, useQuery } from '@apollo/client';
import moment from 'moment';
import { useSnackbar } from 'notistack';
import {
  Button, Box, MenuItem, ListItemIcon,
  TableContainer,
  Paper,
  Tooltip,
} from '@mui/material';
import {
  Delete, Edit, OpenInBrowser,
} from '@mui/icons-material';
import {
  MaterialReactTable,
  useMaterialReactTable,
  MRT_GlobalFilterTextField as GlobalFilterTextField,
  MRT_TablePagination as TablePagination,
  MRT_ToggleFiltersButton as ToggleFiltersButton,
} from 'material-react-table';
import { MRT_Localization_RU as LocalizationRU } from 'material-react-table/locales/ru';

import Link from 'next/link';
import LeftSubPanel from '../../../../components/LeftSubPanel';
import DomainLeftPanel from '../../../../components/DomainLeftPanel';
import SimpleDialog from '../../../../components/SimpleDialog';
import TableDateRangeFilter from '../../../../components/TableDateRangeFilter';
import MarkdownPreviewLoaded from '../../../../components/MarkdownPreviewLoaded';
import DriversTable from '../../../../components/DriversTable';
import { effects, proposalStatuses } from '../../../../components/common';

const DELETE_PROPOSAL = gql`
  mutation Mutation($id: ID!) {
    deleteProposal(id: $id)
  }
`;

const GET_DOMAIN = gql`
  query getDomain($id: ID!, $withSubDomains: Boolean) {
    getDomain(id: $id) {
      proposals(withSubDomains: $withSubDomains){
        id
        name
        status
        description
        createdAt
        drivers {
          id
          name
          driverEffect
          domain {
            id
            name
          }
        }
        domain {
          id
          name
        }
      }
    }
  }
`;

function Proposals() {
  const router = useRouter();
  const { enqueueSnackbar } = useSnackbar();
  const { id } = router.query;

  const { data, refetch, loading } = useQuery(GET_DOMAIN, {
    variables: {
      id,
      withSubDomains: true,
    },
  });

  const [deleteProposal] = useMutation(DELETE_PROPOSAL);
  const [deleteDialog, setDeleteDialog] = useState({ open: false, proposal: {} });

  const columns = useMemo(() => [
    {
      accessorFn: (row) => new Date(row.createdAt),
      header: 'Дата создания',
      // eslint-disable-next-line react/no-unstable-nested-components
      Header: ({ column }) => (
        <Tooltip title={column.columnDef.header}>
          {column.columnDef.header}
        </Tooltip>
      ),
      Cell: ({ cell }) => moment(cell.getValue()).format('DD.MM.YYYY'),
      sortingFn: 'datetime',
      // eslint-disable-next-line react/no-unstable-nested-components
      Filter: ({ column }) => <TableDateRangeFilter column={column} />,
      filterFn: (row, _id, filterValue) => {
        const date = new Date(row.original.createdAt);
        if (!filterValue.length) return true;
        return filterValue[0] <= date && date <= filterValue[1];
      },
      maxSize: 100,
      grow: false,
    },
    {
      accessorKey: 'name',
      header: 'Название',
      filterFn: 'contains',
      filterVariant: 'text',
      muiFilterTextFieldProps: {
        placeholder: 'Название',
      },
      // eslint-disable-next-line react/no-unstable-nested-components
      Cell: ({ row }) => (
        <Link className="no-underline text-black" href={`/domains/${id}/proposals/${row.original.id}`}>
          {row.original.name}
        </Link>
      ),
    },
    {
      accessorKey: 'status',
      header: 'Статус',
      filterVariant: 'autocomplete',
      filterSelectOptions: Object.keys(proposalStatuses).map((key) => ({
        value: key,
        label: proposalStatuses[key].label,
      })),
      Cell: ({ cell }) => proposalStatuses[cell.getValue()].label,
      minSize: 2,
      grow: false,
    },
    {
      accessorKey: 'domain.name',
      header: 'Домен',
      filterVariant: 'autocomplete',
      // eslint-disable-next-line react/no-unstable-nested-components
      Cell: ({ row }) => (
        <Link
          className="text-wrap"
          href={`/domains/${row.original.domain.id}`}
        >
          {row.original.domain.name}
        </Link>
      ),
      minSize: 2,
      grow: false,
    },
  ], [id]);

  const table = useMaterialReactTable({
    columns,
    data: data?.getDomain.proposals || [],
    state: { isLoading: loading },
    localization: LocalizationRU,
    globalFilterFn: 'contains',
    columnFilterModeOptions: ['contains'],
    initialState: {
      showColumnFilters: true,
      showGlobalFilter: true,
      pagination: { pageSize: 20, pageIndex: 0 },
    },
    displayColumnDefOptions: { 'mrt-row-actions': { minSize: 80, grow: false }, 'mrt-row-expand': { maxSize: 40, grow: false } },
    enableRowActions: true,
    layoutMode: 'grid',
    paginationDisplayMode: 'pages',
    enableExpanding: true,
    enableFacetedValues: true,
    muiSearchTextFieldProps: {
      size: 'small',
      variant: 'outlined',
      autoComplete: 'off',
      inputProps: {
        form: {
          autocomplete: 'off',
        },
      },
    },
    muiFilterTextFieldProps: {
      autoComplete: 'off',
      inputProps: {
        form: {
          autocomplete: 'off',
        },
      },
    },
    positionActionsColumn: 'last',
    renderTopToolbar: ({ table: _table }) => (
      <Box sx={{ display: 'flex', justifyContent: 'space-between', p: '8px' }}>
        <Box sx={{ display: 'flex', gap: '8px', alignItems: 'center' }}>
          <GlobalFilterTextField table={_table} />
          <ToggleFiltersButton table={_table} />
        </Box>
        <Box sx={{ display: 'flex', gap: '8px', alignItems: 'center' }}>
          <TablePagination table={_table} />
          <Button
            color="secondary"
            variant="contained"
            onClick={() => router.push(`${router.asPath}/create`)}
          >
            Добавить предложение
          </Button>
        </Box>
      </Box>
    ),
    renderRowActionMenuItems: ({ row }) => [
      <MenuItem
        key="open"
        onClick={() => router.push(`/domains/${id}/proposals/${row.original.id}`)}
      >
        <ListItemIcon><OpenInBrowser fontSize="small" /></ListItemIcon>
        Открыть
      </MenuItem>,
      <MenuItem
        key="edit"
        onClick={() => router.push(`/domains/${id}/proposals/${row.original.id}/edit`)}
      >
        <ListItemIcon><Edit fontSize="small" /></ListItemIcon>
        Редактировать
      </MenuItem>,
      <MenuItem
        key="delete"
        onClick={() => setDeleteDialog({ open: true, proposal: row.original })}
      >
        <ListItemIcon><Delete fontSize="small" /></ListItemIcon>
        Удалить
      </MenuItem>,
    ],
    renderDetailPanel: ({ row }) => (
      <Box sx={{ margin: '10px' }}>
        {row.original.description ? (
          <div>
            <MarkdownPreviewLoaded value={row.original.description || 'Нет описания'} />
          </div>
        ) : null}
        <span>Потребности:</span>
        <TableContainer component={Paper} elevation={0} variant="outlined">
          <DriversTable drivers={row.original.drivers} icons={effects} />
        </TableContainer>
      </Box>
    ),
  });

  return (
    <>
      <Head>
        <title>Предложения</title>
      </Head>
      <Box sx={{ display: 'flex' }}>
        <LeftSubPanel title="Домены">
          <DomainLeftPanel selected="proposals" />
        </LeftSubPanel>
        <Box sx={{ flexGrow: 1 }}>
          <MaterialReactTable table={table} />
        </Box>
      </Box>
      <SimpleDialog
        open={deleteDialog.open}
        title="Удаление предложения"
        text={`Вы уверены, что хотите удалить предложение "${deleteDialog.proposal.name}"?`}
        onCancel={() => setDeleteDialog({ ...deleteDialog, open: false })}
        onAccept={async () => {
          try {
            await deleteProposal({
              variables: { id: deleteDialog.proposal.id },
              onCompleted: () => {
                enqueueSnackbar('Предложение успешно удалено', { variant: 'success' });
                setDeleteDialog({ ...deleteDialog, open: false });
                refetch();
              },
            });
          } catch (e) {
            enqueueSnackbar(e.message, { variant: 'error' });
          }
        }}
      />
    </>
  );
}

export default Proposals;
