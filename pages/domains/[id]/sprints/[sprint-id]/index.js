import Head from 'next/head';
import Link from 'next/link';
import { gql, useQuery } from '@apollo/client';
import {
  Box,
  IconButton,
  MenuItem,
  Select,
  Table,
  TableCell,
  TableRow,
  Tooltip,
  useTheme,
  Chip,
} from '@mui/material';

import { useRouter } from 'next/router';
import {
  priorities,
  priorityShield,
} from '../../../../../components/common';

import DomainLeftPanel from '../../../../../components/DomainLeftPanel';
import LeftSubPanel from '../../../../../components/LeftSubPanel';
import RangeDatePicker from '../../../../../components/RangeDatePicker';
import S3Avatar from '../../../../../components/S3Avatar';

import Edit from '../../../../../assets/edit.svg';
import Close from '../../../../../assets/close.svg';
import MarkdownPreviewLoaded from '../../../../../components/MarkdownPreviewLoaded';

const GET_SPRINT = gql`
query getSprint($id: ID!) {
  getSprint(id: $id) {
    id
    name
    startDate
    endDate
    goal
    participants {
      id
      name      
    }
    issues {
      id
      name
      priority
      executor {
        id
        name
      }
      stage {
        name
        id
      }
    }
    domain {
      id
      name
    }
  }
}
  `;

const styles = {
  fields: {
    '&': { width: '50%', paddingTop: '10px' },
    '& > Box': {
      padding: '10px 0px',
      display: 'flex',
      alignItems: 'center',
      height: '20px',
    },
  },
  fieldTitle: {
    color: '#A2BDE4',
    paddingRight: 4,
    fontSize: '14px',
  },
  fieldContent: {
    minWidth: '348px',
    minHeight: '30px',
    border: '2px solid #DAE5F4',
    borderRadius: '5px',
    fontSize: '14px',
    display: 'flex',
    alignItems: 'center',
    padding: '10px',
    boxSizing: 'border-box',
  },
  header: { fontSize: '14px', color: '#4B5A73' },
};

function Sprint() {
  const theme = useTheme();

  const router = useRouter();
  const { 'sprint-id': sprintId } = router.query;
  const { loading, data } = useQuery(GET_SPRINT, { variables: { id: sprintId } });
  if (loading) {
    return null;
  }

  const sprint = data.getSprint;

  return (
    <>
      <Head>
        <title>Спринт</title>
      </Head>
      <Box sx={{ display: 'flex' }}>
        <LeftSubPanel title="Домены">
          <DomainLeftPanel selected="sprints" />
        </LeftSubPanel>

        <Box sx={{
          flex: 1, ml: 4, display: 'flex', flexDirection: 'column',
        }}
        >
          <Box sx={{ display: 'flex', justifyContent: 'space-between' }}>
            <Box
              style={{
                display: 'flex',
                alignItems: 'center',
                width: '100%',
                borderBottom: `${theme.palette.primary.main} solid 2px`,
              }}
            >
              <span style={{ width: '100%', fontSize: '20px', fontWeight: 700 }}>{sprint.name}</span>

              <IconButton onClick={() => router.push(`${router.asPath}/edit`)}>
                <Edit
                  style={{ stroke: theme.palette.primary.main }}
                />
              </IconButton>

              <IconButton onClick={() => router.back()}>
                <Close
                  style={{ stroke: theme.palette.primary.main }}
                />
              </IconButton>
            </Box>
          </Box>

          <Box sx={{ display: 'flex', flex: 1 }}>
            <Box sx={{ flex: 1 }}>
              <Box sx={{ mr: 4 }}>
                <Box sx={{
                  display: 'flex',
                  mt: 2.75,
                  justifyContent: 'space-between',
                  alignItems: 'center',
                  flexDirection: 'row',
                  flexWrap: 'wrap',
                  px: 1,
                  fontSize: 14,
                }}
                >
                  <Box>
                    <span style={styles.fieldTitle}>ID</span>
                    <span>{data?.getSprint.id || '-'}</span>
                  </Box>
                  <div style={{ display: 'flex', alignItems: 'center' }}>
                    <span style={styles.fieldTitle}>
                      Домен
                    </span>
                    <span
                      style={styles.link}
                      onClick={() => router.push(`/domains/${router.query.id}`)}
                    >
                      {sprint.domain?.name}
                    </span>
                    <span>
                      <S3Avatar name={sprint.domain?.name} size={30} sx={{ ml: 1 }} noBorder color="#687B98" />
                    </span>
                  </div>
                  <Box>
                    <span style={styles.fieldTitle}>Тип мероприятия</span>
                    <span style={styles.fieldValue}>Спринт</span>
                  </Box>
                  <Box sx={{ display: 'flex', alignItems: 'center' }}>
                    <span style={styles.fieldTitle}>Участники</span>
                    <Select
                      multiple
                      displayEmpty
                      variant="standard"
                      value={sprint.participants.map((item) => item.id) || []}
                      disabled={!sprint.participants?.length}
                      sx={{ minWidth: 120, maxWidth: 250 }}
                      renderValue={(selected) => {
                        if (selected.length === 0) {
                          return <span>Участников нет</span>;
                        }
                        return selected.map((p) => data.getSprint.participants.find((user) => user.id === p)?.name).join(', ');
                      }}
                    >
                      {data.getSprint.participants?.map((participant) => (
                        <MenuItem
                          key={participant.id}
                          value={participant.id}
                          disableRipple
                        >
                          {participant.name}
                        </MenuItem>
                      ))}
                    </Select>
                  </Box>
                </Box>

                <Box sx={{ padding: 1 }}>
                  Цель
                  <span style={{ ...styles.fieldContent, margin: 0, padding: 0 }}>
                    <MarkdownPreviewLoaded value={sprint.goal || 'Цели нет'} />
                  </span>
                </Box>

                <Box sx={{
                  padding: 1,
                  my: 2,
                }}
                >
                  <span style={styles.header}>Проблемы</span>
                  <Table size="small" className="issuesTable" sx={{ mt: 1.25, mb: 2.875 }}>
                    {sprint.issues.length > 0
                      ? (
                        <>
                          {sprint.issues
                            .map((issue) => (
                              <TableRow key={issue.id}>
                                <TableCell
                                  sx={{
                                    width: 50,
                                    height: 50,
                                    padding: 0,
                                    textAlign: 'center',
                                  }}
                                >
                                  <Tooltip title={priorities[issue.priority]?.name}>
                                    {priorityShield(issue.priority)}
                                  </Tooltip>
                                </TableCell>
                                <TableCell>
                                  <Box
                                    style={{
                                      display: 'flex',
                                      justifyContent: 'space-between',
                                      alignItems: 'center',
                                    }}
                                  >
                                    <Box>
                                      <Link href={`/domains/${router.query.id}/issues/${issue.id}`}>
                                        {issue.name}
                                      </Link>
                                      <Chip label={issue.stage?.name} sx={{ marginLeft: 1, display: !issue.stage?.name && 'none' }} />
                                    </Box>
                                    <S3Avatar name={issue.executor?.name} />
                                  </Box>
                                </TableCell>
                              </TableRow>
                            ))}
                        </>
                      )
                      : (
                        <TableRow>
                          <TableCell
                            sx={{
                              width: 50,
                              height: 50,
                              padding: 0,
                              textAlign: 'center',
                            }}
                          >
                            Проблем нет
                          </TableCell>
                        </TableRow>
                      )}
                  </Table>
                </Box>
              </Box>
            </Box>

            <Box
              sx={{
                width: 350,
                backgroundColor: '#DAE5F4',
                display: 'flex',
                flexDirection: 'column',
                alignItems: 'center',
              }}
            >
              <Box sx={{ ...styles.header, my: 2.75 }}>Дата начала и окончания спринта</Box>
              <RangeDatePicker
                startDate={new Date(sprint.startDate)}
                endDate={new Date(sprint.endDate)}
                disabled
              />
            </Box>
          </Box>
        </Box>

      </Box>

    </>
  );
}

export default Sprint;
