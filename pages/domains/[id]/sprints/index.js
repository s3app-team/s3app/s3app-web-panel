import { useState, useMemo } from 'react';
import Head from 'next/head';
import { useRouter } from 'next/router';
import { gql, useMutation, useQuery } from '@apollo/client';
import { useSnackbar } from 'notistack';

import {
  Button, MenuItem, ListItemIcon, Box,
  Tooltip,
} from '@mui/material';
import {
  Delete, Edit, OpenInBrowser,
} from '@mui/icons-material';

import {
  MaterialReactTable,
  useMaterialReactTable,
  MRT_GlobalFilterTextField as GlobalFilterTextField,
  MRT_TablePagination as TablePagination,
  MRT_ToggleFiltersButton as ToggleFiltersButton,
} from 'material-react-table';
import { MRT_Localization_RU as LocalizationRU } from 'material-react-table/locales/ru';

import moment from 'moment';
import Link from 'next/link';
import LeftSubPanel from '../../../../components/LeftSubPanel';
import DomainLeftPanel from '../../../../components/DomainLeftPanel';
import SimpleDialog from '../../../../components/SimpleDialog';
import TableDateRangeFilter from '../../../../components/TableDateRangeFilter';
import MarkdownPreviewLoaded from '../../../../components/MarkdownPreviewLoaded';

const DELETE_SPRINT = gql`
mutation Mutation($id: ID!) {
  deleteSprint(id: $id)
}
`;

const GET_DOMAIN = gql`
    query getDomain($id: ID!, $withSubDomains: Boolean) {
        getDomain(id: $id) {
            sprints(withSubDomains: $withSubDomains){
                id
                name
                startDate
                goal
                domain {
                  id
                  name
                }
            }
        }
    }
`;

function Sprints() {
  const router = useRouter();
  const { enqueueSnackbar } = useSnackbar();

  const { id } = router.query;

  const { data, refetch, loading } = useQuery(GET_DOMAIN, {
    variables: {
      id,
      withSubDomains: true,
    },
  });

  const [deleteSprint] = useMutation(DELETE_SPRINT);

  const [deleteDialog, setDeleteDialog] = useState({ open: false, sprint: {} });

  const columns = useMemo(
    () => [
      {
        accessorFn: (row) => new Date(row.startDate),
        header: 'Дата начала',
        // eslint-disable-next-line react/no-unstable-nested-components
        Header: ({ column }) => (
          <Tooltip title={column.columnDef.header}>
            {column.columnDef.header}
          </Tooltip>
        ),
        sortingFn: 'datetime',
        filterFn: (row, _id, filterValue) => {
          const date = new Date(row.original.startDate);
          if (!filterValue.length) return true;
          return filterValue[0] <= date && date <= filterValue[1];
        },
        muiFilterTextFieldProps: {
          autoComplete: 'off',
          inputProps: {
            form: {
              autocomplete: 'off',
            },
          },
        },
        Cell: ({ cell }) => moment(cell.getValue()).format('DD.MM.YYYY'),
        // eslint-disable-next-line react/no-unstable-nested-components
        Filter: ({ column }) => <TableDateRangeFilter column={column} />,
        maxSize: 100,
        grow: false,
      },
      {
        accessorKey: 'name',
        header: 'Название',
        filterFn: 'contains',
        filterVariant: 'text',
        muiFilterTextFieldProps: {
          placeholder: 'Название',
        },
        // eslint-disable-next-line react/no-unstable-nested-components
        Cell: ({ row }) => (
          <Link className="no-underline text-black" href={`/domains/${id}/sprints/${row.original.id}`}>
            {row.original.name}
          </Link>
        ),
      },
      {
        accessorKey: 'domain.name',
        header: 'Домен',
        filterVariant: 'autocomplete',
        muiFilterTextFieldProps: {
          placeholder: 'Домен',
        },
        // eslint-disable-next-line react/no-unstable-nested-components
        Cell: ({ row }) => (
          <Link
            className="text-wrap"
            href={`/domains/${row.original.domain.id}`}
          >
            {row.original.domain.name}
          </Link>
        ),
        minSize: 2,
        grow: false,
      },
    ],
    [id],
  );

  const mtable = useMaterialReactTable({
    columns,
    data: data?.getDomain.sprints || [],
    state: { isLoading: loading },
    muiCircularProgressProps: {
      color: 'primary',
      thickness: 5,
      size: 55,
    },
    localization: LocalizationRU,
    globalFilterFn: 'contains',
    columnFilterModeOptions: ['contains'],
    initialState: {
      columnPinning: {
        right: ['mrt-row-actions'],
      },
      pagination: { pageSize: 20, pageIndex: 0 },
      showColumnFilters: true,
      showGlobalFilter: true,
      density: 'compact',
    },
    displayColumnDefOptions: { 'mrt-row-actions': { minSize: 80, grow: false }, 'mrt-row-expand': { maxSize: 40, grow: false } },
    enableDensityToggle: false,
    enableColumnDragging: false,
    enableFacetedValues: true,
    enableRowActions: true,
    layoutMode: 'grid',
    paginationDisplayMode: 'pages',
    muiTablePaperProps: {
      className: 'shadow-none grow flex flex-col',
    },
    muiTableContainerProps: {
      className: 'grow border-solid border-0 border-t border-gray-200 flex max-h-min',
    },
    muiSearchTextFieldProps: {
      size: 'small',
      variant: 'outlined',
      autoComplete: 'off',
      inputProps: {
        form: {
          autocomplete: 'off',
        },
      },
    },
    muiFilterTextFieldProps: {
      autoComplete: 'off',
      inputProps: {
        form: {
          autocomplete: 'off',
        },
      },
    },
    muiPaginationProps: {
      color: 'secondary',
      shape: 'rounded',
      variant: 'outlined',
    },
    renderTopToolbar: ({ table }) => (
      <Box className="flex justify-between items-center px-4 py-2">
        <Box className="flex items-center gap-2">
          <GlobalFilterTextField table={table} />
          <ToggleFiltersButton table={table} />
          <TablePagination table={table} />
        </Box>
        <Button
          color="secondary"
          variant="contained"
          onClick={() => router.push(`${router.asPath}/create`)}
        >
          Добавить спринт
        </Button>
      </Box>
    ),
    renderRowActionMenuItems: ({ row }) => [
      <MenuItem
        key={0}
        onClick={() => router.push(`/domains/${id}/sprints/${row.original.id}`)}
      >
        <ListItemIcon>
          <OpenInBrowser />
        </ListItemIcon>
        Открыть
      </MenuItem>,
      <MenuItem
        key={1}
        onClick={() => router.push(`/domains/${id}/sprints/${row.original.id}/edit`)}
      >
        <ListItemIcon>
          <Edit />
        </ListItemIcon>
        Редактировать
      </MenuItem>,
      <MenuItem
        key={2}
        onClick={() => setDeleteDialog({ open: true, sprint: row.original })}
      >
        <ListItemIcon>
          <Delete />
        </ListItemIcon>
        Удалить
      </MenuItem>,
    ],
    renderDetailPanel: ({ row }) => (
      <div className="flex flex-col">
        <div>
          <MarkdownPreviewLoaded value={row.original.goal || 'Нет цели'} />
        </div>
      </div>
    ),
  }, [data, id, router]);

  return (
    <>
      <Head>
        <title>Спринты</title>
      </Head>
      <div className="flex">
        <LeftSubPanel title="Домены">
          <DomainLeftPanel selected="sprints" />
        </LeftSubPanel>

        <div className="flex-1 flex flex-col">
          <SimpleDialog
            open={deleteDialog.open}
            title="Удаление спринта"
            text={`Вы уверены, что хотите удалить спринт "${deleteDialog.sprint.name}"?`}
            onCancel={() => setDeleteDialog({ ...deleteDialog, open: false })}
            onAccept={async () => {
              try {
                await deleteSprint({
                  variables: { id: deleteDialog.sprint.id },
                  onCompleted: () => {
                    enqueueSnackbar('Спринт успешно удален', { variant: 'success' });
                    setDeleteDialog({ ...deleteDialog, open: false });
                    refetch();
                  },
                });
              } catch (e) {
                enqueueSnackbar(e.message, { variant: 'error' });
              }
            }}
          />
          <MaterialReactTable table={mtable} />
        </div>
      </div>
    </>
  );
}

export default Sprints;
