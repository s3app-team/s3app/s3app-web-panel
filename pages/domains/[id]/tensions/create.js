import Head from 'next/head';
import {
  Box,
  Button,
  MenuItem,
  Select,
  TextField,
  useTheme,
  IconButton,
} from '@mui/material';
import { useQuery, gql, useMutation } from '@apollo/client';
import { useState } from 'react';
import { useRouter } from 'next/router';
import { checkEmpty } from '../../../../components/common';
import LeftSubPanel from '../../../../components/LeftSubPanel';
import DomainLeftPanel from '../../../../components/DomainLeftPanel';

import Delete from '../../../../assets/delete.svg';
import Close from '../../../../assets/close.svg';
import SimpleDialog from '../../../../components/SimpleDialog';
import MarkdownEditorLoaded from '../../../../components/MarkdownEditorLoaded';

const CREATE_TENSION = gql`
  mutation createTension($tension: TensionInput!) {
    createTension(tension: $tension) {
      id
    }
  }
`;

const EDIT_TENSION = gql`
  mutation editTension($id: ID!, $tension: TensionInput!) {
    editTension(id: $id, tension: $tension) {
      id
    }
  }
`;

const DELETE_TENSION = gql`
  mutation deleteTension($id: ID!) {
    deleteTension(id: $id)
  }
`;

const GET_DOMAINS = gql`
  query getDomainsOfOrganization($domainId: ID!) {
    getDomainsOfOrganization(domainId: $domainId) {
      id
      name
    }
  }
`;

const GET_TENSION = gql`
  query getTension($id: ID!) {
    getTension(id: $id) {
      id
      name
      description
      createdAt
      authorUser {
        id
        name
      }
      domain {
        name
        id
      }
      tensionMark
    }
  }
`;

const styles = {
  fields: {
    '&': { width: '50%', paddingTop: '10px' },
    '& > div': {
      padding: '10px 0px',
      display: 'flex',
      alignItems: 'center',
      height: '20px',
    },
  },
  fieldTitle: {
    color: '#A2BDE4',
    paddingRight: 4,
    fontSize: '14px',
  },
  description: {
    border: '2px solid #A2BDE4',
    borderRadius: '5px',
    padding: '20px',
  },
  image: {
    marginLeft: '4px',
    borderRadius: '50%',
    borderWidth: '2px',
    borderStyle: 'solid',
    borderColor: (theme) => theme.palette.primary.main,
  },
};

export default function Tension() {
  const theme = useTheme();

  const router = useRouter();

  const isNew = () => !router.query['tension-id'];

  const [save] = useMutation(CREATE_TENSION);
  const [edit] = useMutation(EDIT_TENSION);
  const [remove] = useMutation(DELETE_TENSION);
  const domainId = router.query.id;
  const [tension, setTension] = useState({
    name: '',
    description: '',
    domainId,
    tensionMark: '0',
  });
  const domains = useQuery(GET_DOMAINS, { variables: { domainId } });
  const { loading } = useQuery(GET_TENSION, {
    variables: { id: router.query['tension-id'] },
    skip: !router.query['tension-id'],
    onCompleted: (data) => {
      setTension({
        name: data.getTension.name,
        description: data.getTension.description,
        domainId: data.getTension.domain?.id,
        authorUserId: data.getTension.author?.id,
        tensionMark: data.getTension.tensionMark,
      });
    },
  });
  const [deleteDialog, setDeleteDialog] = useState(false);
  const [closeDialog, setCloseDialog] = useState(false);

  if (loading) {
    return null;
  }

  return (
    <div style={{ display: 'flex' }}>
      <Head>
        <title>
          {router.query['tension-id'] ? `Потребность ${tension.name}` : 'Новая потребность'}
        </title>
      </Head>
      <LeftSubPanel title="Домены">
        <DomainLeftPanel selected="tensions" />
      </LeftSubPanel>
      <Box sx={{
        flex: 1, mx: 4, display: 'flex', flexDirection: 'column',
      }}
      >
        <Box sx={{ display: 'flex', justifyContent: 'space-between' }}>
          <Box
            style={{
              display: 'flex',
              alignItems: 'center',
              width: '100%',
              borderBottom: `${theme.palette.primary.main} solid 2px`,
            }}
          >
            <span style={styles.fieldTitle}>Название*</span>
            <TextField
              value={tension.name}
              error={checkEmpty(tension.name)}
              size="normal"
              variant="standard"
              color="secondary"
              fullWidth
              sx={checkEmpty(tension.name) ? undefined : {
                '& .MuiInput-underline::before': {
                  display: 'none',
                },
                '& .MuiInput-underline::after': {
                  display: 'none',
                },
              }}
              onChange={(e) => {
                setTension({
                  ...tension,
                  name: e.target.value,
                });
              }}
            />

            {!isNew()
                && (
                <IconButton onClick={() => setDeleteDialog(true)}>
                  <Delete
                    style={{ stroke: theme.palette.primary.main }}
                  />
                </IconButton>
                )}

            <IconButton onClick={() => {
              if (isNew()) router.back();
              else setCloseDialog(true);
            }}
            >
              <Close
                style={{ stroke: theme.palette.primary.main }}
              />
            </IconButton>
          </Box>
        </Box>
        <div style={{ display: 'flex', marginBottom: '20px' }}>
          <Box sx={styles.fields}>
            <div>
              <span style={styles.fieldTitle}>
                Создатель
              </span>
              <span style={styles.fieldValue}>
                {tension.authorUserId}
              </span>
            </div>
          </Box>
          <Box sx={styles.fields}>
            <div>
              <span style={styles.fieldTitle}>
                Оценка
              </span>
              <Select
                value={tension.tensionMark}
                onChange={(e) => {
                  setTension({
                    ...tension,
                    tensionMark: e.target.value,
                  });
                }}
                variant="standard"
                color="secondary"
              >
                {['-', '+', '0'].map((tensionMark) => (
                  <MenuItem key={tensionMark} value={tensionMark}>
                    {tensionMark}
                  </MenuItem>
                ))}
              </Select>
            </div>
          </Box>
          <Box sx={styles.fields}>
            <div>
              <span style={styles.fieldTitle}>
                Домен
              </span>
              <Select
                value={tension.domainId}
                onChange={(e) => {
                  setTension({
                    ...tension,
                    domainId: e.target.value,
                  });
                }}
                variant="standard"
                color="secondary"
              >
                {domains.data?.getDomainsOfOrganization?.map((domain) => (
                  <MenuItem key={domain.id} value={domain.id}>
                    {domain.name}
                  </MenuItem>
                ))}
              </Select>
            </div>
          </Box>
        </div>
        <Box style={{
          border: '1px solid #ddd',
          borderRadius: '5px',
          padding: '5px',
          marginBottom: '5px',
        }}
        >
          <MarkdownEditorLoaded
            value={tension.description}
            onChange={(e) => setTension({ ...tension, description: e })}
          />
        </Box>
        <div style={{ marginTop: '40px', textAlign: 'right' }}>
          <Button
            variant="outlined"
            color="tertiary"
            style={{ marginRight: '30px' }}
            onClick={() => setCloseDialog(true)}
          >
            Закрыть

          </Button>
          <Button
            variant="contained"
            disabled={
              checkEmpty(tension.name) || checkEmpty(tension.domainId)
            }
            onClick={async () => {
              if (router.query['tension-id']) {
                await edit({ variables: { id: router.query['tension-id'], tension: { ...tension, authorUserId: undefined } } });
              } else {
                await save({ variables: { tension: { ...tension, authorUserId: undefined } } });
              }
              router.push(`${router.asPath}/../`);
            }}
          >
            {isNew() ? 'Создать' : 'Сохранить'}
          </Button>
        </div>
      </Box>
      <SimpleDialog
        open={closeDialog}
        title="Сохранить изменения перед закрытием?"
        onCancel={() => { setCloseDialog(false); router.push(`${router.asPath}/../`); }}
        onAccept={async () => {
          if (router.query['tension-id']) {
            await edit({ variables: { id: router.query['tension-id'], tension: { ...tension, authorUserId: undefined } } });
          } else {
            await save({ variables: { tension: { ...tension, authorUserId: undefined } } });
          }
          router.push(`${router.asPath}/../`);
        }}
      />
      <SimpleDialog
        open={deleteDialog}
        title="Удаление потребности"
        text="Вы уверены, что хотите удалить эту потребность?"
        onCancel={() => setDeleteDialog(false)}
        onAccept={async () => {
          if (router.query['tension-id']) {
            await remove({ variables: { id: router.query['tension-id'] } });
          } else {
            await save({ variables: { tension } });
          }
          router.push(`${router.asPath}/../../`);
        }}
      />
    </div>
  );
}
