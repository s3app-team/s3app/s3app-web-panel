import { useState, useMemo } from 'react';
import Head from 'next/head';
import { useRouter } from 'next/router';
import { gql, useMutation, useQuery } from '@apollo/client';
import moment from 'moment';
import { useSnackbar } from 'notistack';
import {
  Button, Box, MenuItem, ListItemIcon,
  Tooltip,
} from '@mui/material';
import {
  Delete, Edit, OpenInBrowser,
} from '@mui/icons-material';
import {
  MaterialReactTable,
  useMaterialReactTable,
  MRT_GlobalFilterTextField as GlobalFilterTextField,
  MRT_TablePagination as TablePagination,
  MRT_ToggleFiltersButton as ToggleFiltersButton,
} from 'material-react-table';
import { MRT_Localization_RU as LocalizationRU } from 'material-react-table/locales/ru';

import Link from 'next/link';
import LeftSubPanel from '../../../../components/LeftSubPanel';
import DomainLeftPanel from '../../../../components/DomainLeftPanel';
import SimpleDialog from '../../../../components/SimpleDialog';
import TableDateRangeFilter from '../../../../components/TableDateRangeFilter';
import MarkdownPreviewLoaded from '../../../../components/MarkdownPreviewLoaded';
import { effects } from '../../../../components/common';

const DELETE_TENSION = gql`
  mutation Mutation($id: ID!) {
    deleteTension(id: $id)
  }
`;

const GET_DOMAIN = gql`
  query getDomain($id: ID!, $withSubDomains: Boolean) {
    getDomain(id: $id) {
      tensions(withSubDomains: $withSubDomains){
        id
        name
        tensionMark
        description
        authorUser {
          id
          name
        }
        domain {
          id
          name
        }
        createdAt
      }
    }
  }
`;

function Tensions() {
  const router = useRouter();
  const { enqueueSnackbar } = useSnackbar();
  const { id } = router.query;

  const { data, refetch, loading } = useQuery(GET_DOMAIN, {
    variables: {
      id,
      withSubDomains: true,
    },
  });

  const [deleteTension] = useMutation(DELETE_TENSION);
  const [deleteDialog, setDeleteDialog] = useState({ open: false, tension: {} });

  const columns = useMemo(() => [
    {
      accessorFn: (row) => new Date(row.createdAt),
      header: 'Дата создания',
      // eslint-disable-next-line react/no-unstable-nested-components
      Header: ({ column }) => (
        <Tooltip title={column.columnDef.header}>
          {column.columnDef.header}
        </Tooltip>
      ),
      sortingFn: 'datetime',
      filterFn: (row, _id, filterValue) => {
        const date = new Date(row.original.createdAt);
        if (!filterValue.length) return true;
        return filterValue[0] <= date && date <= filterValue[1];
      },
      Cell: ({ cell }) => moment(cell.getValue()).format('DD.MM.YYYY'),
      // eslint-disable-next-line react/no-unstable-nested-components
      Filter: ({ column }) => <TableDateRangeFilter column={column} />,
      maxSize: 100,
      grow: false,
    },
    {
      accessorKey: 'name',
      header: 'Название',
      filterFn: 'contains',
      filterVariant: 'text',
      muiFilterTextFieldProps: {
        placeholder: 'Название',
      },
      // eslint-disable-next-line react/no-unstable-nested-components
      Cell: ({ row }) => (
        <Link className="no-underline text-black" href={`/domains/${id}/tensions/${row.original.id}`}>
          {row.original.name}
        </Link>
      ),
    },
    {
      accessorKey: 'domain.name',
      header: 'Домен',
      filterVariant: 'autocomplete',
      // eslint-disable-next-line react/no-unstable-nested-components
      Cell: ({ row }) => (
        <Link
          className="text-wrap"
          href={`/domains/${row.original.domain.id}`}
        >
          {row.original.domain.name}
        </Link>
      ),
      minSize: 2,
      grow: false,
    },
    {
      accessorKey: 'tensionMark',
      header: 'Оценка',
      // eslint-disable-next-line react/no-unstable-nested-components
      Header: ({ column }) => (
        <Tooltip title={column.columnDef.header}>
          {column.columnDef.header}
        </Tooltip>
      ),
      sortingFn: (rowA, rowB, columnId) => {
        const order = { '+': 1, 0: 2, '-': 3 };
        return order[rowA.original[columnId]] - order[rowB.original[columnId]];
      },
      // eslint-disable-next-line react/no-unstable-nested-components
      Cell: ({ cell }) => (
        <Tooltip title={effects[cell.getValue()].label}>
          {effects[cell.getValue()].icon}
        </Tooltip>
      ),
      filterVariant: 'multi-select',
      filterSelectOptions: [
        { label: 'Положительная', value: '+' },
        { label: 'Нейтральная', value: '0' },
        { label: 'Негативная', value: '-' },
      ],
      muiFilterTextFieldProps: {
        placeholder: 'Оценка',
      },
      muiTableBodyCellProps: {
        align: 'center',
      },
      maxSize: 60,
      grow: false,
    },
    {
      accessorKey: 'authorUser.name',
      header: 'Автор',
      filterVariant: 'autocomplete',
      maxSize: 100,
      grow: false,
    },
  ], [id]);

  const table = useMaterialReactTable({
    columns,
    data: data?.getDomain.tensions || [],
    state: { isLoading: loading },
    localization: LocalizationRU,
    globalFilterFn: 'contains',
    columnFilterModeOptions: ['contains'],
    initialState: {
      showColumnFilters: true,
      showGlobalFilter: true,
      pagination: { pageSize: 20, pageIndex: 0 },
    },
    displayColumnDefOptions: { 'mrt-row-actions': { minSize: 80, grow: false }, 'mrt-row-expand': { maxSize: 40, grow: false } },
    enableRowActions: true,
    layoutMode: 'grid',
    paginationDisplayMode: 'pages',
    enableFacetedValues: true,
    muiFilterTextFieldProps: {
      autoComplete: 'off',
      inputProps: {
        form: {
          autocomplete: 'off',
        },
      },
    },
    positionActionsColumn: 'last',
    renderTopToolbar: ({ table: _table }) => (
      <Box sx={{ display: 'flex', justifyContent: 'space-between', p: '8px' }}>
        <Box sx={{ display: 'flex', gap: '8px', alignItems: 'center' }}>
          <GlobalFilterTextField table={_table} />
          <ToggleFiltersButton table={_table} />
        </Box>
        <Box sx={{ display: 'flex', gap: '8px', alignItems: 'center' }}>
          <TablePagination table={_table} />
          <Button
            color="secondary"
            variant="contained"
            onClick={() => router.push(`${router.asPath}/create`)}
          >
            Добавить факт
          </Button>
        </Box>
      </Box>
    ),
    renderRowActionMenuItems: ({ row }) => [
      <MenuItem
        key="open"
        onClick={() => router.push(`/domains/${id}/tensions/${row.original.id}`)}
      >
        <ListItemIcon><OpenInBrowser fontSize="small" /></ListItemIcon>
        Открыть
      </MenuItem>,
      <MenuItem
        key="edit"
        onClick={() => router.push(`/domains/${id}/tensions/${row.original.id}/edit`)}
      >
        <ListItemIcon><Edit fontSize="small" /></ListItemIcon>
        Редактировать
      </MenuItem>,
      <MenuItem
        key="delete"
        onClick={() => setDeleteDialog({ open: true, tension: row.original })}
      >
        <ListItemIcon><Delete fontSize="small" /></ListItemIcon>
        Удалить
      </MenuItem>,
    ],
    renderDetailPanel: ({ row }) => (
      <div className="flex flex-col">
        <div>
          <MarkdownPreviewLoaded value={row.original.description || 'Нет описания'} />
        </div>
      </div>
    ),
  });

  return (
    <>
      <Head>
        <title>Факты</title>
      </Head>
      <Box sx={{ display: 'flex' }}>
        <LeftSubPanel title="Домены">
          <DomainLeftPanel selected="tensions" />
        </LeftSubPanel>
        <Box sx={{ flexGrow: 1 }}>
          <MaterialReactTable table={table} />
        </Box>
      </Box>
      <SimpleDialog
        open={deleteDialog.open}
        title="Удаление факта"
        text={`Вы уверены, что хотите удалить факт "${deleteDialog.tension.name}"?`}
        onCancel={() => setDeleteDialog({ ...deleteDialog, open: false })}
        onAccept={async () => {
          try {
            await deleteTension({
              variables: { id: deleteDialog.tension.id },
              onCompleted: () => {
                enqueueSnackbar('Факт успешно удален', { variant: 'success' });
                setDeleteDialog({ ...deleteDialog, open: false });
                refetch();
              },
            });
          } catch (e) {
            enqueueSnackbar(e.message, { variant: 'error' });
          }
        }}
      />
    </>
  );
}

export default Tensions;
