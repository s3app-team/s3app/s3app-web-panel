import Head from 'next/head';
import {
  Box,
  Button,
  MenuItem,
  Select,
  TextField,
  useTheme,
  IconButton,
} from '@mui/material';
import { useQuery, gql, useMutation } from '@apollo/client';
import { useState } from 'react';
import { useRouter } from 'next/router';
import { checkEmpty } from '../../../../components/common';
import LeftSubPanel from '../../../../components/LeftSubPanel';
import DomainLeftPanel from '../../../../components/DomainLeftPanel';

import Delete from '../../../../assets/delete.svg';
import Close from '../../../../assets/close.svg';
import SimpleDialog from '../../../../components/SimpleDialog';
import MarkdownEditorLoaded from '../../../../components/MarkdownEditorLoaded';

const CREATE_THOUGHT = gql`
  mutation createThought($thought: ThoughtInput!) {
    createThought(thought: $thought) {
      id
    }
  }
`;

const EDIT_THOUGHT = gql`
  mutation editThought($id: ID!, $thought: ThoughtInput!) {
    editThought(id: $id, thought: $thought) {
      id
    }
  }
`;

const DELETE_THOUGHT = gql`
  mutation deleteThought($id: ID!) {
    deleteThought(id: $id)
  }
`;

const GET_DOMAINS = gql`
  query getDomainsOfOrganization($domainId: ID!) {
    getDomainsOfOrganization(domainId: $domainId) {
      id
      name
    }
  }
`;

const GET_THOUGHT = gql`
  query getThought($id: ID!) {
    getThought(id: $id) {
      id
      name
      description
      createdAt
      authorUser {
        id
        name
      }
      domain {
        name
        id
      }
    }
  }
`;

const styles = {
  fields: {
    '&': { width: '50%', paddingTop: '10px' },
    '& > div': {
      padding: '10px 0px',
      display: 'flex',
      alignItems: 'center',
      height: '20px',
    },
  },
  fieldTitle: {
    color: '#A2BDE4',
    paddingRight: 4,
    fontSize: '14px',
  },
  description: {
    border: '2px solid #A2BDE4',
    borderRadius: '5px',
    padding: '20px',
  },
  image: {
    marginLeft: '4px',
    borderRadius: '50%',
    borderWidth: '2px',
    borderStyle: 'solid',
    borderColor: (theme) => theme.palette.primary.main,
  },
};

export default function Thought() {
  const theme = useTheme();

  const router = useRouter();

  const isNew = () => !router.query['thought-id'];

  const [save] = useMutation(CREATE_THOUGHT);
  const [edit] = useMutation(EDIT_THOUGHT);
  const [remove] = useMutation(DELETE_THOUGHT);
  const domainId = router.query.id;
  const [thought, setThought] = useState({
    name: '',
    description: '',
    domainId,
  });
  const domains = useQuery(GET_DOMAINS, { variables: { domainId } });
  const { loading } = useQuery(GET_THOUGHT, {
    variables: { id: router.query['thought-id'] },
    skip: !router.query['thought-id'],
    onCompleted: (data) => {
      setThought({
        name: data.getThought.name,
        description: data.getThought.description,
        domainId: data.getThought.domain?.id,
        authorUserId: data.getThought.author?.id,
      });
    },
  });
  const [deleteDialog, setDeleteDialog] = useState(false);
  const [closeDialog, setCloseDialog] = useState(false);

  console.log(thought);

  if (loading) {
    return null;
  }

  return (
    <div style={{ display: 'flex' }}>
      <Head>
        <title>
          {router.query['thought-id'] ? `Потребность ${thought.name}` : 'Новая потребность'}
        </title>
      </Head>
      <LeftSubPanel title="Домены">
        <DomainLeftPanel selected="thoughts" />
      </LeftSubPanel>
      <Box sx={{
        flex: 1, mx: 4, display: 'flex', flexDirection: 'column',
      }}
      >
        <Box sx={{ display: 'flex', justifyContent: 'space-between' }}>
          <Box
            style={{
              display: 'flex',
              alignItems: 'center',
              width: '100%',
              borderBottom: `${theme.palette.primary.main} solid 2px`,
            }}
          >
            <span style={styles.fieldTitle}>Название*</span>
            <TextField
              value={thought.name}
              error={checkEmpty(thought.name)}
              size="normal"
              variant="standard"
              color="secondary"
              fullWidth
              sx={checkEmpty(thought.name) ? undefined : {
                '& .MuiInput-underline::before': {
                  display: 'none',
                },
                '& .MuiInput-underline::after': {
                  display: 'none',
                },
              }}
              onChange={(e) => {
                setThought({
                  ...thought,
                  name: e.target.value,
                });
              }}
            />

            {!isNew()
                && (
                <IconButton onClick={() => setDeleteDialog(true)}>
                  <Delete
                    style={{ stroke: theme.palette.primary.main }}
                  />
                </IconButton>
                )}

            <IconButton onClick={() => {
              if (isNew()) router.back();
              else setCloseDialog(true);
            }}
            >
              <Close
                style={{ stroke: theme.palette.primary.main }}
              />
            </IconButton>
          </Box>
        </Box>
        <div style={{ display: 'flex', marginBottom: '20px' }}>
          <Box sx={styles.fields}>
            <div>
              <span style={styles.fieldTitle}>
                Создатель
              </span>
              <span style={styles.fieldValue}>
                {thought.authorUserId}
              </span>
            </div>
          </Box>
          <Box sx={styles.fields}>
            <div>
              <span style={styles.fieldTitle}>
                Домен
              </span>
              <Select
                value={thought.domainId}
                onChange={(e) => {
                  setThought({
                    ...thought,
                    domainId: e.target.value,
                  });
                }}
                variant="standard"
                color="secondary"
              >
                {domains.data?.getDomainsOfOrganization?.map((domain) => (
                  <MenuItem key={domain.id} value={domain.id}>
                    {domain.name}
                  </MenuItem>
                ))}
              </Select>
            </div>
          </Box>
        </div>
        <Box style={{
          border: '1px solid #ddd',
          borderRadius: '5px',
          padding: '5px',
          marginBottom: '5px',
        }}
        >
          <MarkdownEditorLoaded
            value={thought.description}
            onChange={(e) => setThought({ ...thought, description: e })}
          />
        </Box>
        <div style={{ marginTop: '40px', textAlign: 'right' }}>
          <Button
            variant="outlined"
            color="tertiary"
            style={{ marginRight: '30px' }}
            onClick={() => setCloseDialog(true)}
          >
            Закрыть

          </Button>
          <Button
            variant="contained"
            disabled={
              checkEmpty(thought.name) || checkEmpty(thought.domainId)
            }
            onClick={async () => {
              if (router.query['thought-id']) {
                await edit({ variables: { id: router.query['thought-id'], thought: { ...thought, authorUserId: undefined } } });
              } else {
                await save({ variables: { thought: { ...thought, authorUserId: undefined } } });
              }
              router.push(`${router.asPath}/../`);
            }}
          >
            {isNew() ? 'Создать' : 'Сохранить'}
          </Button>
        </div>
      </Box>
      <SimpleDialog
        open={closeDialog}
        title="Сохранить изменения перед закрытием?"
        onCancel={() => { setCloseDialog(false); router.push(`${router.asPath}/../`); }}
        onAccept={async () => {
          if (router.query['thought-id']) {
            await edit({ variables: { id: router.query['thought-id'], thought: { ...thought, authorUserId: undefined } } });
          } else {
            await save({ variables: { thought: { ...thought, authorUserId: undefined } } });
          }
          router.push(`${router.asPath}/../`);
        }}
      />
      <SimpleDialog
        open={deleteDialog}
        title="Удаление потребности"
        text="Вы уверены, что хотите удалить эту потребность?"
        onCancel={() => setDeleteDialog(false)}
        onAccept={async () => {
          if (router.query['thought-id']) {
            await remove({ variables: { id: router.query['thought-id'] } });
          } else {
            await save({ variables: { thought } });
          }
          router.push(`${router.asPath}/../../`);
        }}
      />
    </div>
  );
}
