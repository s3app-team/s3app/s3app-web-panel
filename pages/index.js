import { gql, useQuery } from '@apollo/client';
import DomainMap from '../components/domainMap';
import DomainCreator from '../components/domain-creator';
import { domainsToHierarchy } from '../components/common';

const GET_DOMAINS = gql`
query getDomains {
  getDomains {
    id
    name
    domainType
    parentDomain {
      id
    }
    participants {
      name
      id
    }
  }
}`;

function Home(props) {
  const allDomains = useQuery(GET_DOMAINS);

  if (allDomains.loading) return null;

  const domains = domainsToHierarchy(allDomains.data?.getDomains);

  props.refetchDomain();

  return (
    <div>
      <DomainMap domains={domains} clickDomain />
    </div>
  );
}

Home.Header = function Header(props) {
  return (
    <div style={{ paddingLeft: 8 }}>
      <DomainCreator
        {...props}
        refetch={props.refetchDomain}
        organization
        style={{ width: 'initial' }}
      />
    </div>
  );
};

export default Home;
