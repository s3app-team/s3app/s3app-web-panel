import {
  Button, Container, TextField, Typography,
} from '@mui/material';
import Link from '@mui/material/Link';
import { gql, useMutation } from '@apollo/client';
import { useState } from 'react';
import { useSnackbar } from 'notistack';

const SEND_PASSWORD_RECOVER_LINK = gql`
  mutation ($email: String!) {
    sendRecoverPasswordLink(email: $email)
  }
`;

export default function Request() {
  const { enqueueSnackbar } = useSnackbar();
  const [sendPasswordRecoverLink] = useMutation(SEND_PASSWORD_RECOVER_LINK);
  const [email, setEmail] = useState('');

  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      await sendPasswordRecoverLink({ variables: { email } });
      enqueueSnackbar(
        'Ссылка для восстановления пароля отправлена на указанный E-Mail',
        { variant: 'success' },
      );
    } catch (error) {
      enqueueSnackbar(error.message, { variant: 'error' });
    }
  };

  return (
    <Container className="flex flex-col items-center justify-center max-w-lg pb-18">
      <form
        onSubmit={handleSubmit}
        className="flex flex-col items-center justify-center"
      >
        <div className="flex flex-col items-center">
          <Typography variant="h5">Восстановление пароля</Typography>
          <div className="mt-4 text-center">
            <TextField
              margin="normal"
              fullWidth
              label="E-Mail"
              type="email"
              value={email}
              onChange={(e) => setEmail(e.target.value)}
            />
            <Button
              type="submit"
              fullWidth
              variant="contained"
              className="mt-4"
              disabled={!email}
            >
              Выслать ссылку
            </Button>
            <div className="flex justify-between pt-2 text-base">
              <Link href="/login" className="no-underline">
                Войти
              </Link>
              <Link href="/register" className="no-underline">
                Зарегистрироваться
              </Link>
            </div>
          </div>
        </div>
      </form>
    </Container>
  );
}
