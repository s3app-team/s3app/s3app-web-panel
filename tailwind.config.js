module.exports = {
  content: ['./pages/**/*.js', './components/*.js'],
  important: '#__next',
  theme: {
    extend: {
      colors: {
        primary: '#f79244',
        secondary: '#dae5f4',
        tertiary: '#687B98',
      },
      spacing: {
        18: '4.5rem',
      },
      borderRadius: {
        '4xl': '2rem',
      },
    },
  },
  corePlugins: {
    preflight: false,
  },
  plugins: [],
};
